function Cities_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, fileUpload, $q) {
	"use strict";

	$http.get(BASE_URL + 'api/custom_fields_by_type/' + 'cities').then(function (custom_fields) {
		$scope.all_custom_fields = custom_fields.data;
		$scope.custom_fields = $filter('filter')($scope.all_custom_fields, {
			active: 'true',
		});
	});

	$scope.citiesLoader = true;
	$scope.Create = buildToggler('Create');	

	function buildToggler(navID) {
		return function () {
			$mdSidenav(navID).toggle();

		};
	}

	$scope.close = function () {
		$mdSidenav('Create').close();
		$mdDialog.hide();
	};
	
	var deferred = $q.defer();
	$scope.city_list = {
		order: '',
		limit: 10,
		page: 1
	};
	
	$http.get(BASE_URL + 'api/cities').then(function (Cities) {
		$scope.cities = Cities.data;

		$scope.limitOptions = [5, 10, 15, 20];
		if($scope.cities.length > 20 ) {
			$scope.limitOptions = [5, 10, 15, 20, $scope.cities.length];
		}
		$scope.citiesLoader = false;

		$scope.saving = false;

		$scope.AddCity = function () {
			$scope.saving = true;
			$scope.tempArr = [];
			angular.forEach($scope.custom_fields, function (value) {
				if (value.type === 'input') {
					$scope.field_data = value.data;
				}
				if (value.type === 'textarea') {
					$scope.field_data = value.data;
				}
				if (value.type === 'date') {
					$scope.field_data = moment(value.data).format("YYYY-MM-DD");
				}
				if (value.type === 'select') {
					$scope.field_data = JSON.stringify(value.selected_opt);
				}
				$scope.tempArr.push({
					id: value.id,
					name: value.name,
					type: value.type,
					order: value.order,
					data: $scope.field_data,
					relation: value.relation,
					permission: value.permission,
				});
			});
			if (!$scope.cities) {
				var dataObj = $.param({
					city: ''
				});
			} else {
				var dataObj = $.param({
					city: $scope.cities.city
				});
			}
			var posturl = BASE_URL + 'cities/create/';
			$http.post(posturl, dataObj, config)
				.then(
					function (response) {
						if (response.data.success == true) {
							if (response.data.id) {
								$http.get(BASE_URL + 'api/cities').then(function (Cities) {
									$scope.cities = Cities.data;
								});
								globals.mdToast('success', response.data.message);
								$mdSidenav('Create').close();							
							} else {
								$scope.saving = false;
								globals.mdToast('error', response.data.message);
							}
						} else {
							$scope.saving = false;
							globals.mdToast('error', response.data.message);
						}
					},
					function (response) {
						$scope.saving = false;
						$http.get(BASE_URL + 'api/cities').then(function (Cities) {
							$scope.cities = Cities.data;
						});
					}
				);
		};

		// Filtered Datas
		$scope.search = {
			city: ''
		};
	});

	$scope.UpdateCity = function(id) {
		$scope.update = true;
		var dataObj = $.param({
			city: $scope.city.city
		});
		$http.post(BASE_URL + 'cities/update_city/' + id, dataObj, config)
		.then(
			function(response) {
				$scope.update = false;
				if (response.data.success == true) {
					$mdDialog.cancel();
					$http.get(BASE_URL + 'api/cities').then(function (Units) {
						$scope.units = Units.data;
					});
					showToast(NTFTITLE, response.data.message, ' success');
				} else {
					$http.get(BASE_URL + 'api/cities').then(function (Units) {
						$scope.units = Units.data;
					});
					showToast(NTFTITLE, response.data.message, ' danger');
				}
			},
			function(response) {
				$scope.update = false;
			}
			);
	};

	$scope.editCity = function(city) {
		$scope.city = city;
		$mdDialog.show({
			templateUrl: 'update_city.html',
			scope: $scope,
			preserveScope: true,
			targetEvent: city
		});
	};

	$scope.Delete = function (CITYID) {
		globals.deleteDialog(lang.attention, lang.delete_city, CITYID, lang.doIt, lang.cancel, 'cities/remove/' + CITYID, function(response) {
			if (response.success == true) {
				window.location.href = BASE_URL + 'cities';
			} else {
				globals.mdToast('error',response.message);
			}
		});
	};


}

LwCRM.controller('Cities_Controller', Cities_Controller);
