
function Solar_Calculator_Controller($scope, $http, $filter, $mdSidenav, $mdDialog) {
	"use strict";
	
	$http.get(BASE_URL + 'solar_calculator/states').then(function (States) {
		$scope.states = States.data;
	});
	$http.get(BASE_URL + 'solar_calculator/products').then(function (Products) {
		$scope.products = Products.data;
	});
	$http.get(BASE_URL + 'solar_calculator/customer_categories').then(function (Cust_Categories) {
		$scope.cust_categories = Cust_Categories.data;
	});
	$scope.getDistricts = function(state_id) {
		$http.get(BASE_URL + 'solar_calculator/districts/' + state_id).then(function (Districts) {
			$scope.districts = Districts.data; 
		});
	};

	$scope.getCustSubcategories = function(cust_category_id) {
		$http.get(BASE_URL + 'solar_calculator/customer_subcategories/' + cust_category_id).then(function (Cust_Subcategories) {
			$scope.cust_subcategories = Cust_Subcategories.data; 
		});
	};

	$scope.ShowFields = function (value) {
        $scope.type = value ;
        $scope.calculationtype = value;
    }

    $scope.ShowMonth = function (value) {
        $scope.unittype = value ;
    }


	function buildToggler(navID) {
		return function () {
			$mdSidenav(navID).toggle();

		};
	}

	
	$scope.GetOtp = function () {

		
			$scope.saving = true;
			$scope.tempArr = [];
			
			angular.forEach($scope.custom_fields, function (value) {
				if (value.type === 'input') {
					$scope.field_data = value.data;
				}
				if (value.type === 'textarea') {
					$scope.field_data = value.data;
				}
				if (value.type === 'date') {
					$scope.field_data = moment(value.data).format("YYYY-MM-DD");
				}
				if (value.type === 'select') {
					$scope.field_data = JSON.stringify(value.selected_opt);
				}
				$scope.tempArr.push({
					id: value.id,
					name: value.name,
					type: value.type,
					order: value.order,
					data: $scope.field_data,
					relation: value.relation,
					permission: value.permission,
				});
			});
			if (!$scope.leads) {
				var dataObj = $.param({
					name: '',
					middle_name: '',
					last_name:'',
					email: '',
					phone: '',
					address: '',
					pincode: '',
					state_id : '',
					district_id : '',
					city: '',					
					cust_category_id: '',
					cust_subcategory_id: '',
					product: '',
					consumer_number: '',
					supplier: '',
					units: '',
					unit_type: '',
					no_of_months: '',
					approx_unit_rate: ''
				});
			} else {
				var dataObj = $.param({
					name: $scope.leads.name,
					middle_name: $scope.leads.middle_name,
					last_name: $scope.leads.last_name,
					email: $scope.leads.email,
					phone: $scope.leads.phone,
					address: $scope.leads.address,
					pincode: $scope.leads.pincode,
					state_id: $scope.leads.state_id,
					district_id: $scope.leads.district_id,
					city: $scope.leads.city,					
					cust_category_id: $scope.leads.cust_category_id,
					cust_subcategory_id: $scope.leads.cust_subcategory_id,
					product: $scope.leads.product,
					consumer_number: $scope.leads.consumer_number,
					supplier: $scope.leads.supplier,
					units: $scope.leads.units,
					unit_type: $scope.leads.unit_type,
					no_of_months: $scope.leads.months,
					approx_unit_rate: $scope.leads.approx_unit_rate,
					calculation_type:$scope.leads.calculation_type,
				});
			}
			var posturl = BASE_URL + 'solar_calculator/getOtp/';
			$http.post(posturl, dataObj, config)
				.then(
					function (response) {
						if (response.data.success == true) 
						{
							location.reload();
							globals.mdToast('success', response.data.message);			
						} 
						else {
							$scope.saving = false;
							globals.mdToast('error', response.data.message);
						}
					},
					function (response) {
						console.log(response);
					}
				);
		
			
		};



	$scope.addLead = function () {

		
			$scope.saving = true;
			$scope.tempArr = [];
			
			angular.forEach($scope.custom_fields, function (value) {
				if (value.type === 'input') {
					$scope.field_data = value.data;
				}
				if (value.type === 'textarea') {
					$scope.field_data = value.data;
				}
				if (value.type === 'date') {
					$scope.field_data = moment(value.data).format("YYYY-MM-DD");
				}
				if (value.type === 'select') {
					$scope.field_data = JSON.stringify(value.selected_opt);
				}
				$scope.tempArr.push({
					id: value.id,
					name: value.name,
					type: value.type,
					order: value.order,
					data: $scope.field_data,
					relation: value.relation,
					permission: value.permission,
				});
			});
			if (!$scope.leads) {
				var dataObj = $.param({
					name: '',
					middle_name: '',
					last_name:'',
					email: '',
					phone: '',
					address: '',
					pincode: '',
					state_id : '',
					district_id : '',
					city: '',					
					cust_category_id: '',
					cust_subcategory_id: '',
					product: '',
					consumer_number: '',
					supplier: '',
					units: '',
					unit_type: '',
					no_of_months: '',
					approx_unit_rate: ''
				});
			} else {
				var dataObj = $.param({
					name: $scope.leads.name,
					middle_name: $scope.leads.middle_name,
					last_name: $scope.leads.last_name,
					email: $scope.leads.email,
					phone: $scope.leads.phone,
					address: $scope.leads.address,
					pincode: $scope.leads.pincode,
					state_id: $scope.leads.state_id,
					district_id: $scope.leads.district_id,
					city: $scope.leads.city,					
					cust_category_id: $scope.leads.cust_category_id,
					cust_subcategory_id: $scope.leads.cust_subcategory_id,
					product: $scope.leads.products,
					consumer_number: $scope.leads.consumer_number,
					supplier: $scope.leads.supplier,
					units: $scope.leads.units,
					unit_type: $scope.leads.unit_type,
					no_of_months: $scope.leads.months,
					approx_unit_rate: $scope.leads.approx_unit_rate,
					calculation_type:$scope.leads.calculation_type,
				});
			}
			var posturl = BASE_URL + 'solar_calculator/create/';
			$http.post(posturl, dataObj, config)
				.then(
					function (response) {
						if (response.data.success == true) 
						{
							$scope.PDFCreating = false;
							$scope.CreatedPDFName = response.data.file_name;
						} 
					},
					function (response) {
						console.log(response);
					}
				);
		
			
		};

	
	// Enable resend OTP button after 120 sec.
	window.onload = function() {
    window.setTimeout(setDisabled, 120000);

    function setDisabled() {
    	document.getElementById('resendotp').disabled = false;
	}
}
	


	

	

	

	

	
}

LwCRM.controller('Solar_Calculator_Controller', Solar_Calculator_Controller);