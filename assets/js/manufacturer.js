function Manufacturer_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, fileUpload, $q) {
	"use strict";

	$http.get(BASE_URL + 'api/custom_fields_by_type/' + 'manufacturer').then(function (custom_fields) {
		$scope.all_custom_fields = custom_fields.data;
		$scope.custom_fields = $filter('filter')($scope.all_custom_fields, {
			active: 'true',
		});
	});

	$scope.manufacturersLoader = true;
	$scope.Create = buildToggler('Create');	

	function buildToggler(navID) {
		return function () {
			$mdSidenav(navID).toggle();

		};
	}

	$scope.close = function () {
		$mdSidenav('Create').close();
		$mdDialog.hide();
	};
	
	var deferred = $q.defer();
	$scope.manufacturer_list = {
		order: '',
		limit: 5,
		page: 1
	};
	
	$http.get(BASE_URL + 'api/manufacturers').then(function (Manufacturers) {
		$scope.manufacturers = Manufacturers.data;

		$scope.limitOptions = [5, 10, 15, 20];
		if($scope.manufacturers.length > 20 ) {
			$scope.limitOptions = [5, 10, 15, 20, $scope.manufacturers.length];
		}
		$scope.manufacturersLoader = false;

		$scope.saving = false;

		$scope.AddManufacturer = function () {
			$scope.saving = true;
			$scope.tempArr = [];
			angular.forEach($scope.custom_fields, function (value) {
				if (value.type === 'input') {
					$scope.field_data = value.data;
				}
				if (value.type === 'textarea') {
					$scope.field_data = value.data;
				}
				if (value.type === 'date') {
					$scope.field_data = moment(value.data).format("YYYY-MM-DD");
				}
				if (value.type === 'select') {
					$scope.field_data = JSON.stringify(value.selected_opt);
				}
				$scope.tempArr.push({
					id: value.id,
					name: value.name,
					type: value.type,
					order: value.order,
					data: $scope.field_data,
					relation: value.relation,
					permission: value.permission,
				});
			});
			if (!$scope.manufacturer) {
				var dataObj = $.param({
					name: ''
				});
			} else {
				var dataObj = $.param({
					name: $scope.manufacturer.name
				});
			}
			var posturl = BASE_URL + 'manufacturer/create/';
			$http.post(posturl, dataObj, config)
				.then(
					function (response) {
						if (response.data.success == true) {
							if (response.data.id) {
								$http.get(BASE_URL + 'api/manufacturers').then(function (Manufacturers) {
									$scope.manufacturers = Manufacturers.data;
								});
								globals.mdToast('success', response.data.message);
								$mdSidenav('Create').close();							
							} else {
								$scope.saving = false;
								globals.mdToast('error', response.data.message);
							}
						} else {
							$scope.saving = false;
							globals.mdToast('error', response.data.message);
						}
					},
					function (response) {
						$scope.saving = false;
						$http.get(BASE_URL + 'api/manufacturers').then(function (Manufacturers) {
							$scope.manufacturers = Manufacturers.data;
						});
					}
				);
		};

		// Filtered Datas
		$scope.search = {
			name: ''
		};
	});

	$scope.UpdateManufacturer = function(id) {
		$scope.update = true;
		var dataObj = $.param({
			name: $scope.manufacturer.name
		});
		$http.post(BASE_URL + 'manufacturer/update_manufacturer/' + id, dataObj, config)
		.then(
			function(response) {
				$scope.update = false;
				if (response.data.success == true) {
					$mdDialog.cancel();
					$http.get(BASE_URL + 'api/manufacturers').then(function (Units) {
						$scope.units = Units.data;
					});
					showToast(NTFTITLE, response.data.message, ' success');
				} else {
					$http.get(BASE_URL + 'api/manufacturers').then(function (Units) {
						$scope.units = Units.data;
					});
					showToast(NTFTITLE, response.data.message, ' danger');
				}
			},
			function(response) {
				$scope.update = false;
			}
			);
	};

	$scope.editManufacturer = function(manufacturer) {
		$scope.manufacturer = manufacturer;
		$mdDialog.show({
			templateUrl: 'update_manufacturer.html',
			scope: $scope,
			preserveScope: true,
			targetEvent: manufacturer
		});
	};

	$scope.Delete = function (MANFID) {
		globals.deleteDialog(lang.attention, lang.delete_area, MANFID, lang.doIt, lang.cancel, 'manufacturer/remove/' + MANFID, function(response) {
			if (response.success == true) {
				window.location.href = BASE_URL + 'manufacturer';
			} else {
				globals.mdToast('error',response.message);
			}
		});
	};


}

LwCRM.controller('Manufacturer_Controller', Manufacturer_Controller);
