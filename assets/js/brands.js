function Brands_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, fileUpload, $q) {
	"use strict";

	$http.get(BASE_URL + 'manufacturer/getManufacturers').then(function (Manufacturers) {
		$scope.manufacturers = Manufacturers.data;
	});

	$http.get(BASE_URL + 'api/custom_fields_by_type/' + 'brands').then(function (custom_fields) {
		$scope.all_custom_fields = custom_fields.data;
		$scope.custom_fields = $filter('filter')($scope.all_custom_fields, {
			active: 'true',
		});
	});

	$scope.brandsLoader = true;
	$scope.Create = buildToggler('Create');	

	function buildToggler(navID) {
		return function () {
			$mdSidenav(navID).toggle();

		};
	}
	
	$scope.close = function () {
		$mdSidenav('Create').close();
		$mdDialog.hide();
	};

	var deferred = $q.defer();
	$scope.brand_list = {
		order: '',
		limit: 10,
		page: 1
	};
	
	$http.get(BASE_URL + 'api/brands').then(function (Brands) {
		$scope.brands = Brands.data;

		$scope.limitOptions = [5, 10, 15, 20];
		if($scope.brands.length > 20 ) {
			$scope.limitOptions = [5, 10, 15, 20, $scope.brands.length];
		}
		$scope.brandsLoader = false;

		$scope.saving = false;

		$scope.AddBrand = function () {
			$scope.saving = true;
			$scope.tempArr = [];
			angular.forEach($scope.custom_fields, function (value) {
				if (value.type === 'input') {
					$scope.field_data = value.data;
				}
				if (value.type === 'textarea') {
					$scope.field_data = value.data;
				}
				if (value.type === 'date') {
					$scope.field_data = moment(value.data).format("YYYY-MM-DD");
				}
				if (value.type === 'select') {
					$scope.field_data = JSON.stringify(value.selected_opt);
				}
				$scope.tempArr.push({
					id: value.id,
					name: value.name,
					type: value.type,
					order: value.order,
					data: $scope.field_data,
					relation: value.relation,
					permission: value.permission,
				});
			});
			if (!$scope.brands) {
				var dataObj = $.param({
					name: '',
					manufacturer_id:''
				});
			} else {
				var dataObj = $.param({
					name: $scope.brands.name,
					manufacturer_id: $scope.brands.manufacturer_id,
				});
			}
			var posturl = BASE_URL + 'brands/create/';
			$http.post(posturl, dataObj, config)
				.then(
					function (response) {
						if (response.data.success == true) {
							if (response.data.id) {
								$http.get(BASE_URL + 'api/brands').then(function (Brands) {
									$scope.brands = Brands.data;
								});
								globals.mdToast('success', response.data.message);
								$mdSidenav('Create').close();							
							} else {
								$scope.saving = false;
								globals.mdToast('error', response.data.message);
							}
						} else {
							$scope.saving = false;
							globals.mdToast('error', response.data.message);
						}
					},
					function (response) {
						$scope.saving = false;
						$http.get(BASE_URL + 'api/brands').then(function (Brands) {
							$scope.brands = Brands.data;
						});
					}
				);
		};

		// Filtered Datas
		$scope.search = {
			name: '',
			manufacturer_id: '',
		};
	});

	$scope.UpdateBrand = function(id) {
		$scope.update = true;
		var dataObj = $.param({
			name: $scope.brand.name,
			manufacturer_id: $scope.brand.manufacturer_id,
		});
		$http.post(BASE_URL + 'brands/update_brand/' + id, dataObj, config)
		.then(
			function(response) {
				$scope.update = false;
				if (response.data.success == true) {
					$http.get(BASE_URL + 'api/brands').then(function (Brands) {
						$scope.brands = Brands.data;
					});
					$mdDialog.cancel();
					showToast(NTFTITLE, response.data.message, ' success');

				} else {
					$http.get(BASE_URL + 'api/brands').then(function (Brands) {
						$scope.brands = Brands.data;
					});
					showToast(NTFTITLE, response.data.message, ' danger');
				}
			},
			function(response) {
				$scope.update = false;
			}
			);
	};

	$scope.editBrand = function(brand) {
		$scope.brand = brand;
		$mdDialog.show({
			templateUrl: 'update_brand.html',
			scope: $scope,
			preserveScope: true,
			targetEvent: brand
		});
	};

	$scope.Delete = function (BRANDID) {
		globals.deleteDialog(lang.attention, lang.delete_brand, BRANDID, lang.doIt, lang.cancel, 'brands/remove/' + BRANDID, function(response) {
			if (response.success == true) {
				window.location.href = BASE_URL + 'brands';
			} else {
				globals.mdToast('error',response.message);
			}
		});
	};


}

LwCRM.controller('Brands_Controller', Brands_Controller);
