function Noorders_Controller($scope, $http, $mdDialog, $q, $timeout) {
	"use strict";

	$scope.get_staff();
	$scope.expensesLoader = true;
	$scope.from = new Date();
	$scope.to = new Date();


	var deferred = $q.defer();
	$scope.noorder_list = {
		order: '',
		limit: 10,
		page: 1
	};

	function getNoOrderlistInfo() {
	var dataObj = $.param({
		customer: '',
		salesman_id: '',
		beat: '',
		from: moment(new Date()).format("YYYY-MM-DD 00:00:00"),
		to: moment(new Date()).format("YYYY-MM-DD 23:59:59"),
	});

	var posturl = BASE_URL + 'noorder/getNoOrderlist';
	$http.post(posturl, dataObj, config).then(function (response) {
		if (response.data.success == true) {
			$scope.customersReport = 'false';
			$scope.noorders = response.data.result;
			$scope.noorder_limitOptions = [5, 10, 15, 20];
			if ($scope.noorders.length > 20) {
				$scope.cust_limitOptions = [5, 10, 15, 20, $scope.noorders.length];
			}
			$scope.noordersLoader = false;
	} else {
		$scope.noordersLoader = false;
		}
	});
	}

	getNoOrderlistInfo();

	$scope.refreshing = false;
	$scope.refreshTimeLogs = function() {
		$scope.refreshing = true;
		getNoOrderlistInfo();
	}

		

		$scope.viewReport = function() {
		$scope.loadingReport = true;
		var dataObj = $.param({
			customer: $scope.customer,
			salesman_id: $scope.salesman_id,
			beat: $scope.beat,
			from: $scope.from ? moment($scope.from).format("YYYY-MM-DD 00:00:00") : moment(new Date()).format("YYYY-MM-DD 00:00:00"),
			to: $scope.to ? moment($scope.to).format("YYYY-MM-DD 23:59:59") : moment(new Date()).format("YYYY-MM-DD 23:59:59"),
		});
		var config = {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
			}
		};
		
		var posturl = BASE_URL + 'noorder/getNoOrderlist';
			$http.post(posturl, dataObj, config).then(function (response) {
				if (response.data.success == true) {
					$scope.noorder_list = {
						order: '',
						limit: 10,
						page: 1
					};
					$scope.customersReport = 'false';
					$scope.noorders = response.data.result;
					$scope.noorder_limitOptions = [5, 10, 15, 20];
					if ($scope.noorders.length > 20) {
						$scope.cust_limitOptions = [5, 10, 15, 20, $scope.noorders.length];
					}
					$scope.noordersLoader = false;
					 
				} else {
					$scope.noordersLoader = false;
				}
			});
	}

	$scope.getStatus = function (status) 
	{
		if(status == '0')
            return 'Pending';
        else if(status == '1')
            return 'Delivered';
        else if(status == '2')
            return 'Confirmed';
        else if(status == '3')
            return 'Dispatched';
        else if(status == '4')
            return 'Cancel';
	};

	$scope.displayLocation = function(id) {
		var dataObj = $.param({
			noorderid : id
		});
        var posturl = BASE_URL + 'noorder/getLatLong';
        $http.post(posturl, dataObj, config)
            .then(
                function (response) {
                    if (response.data.success == true) {
                    	
                    	$mdDialog.show({
						templateUrl: 'displayLocation.html',
						clickOutsideToClose :true,
						onComplete: function(){
							var latitude = response.data.row.latitude;
                        	var longitude = response.data.row.longitude;
      						var myLatlng = new google.maps.LatLng(latitude,longitude);
                        	var mapOptions = {
                            	zoom: 10,
                           		center: myLatlng
                        	}
                        
                        	var map = new google.maps.Map(document.getElementById("map"), mapOptions);                		 
                        	var marker = new google.maps.Marker({
                            	position: myLatlng,
                            	map: map,
                            	draggable:false,
                            	anchorPoint: new google.maps.Point(0, -29),
                        	});
   							}
						});
                    	                    
                    } 
                }
            );
    };

    /*
    var coordinates = [latitude,longitude];
    $mdDialog.show({
                    		controller: function(scope) {
                      			scope.isRendered = true;
                    			},
                    			parent: angular.element(document.body),
                    		clickOutsideToClose: true,
                    		templateUrl: 'displayLocation.html',
                  		});
    function template(coordinates) {
              alert(coordinates);
              return '<md-dialog aria-label="Map">' +
                '<md-dialog-content style="max-width: 800px; max-height: 810px;">' +
                  '<ng-map ng-if="isRendered"' +
                    'style="height: 300px; width: 500px;"' +
                    'zoom="6"' +
                    'center=[40.74, -74.18]' +
                    'disable-default-u-i="true">' +
                    '<marker position="' + coordinates + '"></marker>' +
                  '</ng-map>' +
                '</md-dialog-content>' +
              '</md-dialog>';
            }*/

}

LwCRM.controller('Noorders_Controller', Noorders_Controller);

