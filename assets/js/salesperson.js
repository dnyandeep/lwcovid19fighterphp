function Location_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, $compile, fileUpload) {
	"use strict";
	$scope.isLoading = true;
	$http.get(BASE_URL + 'staff/get_staff_by_role/4/currentlocation').then(function (Salesperson) {
			$scope.salesperson = Salesperson.data;
			$scope.isLoading = false;
		});

    // global variable for map
    var addressarr=[];
    var contentStringMiddelarr=[];
    var latmidarr = [];
    var longmidarr = [];
    var name = [];
    var status = [];
    var mobile = [];
    var timemidarr=[];
    var midmarkercount = 0;
    var map ;
    var salesfname;
    var saleslname;
    var companyname;

	function split_string(d)
    {
        var res = d.toString().split(" ");
        var q=res[4].split(':');
        return res[2]+'-'+res[1]+'-'+res[3]+' '+q[0]+':'+q[1]+':'+q[2];
    }

    function getAddressbyLatLong(latitude, longitude,ind,timestmp,name,status,mobile){
        var currentAddress = '';
        var markerindex = ind+2;
        currentAddress = 'unknown address :('+latitude+','+ longitude+')';
            var contentStringMiddel = '<div id="content" style="width:250px;">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<div id="bodyContent">'+
                '<p> Status : '+ status + 
                '</br>Name : '+name+
                '<br/> Mobile : '+ mobile +
                '</br>Time : '+split_string(timestmp)+
                '</p>'+
                '</div>'+
                '</div>';
            var myLatlng = new google.maps.LatLng(latitude,longitude);
            var markermiddle = new google.maps.Marker({
                position: myLatlng,
                map: map
            });  
                    
            markermiddle.addListener('click', function() {
                var infowindowmiddel = new google.maps.InfoWindow({
                    content: contentStringMiddel
                 });
            infowindowmiddel.open(map, markermiddle);
            });
        return currentAddress;
    }

	$scope.getCurrentLocation = function () {
       
        var dataObj = $.param({
			salesperson_id: $scope.salesperson.id
		});
        if($scope.salesperson.id == 'all')
        {
            var posturl = BASE_URL + 'salesperson/getLatLongOfAllSalespersons';
            $http.post(posturl, dataObj, config)
            .then(
                function (response) {
                    if (response.data.success == true) {
                        //var map;
                        var i,j;
                        var length = response.data.length;
             
                        for( i = 0; i < length; i++ ) 
                        {
                            if(i==0)
                            {
                                var firstlat=response.data.row[i].latitude;
                                var firstlon=response.data.row[i].longitude;
                                map = new google.maps.Map(document.getElementById("map"), {
                                center: new google.maps.LatLng(firstlat, firstlon),
                                zoom: 8,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                                });
                            }
                            else
                            {
                                var lat=response.data.row[i].latitude;
                                var lon=response.data.row[i].longitude;
                                var timestm = moment(response.data.row[i].date);

                                latmidarr[midmarkercount] = lat;
                                longmidarr[midmarkercount] = lon;
                                timemidarr[midmarkercount]= timestm;
                                midmarkercount++; 
                            }
                            var oldstatus = response.data.row[i].status=='1' ? 'Online' : 'Offline';
                            name[midmarkercount]= response.data.row[i].staffname;
                            status[midmarkercount]= oldstatus;
                            mobile[midmarkercount]= response.data.row[i].phone;
                        }

                        for(var j=0;j<midmarkercount;j++)
                        {
                            var midlat = latmidarr[j] ;
                            var midlon = longmidarr[j] ;
                            var midtime = timemidarr[j];
                            var sname = name[j];
                            var sstatus = status[j];
                            var smobile = mobile[j];
                            getAddressbyLatLong(midlat,midlon,j,midtime,sname,sstatus,smobile);
                
                        }

                    } else {
                        globals.mdToast('error', response.data.message );
                        $('#ulabel').hide();
                        $('#slabel').hide();
                        $('#time').hide();
                        $('#status').hide();
                        $('#map').hide();
                    }
                }
                
            );
        }
        else
        {
            var posturl = BASE_URL + 'salesperson/getLatLong';
        $http.post(posturl, dataObj, config)
            .then(
                function (response) {
                    if (response.data.success == true) {
                        var status = response.data.row.status=='1' ? 'Online' : 'Offline';
                        var time = moment(response.data.row.date);
                        var latitude = response.data.row.latitude;
                        var longitude = response.data.row.longitude;
                        $('#ulabel').show();
                        $('#slabel').show();
                        document.getElementById("time").innerHTML =split_string(time);
                        document.getElementById("status").innerHTML =status;
                        var myLatlng = new google.maps.LatLng(latitude,longitude);
                        var mapOptions = {
                            zoom: 18,
                            center: myLatlng
                        }
                        $('#time').show();
                        $('#status').show();
                        $('#map').show();
                        var map = new google.maps.Map(document.getElementById("map"), mapOptions);

                        var contentString = '<div id="content">'+
                        '<div id="siteNotice">'+
                        '</div>'+
                        '<div id="bodyContent">'+
                        '<p> Status : '+ status + 
                        '<br/>Signal Time :'+split_string(time)+
                        '</p>'+
                        '</div>'+
                        '</div>';
                        var infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });
                        
                        var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            draggable:false,
                            anchorPoint: new google.maps.Point(0, -29),
                        });
                        marker.addListener('click', function() {
                            infowindow.open(map, marker);
                        });
                    
                        setTimeout(function() { check($scope.salesperson.id); } ,300000); 
                        
                    } else {
                        globals.mdToast('error', response.data.message );
                        $('#ulabel').hide();
                        $('#slabel').hide();
                        $('#time').hide();
                        $('#status').hide();
                        $('#map').hide();
                    }
                }
            );
        }
		
	};

	function check(salesman_id)
	{
		var dataObj = $.param({
			salesperson_id: $scope.salesperson.id
		});
		var posturl = BASE_URL + 'salesperson/getLatLong';
		$http.post(posturl, dataObj, config)
			.then(
				function (response) {
					if (response.data.success == true) {
						var status = response.data.row.status=='1' ? 'Online' : 'Offline';
						var time = moment(response.data.row.date);
						var latitude = response.data.row.latitude;
						var longitude = response.data.row.longitude;
						$('#ulabel').show();
                    	$('#slabel').show();
                    	document.getElementById("time").innerHTML =split_string(time);
                    	document.getElementById("status").innerHTML =status;
                    	var myLatlng = new google.maps.LatLng(latitude,longitude);
                    	var mapOptions = {
                    		zoom: 18,
                    		center: myLatlng
                    	}
                    	var map = new google.maps.Map(document.getElementById("map"), mapOptions);

						var contentString = '<div id="content">'+
                    	'<div id="siteNotice">'+
                    	'</div>'+
                    	'<div id="bodyContent">'+
                    	'<p> Status : '+ status + 
                    	'<br/>Signal Time :'+split_string(time)+
                    	'</p>'+
                    	'</div>'+
                    	'</div>';
                    	var infowindow = new google.maps.InfoWindow({
                        	content: contentString
                    	});
                    	
                    	var marker = new google.maps.Marker({
                    		position: myLatlng,
                    		map: map,
                    		draggable:false,
                    		anchorPoint: new google.maps.Point(0, -29),
                    	});
                   		marker.addListener('click', function() {
                        	infowindow.open(map, marker);
                    	});
                    
                    	setTimeout(function() { check($scope.salesperson.id); } ,300000); 
						
					} else {
						globals.mdToast('error', response.data.message );
					}
				}
			);
	}

}


function TrackSalesperson_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, $compile, fileUpload, $window) {
    "use strict";
    $scope.isLoading = true;
    $http.get(BASE_URL + 'staff/get_staff_by_role/4').then(function (Salesperson) {
            $scope.salesperson = Salesperson.data;
            $scope.isLoading = false;
        });

    $scope.track_list = {
        order: '',
        limit: 10,
        page: 1
    };

    $scope.getTrackData = function () {
            $scope.tempArr = [];
            var fromdate = '', from_date = '', todate = '', to_date = '';
            if ($scope.from_date) {
                fromdate = moment($scope.from_date).format("YYYY-MM-DD");
            }

            if ($scope.to_date) {
                todate = moment($scope.to_date).format("YYYY-MM-DD");
            }

            if (!$scope.salesperson) {
                var dataObj = $.param({
                    id: '',
                    from_date: '',
                    to_date: ''
                });
            } else {
                var dataObj = $.param({
                    id: $scope.salesperson.id,
                    from_date: fromdate,
                    to_date: todate
                });
            }
            var posturl = BASE_URL + 'salesperson/getXmldatabetweendate/';
            $http.post(posturl, dataObj, config)
                .then(
                    function (Tracks) {
                        if(Tracks.data.success)
                        {
                            $scope.tracks = Tracks.data.track;
                            $scope.limitOptions = [10, 15, 20];
                            if ($scope.tracks.length > 20) {
                                $scope.limitOptions = [10, 15, 20, $scope.tracks.length];
                            }
                        }
                        else {
                            globals.mdToast('error', Tracks.data.message);
                        }
                        
                    }
                    
                );
        };

    $scope.displayMap = function(id) {
        $window.open(BASE_URL + 'salesperson/displayMap/'+id, 'C-Sharpcorner', 'width=1400,height=900');
    };   

}

function Popupview_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, $compile, fileUpload) {
    "use strict";

    var addressarr=[];
    var contentStringMiddelarr=[];
    var latmidarr = [];
    var longmidarr = [];
    var timemidarr=[];
    var midmarkercount = 0;
    var map ;
    var salesfname;

    function doSomething() {
   //do whatever you want here
    }

    function getAddressbyLatLong(latitude, longitude,ind,timestmp){

    var reverseGeocoder = new google.maps.Geocoder();
    var currentPosition = new google.maps.LatLng(latitude, longitude);
    var currentAddress = '';
    setTimeout(doSomething, 500);
    var markerindex = ind+2;

    reverseGeocoder.geocode({'latLng': currentPosition}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                currentAddress = results[0].formatted_address;
                var contentStringMiddel = '<div id="content" style="width:250px;">'+
                    '<div id="siteNotice">'+
                    '</div>'+
                    '<div id="bodyContent">'+
                    '<p>Waypoint #  : '+ ind +
                    '</br>Address : '+ currentAddress+
                    '</br>Salesman : '+salesfname+
                    '</br>Time : '+new Date(timestmp)+
                    '</p>'+
                    '</div>'+
                    '</div>';
                var myLatlng = new google.maps.LatLng(latitude,longitude);
                var markermiddle = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    icon:'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_blue'+markerindex+'.png',
                });  
                
                markermiddle.addListener('click', function() {
                    var infowindowmiddel = new google.maps.InfoWindow({
                        content: contentStringMiddel
                    });
                    infowindowmiddel.open(map, markermiddle);
                });
            }
            else 
            {
                // alert('Unable to detect your address.1');
            }
        } else {
            currentAddress = 'unknown address :('+latitude+','+ longitude+')';
                var contentStringMiddel = '<div id="content" style="width:250px;">'+
                    '<div id="siteNotice">'+
                    '</div>'+
                    '<div id="bodyContent">'+
                    '<p>Waypoint #  : '+ ind +
                    '</br>Address : '+ currentAddress+
                    '</br>Salesman : '+salesfname+
                    '</br>Time : '+new Date(timestmp)+
                    '</p>'+
                    '</div>'+
                    '</div>';
                    var myLatlng = new google.maps.LatLng(latitude,longitude);
                    var markermiddle = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        icon:'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_blue'+markerindex+'.png',
                    });  
                    
                    markermiddle.addListener('click', function() {
                        var infowindowmiddel = new google.maps.InfoWindow({
                            content: contentStringMiddel
                        });
                        infowindowmiddel.open(map, markermiddle);
                    });
                }
        });
        return currentAddress;
    }

   angular.element(document).ready(function () {

        function is_date(d) {
            if (d == '0000-00-00 00:00:00') {
                return '<div class="text-center"><span class="row_status ">---</span></div>';
            } else{
                var s= d.split(' ');
                var p=s[0].split('-');
                var q=s[1].split(':');
                return p[2]+'-'+p[1]+'-'+p[0]+' '+q[0]+':'+q[1]+':'+q[2];
            }
        }

        var data = xmldata;
        var JSONObject = JSON.parse(xmldata);
        var firstlat=JSONObject['ob']['tr']['trs']['trpt'][0]['@attributes']['lat'];
            var firstlon=JSONObject['ob']['tr']['trs']['trpt'][0]['@attributes']['lon'];
            map = new google.maps.Map(document.getElementById("us3"), {
                center: new google.maps.LatLng(firstlat, firstlon),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var count = Object.keys(JSONObject['ob']['tr']['trs']['trpt']).length;
            var result=[];
            var locations=[];
            var intervalrate = 60;
            
            salesfname = JSONObject["staffname"];
            
            for (var i = 0, length = count; i < length; i++) {

                var lat=JSONObject['ob']['tr']['trs']['trpt'][i]['@attributes']['lat'];
                var lon=JSONObject['ob']['tr']['trs']['trpt'][i]['@attributes']['lon'];
                if(!(i==0 || i==(length-1))  && (i % intervalrate)== '0')
                {  

                    var timestm = JSONObject['ob']['tr']['trs']['trpt'][i]['t'];
                    //geocoder = new google.maps.Geocoder();

                    latmidarr[midmarkercount] = lat;
                    longmidarr[midmarkercount] = lon;
                    timemidarr[midmarkercount]= timestm;
                    midmarkercount++;                 
                }//end interval marker logic


                if(i==0 || i==(length-1))
                {
                  var myLatlng = new google.maps.LatLng(lat,lon);
                  if(i==0){
                    var contentStringStart = '<div id="content" style="width:250px;">'+
                        '<div id="siteNotice">'+
                        '</div>'+
                        '<div id="bodyContent">'+
                        '<p>Start Address : '+JSONObject["start_address"]+
                        '</br>Salesman : '+JSONObject["staffname"]+
                        '</br>Time : '+is_date(JSONObject["start_time"])+
                        '</p>'+
                        '</div>'+
                        '</div>';
                    var infowindowstart = new google.maps.InfoWindow({
                        content: contentStringStart
                    });
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        icon:'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_green1.png',
                    });  
                    
                        marker.addListener('click', function() {
                        infowindowstart.open(map, marker);
                        });
                    
                    }else{
                        //alert("last i"+i);
                        var lastmarkcnt = midmarkercount+2;
                        var contentStringEnd = '<div id="content" style="width:250px;">'+
                        '<div id="siteNotice">'+
                        '</div>'+
                        '<div id="bodyContent">'+
                        '<p>End Address : '+JSONObject["end_address"]+
                        '</br>Salesman : '+JSONObject["staffname"]+
                        '</br>Time : '+is_date(JSONObject["end_time"])+
                        '</br>Distance : '+JSONObject["distance"]+' Km'
                        '</p>'+
                        '</div>'+
                        '</div>';
                        var infowindow = new google.maps.InfoWindow({
                            content: contentStringEnd
                        });
                        var markerend = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            icon:'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_red'+lastmarkcnt+'.png',
                        });  
                        
                            markerend.addListener('click', function() {
                            infowindow.open(map, markerend);
                            });
                        
                    }
             
                }
                result.push({
                    lat: parseFloat(lat),
                    lng: parseFloat(lon),
                }); 
            }
            var flightPath = new google.maps.Polyline({
                path: result,
                geodesic: true,
                strokeColor: '#4988ed',
                strokeOpacity: 1.0,
                strokeWeight: 7
            });
            flightPath.setMap(map);

            for(var j=0;j<midmarkercount;j++){
                  var midlat = latmidarr[j] ;
                   var midlon = longmidarr[j] ;
                    var midtime = timemidarr[j];
                    setTimeout(doSomething, 500);
                getAddressbyLatLong(midlat,midlon,j,midtime);
                
            }
        
    });

}

LwCRM.controller('Location_Controller', Location_Controller);
LwCRM.controller('TrackSalesperson_Controller', TrackSalesperson_Controller);
LwCRM.controller('Popupview_Controller', Popupview_Controller);

