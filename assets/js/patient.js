function Patient_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, fileUpload, $q) {
	"use strict";


	$scope.reEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	$scope.ph_numbr = /^\+?\d{10}$/;
	$scope.aadharno = /^\+?\d{12}$/;
	$scope.pincode = /^\+?\d{6}$/;
	$scope.submitted=false;
	$scope.maxDate=new Date();
	$scope.personUpdate={};
	

	$scope.setsubmitted=function()
	{
		$scope.submitted=true;
		//console.log("submitted "+submitted)
		return true;
	};

	$scope.is_covid19_confirmeds=
	[
	{ id: 'Y', name: 'Yes' },
	{ id: 'N', name: 'No' }

	];


	$scope.is_suspect_patients=
	[
	{ id: 'S', name: 'Suspect' },
	{ id: 'P', name: 'Patient' }

	];

	$scope.genders=[
	{ id: 'M', name: 'Male' },
	{ id: 'F', name: 'Female' },
	{ id: 'O', name: 'Others' }
	];

	$scope.citizen_types=[
	{ id: 'indian', name: 'Indian' },
	{ id: 'foreigner', name: 'Foreigner' },
	{ id: 'NRI', name: 'NRI' }
	];

	$scope.calculateAge=function(date)
	{
		//console.log("in calculateAge "+date)

	};
	$http.get(BASE_URL + 'rest_api/Commonapi/getAllStates').then(function (states) {
		$scope.states = states.data.data;
		//console.log(states.data.data);

	});

	$http.get(BASE_URL + 'rest_api/Commonapi/getAllCountries').then(function (countries) {
		$scope.countries = countries.data.data;
		//console.log(countries.data.data);

	});

	$scope.stateChange=function(data)
	{

		//console.log("state "+data)
		var id=0;
		if(isNaN(data.id))
		{
			id=data;
		}
		else
		{
			id=data.id;
		}

		$http.get(BASE_URL + 'rest_api/Commonapi/getAllDistrictsByState/'+id).then(function (districts) {
			$scope.districts = districts.data.data;
			//console.log(districts.data.data);

		});


	};

	/*$http.get(BASE_URL + 'rest_api/Commonapi/getAllDistricts').then(function (districts) {
		$scope.districts = districts.data.data;
		//console.log("districts ## "+districts.data);

	});*/

	$scope.districts ={};

	$scope.personsLoader = true;
	$scope.Create = buildToggler('Create');	

	function buildToggler(navID) {
		return function () {
			//$scope.person={};
			$mdSidenav(navID).toggle();

		};
	}

	$scope.close = function () {
		$mdSidenav('Create').close();
		$scope.person={};
		$mdDialog.hide();
	};

	var deferred = $q.defer();
	$scope.person_list = {
		order: '',
		limit: 5,
		page: 1
	};

	$http.get(BASE_URL + 'rest_api/PersonAPI/getAllPersonDetails').then(function (persons) {
		//console.log(persons.data);
		$scope.persons = persons.data.data;
		//console.log($scope.persons);

		$scope.limitOptions = [5, 10, 15, 20];
		if($scope.persons.length > 20 ) {
			$scope.limitOptions = [5, 10, 15, 20, $scope.persons.length];
		}
		$scope.personsLoader = false;

		$scope.saving = false;

		$scope.AddPerson = function () {
			$scope.saving = true;


			if (!$scope.person) {
				var dataObj = $.param({
					id:' ',
					fname:' ', 
					mname:' ', 
					lname:' ', 
					device_id:' ', 
					username:' ', 
					PASSWORD:' ', 
					mobile_no:' ', 
					alternate_no:' ', 
					whatsapp_no:' ', 
					email:' ', 
					gender:' ', 
					dob:' ', 
					age:' ', 
					aadhar_no:' ', 
					nationality:' ', 
					citizen_type:' ', 
					state:' ', 
					district_id:' ', 
					tahsil_name:' ', 
					res_country_id:' ', 
					travel_history:' ', 
					last_travel_country_id:' ', 
					date_of_arrival_india:' ', 
					passport_no:' ', 
					is_covid19_confirmed:' ', 
					is_suspect_patient:' ', 
					pincode:' ', 
					address1:' ', 
					address2:' '
				});
			} else {
				
				var arrivalDate   = '';
				if($scope.person.date_of_arrival_india instanceof Date)
				{
					arrivalDate=moment($scope.person.date_of_arrival_india).format("YYYY-MM-DD");
				}
				else
				{
					arrivalDate='';
				}
					//console.log("arrivalDate "+arrivalDate);
					var dataObj = $.param({
						id:'',
						fname:$scope.person.fname, 
						mname:$scope.person.mname, 
						lname:$scope.person.lname, 
						device_id:$scope.person.device_id, 
						username:$scope.person.username, 
						password:$scope.person.password, 
						mobile_no:$scope.person.mobile_no, 
						alternate_no:$scope.person.alternate_no, 
						whatsapp_no:$scope.person.whatsapp_no, 
						email:$scope.person.email, 
						gender:$scope.person.gender.id, 
						dob:moment($scope.person.dob).format("YYYY-MM-DD"), 
						age:$scope.person.age, 
						aadhar_no:$scope.person.aadhar_no, 
						nationality:$scope.person.nationality, 
						citizen_type:$scope.person.citizen_type.id, 
						state:$scope.person.state.id, 
						district_id:$scope.person.district_id.id, 
						tahsil_name:$scope.person.tahsil_name, 
						res_country_id:$scope.person.res_country_id.countries_id, 
						travel_history:$scope.person.travel_history, 
						last_travel_country_id:$scope.person.last_travel_country_id.countries_id, 
						date_of_arrival_india:arrivalDate, 
						passport_no:$scope.person.passport_no, 
						is_covid19_confirmed:$scope.person.is_covid19_confirmed.id, 
						is_suspect_patient:$scope.person.is_suspect_patient.id, 
						pincode:$scope.person.pincode, 
						address1:$scope.person.address1, 
						address2:$scope.person.address2,
						token:"LWCOVID19"
					});


				}
			//console.log("DOB "+$scope.person.dob);
			var posturl = BASE_URL + 'rest_api/PersonAPI/addQuarantinedPerson';
			$http.post(posturl, dataObj, config)
			.then(
				function (response) {
					console.log(response);
					if (response.data.success == true) {
						
						$http.get(BASE_URL + 'rest_api/PersonAPI/getAllPersonDetails').then(function (persons) {
							$scope.persons = persons.data.data;
						});
						globals.mdToast('success', response.data.message);
						$mdSidenav('Create').close();		
						$scope.saving = false;					
						
					} else {
						$scope.saving = false;
						globals.mdToast('error', response.data.message);
					}
				},
				function (response) {
					$scope.saving = false;
					$http.get(BASE_URL + 'rest_api/PersonAPI/getAllPersonDetails').then(function (persons) {
						$scope.persons = persons.data.data;
					});
				}
				);
		};

	// Filtered Datas
	$scope.search = {
		fname: ''
	};
});

	$scope.UpdatePerson = function(id) {
		$scope.update = true;

		var arrivalDate   = '';
				if($scope.person.date_of_arrival_india instanceof Date)
				{
					arrivalDate=moment($scope.person.date_of_arrival_india).format("YYYY-MM-DD");
				}
				else
				{
					arrivalDate='';
				}

		var dataObj = $.param({
			id:id,
			fname:$scope.person.fname, 
			mname:$scope.person.mname, 
			lname:$scope.person.lname, 
			device_id:$scope.person.device_id, 
			username:$scope.person.username, 
			password:$scope.person.password, 
			mobile_no:$scope.person.mobile_no, 
			alternate_no:$scope.person.alternate_no, 
			whatsapp_no:$scope.person.whatsapp_no, 
			email:$scope.person.email, 
			gender:$scope.person.gender, 
			dob:moment($scope.person.dob).format("YYYY-MM-DD"), 
			age:$scope.person.age, 
			aadhar_no:$scope.person.aadhar_no, 
			nationality:$scope.person.nationality, 
			citizen_type:$scope.person.citizen_type, 
			state:$scope.person.state, 
			district_id:$scope.person.district_id, 
			tahsil_name:$scope.person.tahsil_name, 
			res_country_id:$scope.person.res_country_id, 
			travel_history:$scope.person.travel_history, 
			last_travel_country_id:$scope.person.last_travel_country_id, 
			date_of_arrival_india:arrivalDate, 
			passport_no:$scope.person.passport_no, 
			is_covid19_confirmed:$scope.person.is_covid19_confirmed, 
			is_suspect_patient:$scope.person.is_suspect_patient, 
			pincode:$scope.person.pincode, 
			address1:$scope.person.address1, 
			address2:$scope.person.address2,
			token:"LWCOVID19"
		});
		$http.post(BASE_URL + 'rest_api/PersonAPI/addQuarantinedPerson/' + id, dataObj, config)
		.then(
			function(response) {
				$scope.update = false;
				if (response.data.success == true) {
					$mdDialog.cancel();
					$http.get(BASE_URL + 'rest_api/PersonAPI/getAllPersonDetails').then(function (persons) {
						$scope.persons = persons.data.data;
					});
					showToast(NTFTITLE, response.data.message, ' success');
				} else {
					$http.get(BASE_URL + 'rest_api/PersonAPI/getAllPersonDetails').then(function (persons) {
						$scope.persons = persons.data.data;
					});
					showToast(NTFTITLE, response.data.message, ' danger');
				}
			},
			function(response) {
				$scope.update = false;
			}
			);
	};

	$scope.editPerson = function(person) {
		$scope.person = person;
		$scope.submitted = false;
		if($scope.person.dob instanceof Date)
		{
			$scope.person.dob=new Date($scope.person.dob);	
		}

		if($scope.person.date_of_arrival_india instanceof Date)
		{
			$scope.person.date_of_arrival_india=new Date($scope.person.date_of_arrival_india);	
		}
		$http.get(BASE_URL + 'rest_api/Commonapi/getAllDistrictsByState/'+person.state).then(function (districts) {
			$scope.districts = districts.data.data;
			//console.log(districts.data.data);

			$mdDialog.show({
				templateUrl: 'update_person.html',
				scope: $scope,
				preserveScope: true,
				targetEvent: person
			});

		});

		
	};

	$scope.Delete = function (MANFID) {
		globals.deleteDialog(lang.attention, lang.delete_area, MANFID, lang.doIt, lang.cancel, 'manufacturer/remove/' + MANFID, function(response) {
			if (response.success == true) {
				window.location.href = BASE_URL + 'manufacturer';
			} else {
				globals.mdToast('error',response.message);
			}
		});
	};


}

LwCRM.controller('Patient_Controller', Patient_Controller);
