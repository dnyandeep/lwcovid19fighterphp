function Area_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, fileUpload, $q) {
	"use strict";

	$http.get(BASE_URL + 'api/custom_fields_by_type/' + 'customer').then(function (custom_fields) {
		$scope.all_custom_fields = custom_fields.data;
		$scope.custom_fields = $filter('filter')($scope.all_custom_fields, {
			active: 'true',
		});
	});

	$scope.customersLoader = true;
	$scope.isContact = true;
	$scope.Create = buildToggler('Create');	

	function buildToggler(navID) {
		return function () {
			$mdSidenav(navID).toggle();

		};
	}

	$scope.close = function () {
		$mdSidenav('Create').close();
		$mdDialog.hide();
	};
	
	var deferred = $q.defer();
	$scope.area_list = {
		order: '',
		limit: 5,
		page: 1
	};
	
	$http.get(BASE_URL + 'api/areas').then(function (Areas) {
		$scope.areas = Areas.data;

		$scope.limitOptions = [5, 10, 15, 20];
		if($scope.areas.length > 20 ) {
			$scope.limitOptions = [5, 10, 15, 20, $scope.areas.length];
		}
		$scope.areasLoader = false;


		$scope.isIndividual = false;
		$scope.saving = false;

		$scope.AddArea = function () {
			$scope.saving = true;
			$scope.tempArr = [];
			angular.forEach($scope.custom_fields, function (value) {
				if (value.type === 'input') {
					$scope.field_data = value.data;
				}
				if (value.type === 'textarea') {
					$scope.field_data = value.data;
				}
				if (value.type === 'date') {
					$scope.field_data = moment(value.data).format("YYYY-MM-DD");
				}
				if (value.type === 'select') {
					$scope.field_data = JSON.stringify(value.selected_opt);
				}
				$scope.tempArr.push({
					id: value.id,
					name: value.name,
					type: value.type,
					order: value.order,
					data: $scope.field_data,
					relation: value.relation,
					permission: value.permission,
				});
			});
			if (!$scope.area) {
				var dataObj = $.param({
					city: '',
					bit: '',
					distributor_email:'',
					day: ''
				});
			} else {
				var dataObj = $.param({
					city: $scope.area.city,
					bit: $scope.area.bit,
					distributor_email: $scope.area.distributor_email,
					day: $scope.area.day,
				});
			}
			var posturl = BASE_URL + 'area/create/';
			$http.post(posturl, dataObj, config)
				.then(
					function (response) {
						if (response.data.success == true) {
							if (response.data.id) {
								$http.get(BASE_URL + 'api/areas').then(function (Areas) {
									$scope.areas = Areas.data;
								});
								globals.mdToast('success', response.data.message);
								$mdSidenav('Create').close();							
							} else {
								$scope.saving = false;
								globals.mdToast('error', response.data.message);
							}
						} else {
							$scope.saving = false;
							globals.mdToast('error', response.data.message);
						}
					},
					function (response) {
						$scope.saving = false;
						$http.get(BASE_URL + 'api/areas').then(function (Areas) {
							$scope.areas = Areas.data;
						});
					}
				);
		};

		// Filtered Datas
		$scope.search = {
			name: '',
			phone: '',
			email: '',
		};
	});

	$scope.UpdateArea = function(id) {
		$scope.update = true;
		var dataObj = $.param({
			city: $scope.area.city,
			bit: $scope.area.bit,
			distributor_email: $scope.area.distributor_email,
			day: $scope.area.day ? $scope.area.day : null
		});
		$http.post(BASE_URL + 'area/update_area/' + id, dataObj, config)
		.then(
			function(response) {
				$scope.update = false;
				if (response.data.success == true) {
					$mdDialog.cancel();
					showToast(NTFTITLE, response.data.message, ' success');
				} else {
					showToast(NTFTITLE, response.data.message, ' danger');
				}
			},
			function(response) {
				$scope.update = false;
			}
			);
	};

	$scope.editArea = function(area) {
		$scope.area = area;
		$mdDialog.show({
			templateUrl: 'update_area.html',
			scope: $scope,
			preserveScope: true,
			targetEvent: area
		});
	};

	$scope.Delete = function (AREAID) {
		globals.deleteDialog(lang.attention, lang.delete_area, AREAID, lang.doIt, lang.cancel, 'area/remove/' + AREAID, function(response) {
			if (response.success == true) {
				window.location.href = BASE_URL + 'area';
			} else {
				globals.mdToast('error',response.message);
			}
		});
	};


}

LwCRM.controller('Area_Controller', Area_Controller);
