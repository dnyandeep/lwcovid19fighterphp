function Units_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, fileUpload, $q) {
	"use strict";

	$http.get(BASE_URL + 'units/getBaseUnits').then(function (BaseUnits) {
		$scope.baseunits = BaseUnits.data;
	});

	$http.get(BASE_URL + 'api/custom_fields_by_type/' + 'units').then(function (custom_fields) {
		$scope.all_custom_fields = custom_fields.data;
		$scope.custom_fields = $filter('filter')($scope.all_custom_fields, {
			active: 'true',
		});
	});

	$scope.unitsLoader = true;
	$scope.Create = buildToggler('Create');	

	function buildToggler(navID) {
		return function () {
			$mdSidenav(navID).toggle();

		};
	}

	$scope.close = function () {
		$mdSidenav('Create').close();
		$mdDialog.hide();
	};
	
	var deferred = $q.defer();
	$scope.unit_list = {
		order: '',
		limit: 10,
		page: 1
	};
	
	$http.get(BASE_URL + 'api/units').then(function (Units) {
		$scope.units = Units.data;

		$scope.limitOptions = [5, 10, 15, 20];
		if($scope.units.length > 20 ) {
			$scope.limitOptions = [5, 10, 15, 20, $scope.units.length];
		}
		$scope.unitsLoader = false;

		$scope.saving = false;

		$scope.AddUnit = function () {
			$scope.saving = true;
			$scope.tempArr = [];
			angular.forEach($scope.custom_fields, function (value) {
				if (value.type === 'input') {
					$scope.field_data = value.data;
				}
				if (value.type === 'textarea') {
					$scope.field_data = value.data;
				}
				if (value.type === 'date') {
					$scope.field_data = moment(value.data).format("YYYY-MM-DD");
				}
				if (value.type === 'select') {
					$scope.field_data = JSON.stringify(value.selected_opt);
				}
				$scope.tempArr.push({
					id: value.id,
					name: value.name,
					type: value.type,
					order: value.order,
					data: $scope.field_data,
					relation: value.relation,
					permission: value.permission,
				});
			});
			if (!$scope.units) {
				var dataObj = $.param({
					name: '',
					base_unit:''
				});
			} else {
				var dataObj = $.param({
					name: $scope.units.name,
					base_unit: $scope.units.base_unit,
				});
			}
			var posturl = BASE_URL + 'units/create/';
			$http.post(posturl, dataObj, config)
				.then(
					function (response) {
						if (response.data.success == true) {
							if (response.data.id) {
								$http.get(BASE_URL + 'api/units').then(function (Units) {
									$scope.units = Units.data;
								});
								globals.mdToast('success', response.data.message);
								$mdSidenav('Create').close();							
							} else {
								$scope.saving = false;
								globals.mdToast('error', response.data.message);
							}
						} else {
							$scope.saving = false;
							globals.mdToast('error', response.data.message);
						}
					},
					function (response) {
						$scope.saving = false;
						$http.get(BASE_URL + 'api/units').then(function (Units) {
							$scope.units = Units.data;
						});
					}
				);
		};

		// Filtered Datas
		$scope.search = {
			name: '',
			base_unit: '',
		};
	});

	$scope.UpdateUnit = function(id) {
		$scope.update = true;
		var dataObj = $.param({
			name: $scope.unit.name,
			base_unit: $scope.unit.base_unit,
		});
		$http.post(BASE_URL + 'units/update_unit/' + id, dataObj, config)
		.then(
			function(response) {
				$scope.update = false;
				if (response.data.success == true) {
					$http.get(BASE_URL + 'api/units').then(function (Units) {
						$scope.units = Units.data;
					});
					$mdDialog.cancel();
					showToast(NTFTITLE, response.data.message, ' success');

				} else {
					$http.get(BASE_URL + 'api/units').then(function (Units) {
						$scope.units = Units.data;
					});
					showToast(NTFTITLE, response.data.message, ' danger');
				}
			},
			function(response) {
				$scope.update = false;
			}
			);
	};

	$scope.editUnit = function(unit) {
		$scope.unit = unit;
		$mdDialog.show({
			templateUrl: 'update_manufacturer.html',
			scope: $scope,
			preserveScope: true,
			targetEvent: unit
		});
	};

	$scope.Delete = function (UNITID) {
		globals.deleteDialog(lang.attention, lang.delete_area, UNITID, lang.doIt, lang.cancel, 'units/remove/' + UNITID, function(response) {
			if (response.success == true) {
				window.location.href = BASE_URL + 'units';
			} else {
				globals.mdToast('error',response.message);
			}
		});
	};


}

LwCRM.controller('Units_Controller', Units_Controller);
