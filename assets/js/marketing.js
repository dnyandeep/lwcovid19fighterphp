
function Adv_Material_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, fileUpload, $q) {
	"use strict";

	$http.get(BASE_URL + 'units/getAllUnits').then(function (Units) {
		$scope.units = Units.data;
	});

	$http.get(BASE_URL + 'api/custom_fields_by_type/' + 'brands').then(function (custom_fields) {
		$scope.all_custom_fields = custom_fields.data;
		$scope.custom_fields = $filter('filter')($scope.all_custom_fields, {
			active: 'true',
		});
	});

	$scope.materialsLoader = true;
	$scope.Create = buildToggler('Create');	

	function buildToggler(navID) {
		return function () {
			$mdSidenav(navID).toggle();

		};
	}

	$scope.close = function () {
		$mdSidenav('Create').close();
		$mdDialog.hide();
	};
	
	var deferred = $q.defer();
	$scope.material_list = {
		order: '',
		limit: 10,
		page: 1
	};
	
	$http.get(BASE_URL + 'api/advertising_material').then(function (Materials) {
		$scope.materials = Materials.data;

		$scope.limitOptions = [5, 10, 15, 20];
		if($scope.materials.length > 20 ) {
			$scope.limitOptions = [5, 10, 15, 20, $scope.materials.length];
		}
		$scope.materialsLoader = false;

		$scope.saving = false;

		$scope.AddAdvMaterial = function() {
		$scope.saving = true;
		$scope.tempArr = [];
		$scope.uploading = true;
		angular.forEach($scope.custom_fields, function (value) {
				if (value.type === 'input') {
					$scope.field_data = value.data;
				}
				if (value.type === 'textarea') {
					$scope.field_data = value.data;
				}
				if (value.type === 'date') {
					$scope.field_data = moment(value.data).format("YYYY-MM-DD");
				}
				if (value.type === 'select') {
					$scope.field_data = JSON.stringify(value.selected_opt);
				}
				$scope.tempArr.push({
					id: value.id,
					name: value.name,
					type: value.type,
					order: value.order,
					data: $scope.field_data,
					relation: value.relation,
					permission: value.permission,
				});
			});
		
		if (!$scope.materials) {
			var dataObj = {
				name: '',
				unit_id: '',
				category: '',
				file: '',
				description:''
			};
		} else {
			var data = tinyMCE.activeEditor.getContent({format : 'raw'});
			var dataObj = {
				name: $scope.materials.name,
				unit_id: $scope.materials.unit_id,
				category: $scope.materials.category,
				file: $scope.materials.attachment,
				description: data.replace(/&nbsp;/g, ' ').replace(/;/g, '').replace(/&nbsp/g, ' '),
			};
		}
		var uploadUrl = BASE_URL + 'marketing/create/';
		fileUpload.uploadFileWithData(dataObj, uploadUrl, function(response) {
			if (response.success == true) {
				$mdSidenav('Create').close();
				globals.mdToast('success', response.message);
				$scope.ticketsLoader = true;
				$http.get(BASE_URL + 'api/advertising_material').then(function (Materials) {
					$scope.materials = Materials.data;
				});
			} else {
				$scope.saving = false;
				globals.mdToast('error', response.message);
			}
			$scope.uploading = false;
		});
	};	

		// Filtered Datas
		$scope.search = {
			name: '',
			unit_id: '',
			category: ''
		};
	});


	$scope.UpdateAdvMaterial = function(id) {
		$scope.saving = true;
		$scope.tempArr = [];
		$scope.uploading = true;
		
		var data = tinyMCE.activeEditor.getContent({format : 'raw'});
		var dataObj = {
			name: $scope.material.name,
			unit_id: $scope.material.unit_id,
			category: $scope.material.category,
			file: $scope.material.attachment,
			description: data.replace(/&nbsp;/g, ' ').replace(/;/g, '').replace(/&nbsp/g, ' '),
		};
		
		var uploadUrl = BASE_URL + 'marketing/update_adv_material/'+id;
		fileUpload.uploadFileWithData(dataObj, uploadUrl, function(response) {
			if (response.success == true) {
				$mdDialog.hide();
				globals.mdToast('success', response.message);
				$scope.ticketsLoader = true;
				$http.get(BASE_URL + 'api/advertising_material').then(function (Materials) {
					$scope.materials = Materials.data;
				});
			} else {
				$scope.saving = false;
				globals.mdToast('error', response.message);
			}
			$scope.uploading = false;
		});
	};	

	$scope.editAdvMaterial = function(material) {
		$scope.material = material;
		$mdDialog.show({
			templateUrl: 'update_adv_material.html',
			scope: $scope,
			preserveScope: true,
			targetEvent: material,
			parent: angular.element(document.body),
			onComplete: function(){
				if(material.description!=null){
					tinyMCE.activeEditor.setContent(material.description, {format : 'raw'});
				}
				else
				{
					material.description='';
				}
				tinymce.init({ 
    				selector:'textarea',
    				theme: 'modern',
    				width: 600,
   					editor_selector : "mceEditor",
    				theme: 'modern',
    				valid_elements : '*',
    				valid_styles: '*', 
    				plugins: 'print preview searchreplace autoresize autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking importcss anchor code insertdatetime advlist lists textcolor wordcount imagetools  contextmenu colorpicker textpattern',
    				//body_class: 'my_class',
    				valid_children : "+body[style]",
    				valid_elements: "@[id|class|title|style],"
						+ "a[name|href|target|title|alt],"
						+ "#p,-ol,,div,h1,h2,h3,h4,h5,h6,strong,-ul,-li,br,img[src|unselectable],-sub,-sup,-b,-i,-u,"
						+ "-span[data-mce-type],hr",

					valid_child_elements : "body[p,ol,ul,div,h1,h2,h3,h4,h5,h6,strong,b]"
						+ ",p[a|span|b|i|u|sup|sub|img|hr|#text]"
						+ ",span[a|b|i|u|sup|sub|img|#text]"
						+ ",a[span|b|i|u|sup|sub|img|#text]"
						+ ",b[span|a|i|u|sup|sub|img|#text]"
						+ ",i[span|a|b|u|sup|sub|img|#text]"
						+ ",sup[span|a|i|b|u|sub|img|#text]"
						+ ",sub[span|a|i|b|u|sup|img|#text]"
						+ ",li[span|a|b|i|u|sup|sub|img|ol|ul|#text]"
						+ ",ol[li]"
						+ ",ul[li]",
    				content_css: [
    				//fonts.googleapis.com/css?family=Lato:300,300i,400,400i’,
    				//www.tinymce.com/css/codepen.min.css’
    				],
    				toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'
  				});
   			}
			//controller: ['$scope', '$mdDialog', DialogAddMoreInfoController],
		});
	};


	$scope.Delete = function (ADVMATID) {
		globals.deleteDialog(lang.attention, lang.delete_adv_material, ADVMATID, lang.doIt, lang.cancel, 'marketing/remove_adv_material/' + ADVMATID, function(response) {
			if (response.success == true) {
				window.location.href = BASE_URL + 'marketing/adv_material';
			} else {
				globals.mdToast('error',response.message);
			}
		});
	};
}

function Distribution_Info_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, fileUpload, $q) {
	"use strict";

	$scope.distributionsLoader = true;
	
	
	var deferred = $q.defer();
	$scope.distribution_list = {
		order: '',
		limit: 10,
		page: 1
	};
	
	$http.get(BASE_URL + 'api/distribution_information').then(function (Distributions) {
		$scope.distributions = Distributions.data;

		$scope.limitOptions = [5, 10, 15, 20];
		if($scope.distributions.length > 20 ) {
			$scope.limitOptions = [5, 10, 15, 20, $scope.distributions.length];
		}
		$scope.distributionsLoader = false;

		$scope.saving = false;		

		// Filtered Datas
		$scope.search = {
			date: '',
			staffname: '',
			adv_product: '',
			quantity: '',
			customer: ''
		};
	});
}

function Stock_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, $q) {
	"use strict";

	$scope.close = function () {
		$mdDialog.hide();
		$http.get(BASE_URL + 'api/adv_stock_information').then(function (Stocks) {
			$scope.stocks = Stocks.data;
		});
	};

	// get all salespersons
	$http.get(BASE_URL + 'staff/get_staff_by_role/4').then(function (Salesperson) {
        $scope.salesperson = Salesperson.data;
        $scope.isLoading = false;
    });

	// get all advertising products
	$http.get(BASE_URL + 'marketing/getAllAdvertisingProduct').then(function (AdvProducts) {
		$scope.advproducts = AdvProducts.data;
	});

	$scope.stockLoader = true;
	
	var deferred = $q.defer();
	$scope.stock_list = {
		order: '',
		limit: 10,
		page: 1
	};
	
	$http.get(BASE_URL + 'api/adv_stock_information').then(function (Stocks) {
		$scope.stocks = Stocks.data;

		$scope.limitOptions = [5, 10, 15, 20];
		if($scope.stocks.length > 20 ) {
			$scope.limitOptions = [5, 10, 15, 20, $scope.stocks.length];
		}
		$scope.stocksLoader = false;
		$scope.saving = false;		

		// Filtered Datas
		$scope.search = {
			date: '',
			staffname: '',
			adv_product: '',
			quantity: '',
			customer: ''
		};

		$scope.editAdvStock = function(index) {
				for (var i = 0; i < $scope.stocks.length; i++) {
					if ($scope.stocks[i].id == index) {
						$scope.stock = $scope.stocks[i];
						continue;
					}
				}
				if ($scope.stock) {
					$mdDialog.show({
						templateUrl: 'update_adv_stock.html',
						scope: $scope,
						preserveScope: true,
						targetEvent: $scope.stock.id
					});
				}
			}

	});

	$scope.editAdvStockold = function(stock) {
		$scope.stock = stock;
		$mdDialog.show({
			templateUrl: 'update_adv_stock.html',
			scope: $scope,
			preserveScope: true,
			targetEvent: stock
		});
	};

	$scope.UpdateStock = function(id) {
		$scope.update = true;
		if ($scope.stock.date) 
		{
			$scope.stock.date = moment($scope.stock.date).format("YYYY-MM-DD")
		}
		var dataObj = $.param({
			date: $scope.stock.date,
			salesman_id: $scope.stock.salesman_id,
			product_id: $scope.stock.product_id,
			quantity: $scope.stock.quantity,
		});
		$http.post(BASE_URL + 'marketing/update_stock/' + id, dataObj, config)
		.then(
			function(response) {
				$scope.update = false;
				if (response.data.success == true) {
					$mdDialog.cancel();
					$http.get(BASE_URL + 'api/adv_stock_information').then(function (Stocks) {
						$scope.stocks = Stocks.data;
					});
					showToast(NTFTITLE, response.data.message, ' success');
				} else {
					$http.get(BASE_URL + 'api/adv_stock_information').then(function (Stocks) {
						$scope.stocks = Stocks.data;
					});
					showToast(NTFTITLE, response.data.message, ' danger');
				}
			},
			function(response) {
				$scope.update = false;
			}
			);
	};

	$scope.DeleteStock = function (STOCKID) {
		globals.deleteDialog(lang.attention, lang.delete_stock, STOCKID, lang.doIt, lang.cancel, 'marketing/remove_stock/' + STOCKID, function(response) {
			if (response.success == true) {
				window.location.href = BASE_URL + 'marketing/adv_stock';
			} else {
				globals.mdToast('error',response.message);
			}
		});
	};	
}

function AddStock_Controller($scope, $http, $mdSidenav, $filter, $mdDialog, $q, $timeout) {
	"use strict";
	$scope.loaditems = false;
	// get all salespersons
	$http.get(BASE_URL + 'staff/get_staff_by_role/4').then(function (Salesperson) {
        $scope.salesperson = Salesperson.data;
        $scope.isLoading = false;
    });

	// get all advertising products
	$http.get(BASE_URL + 'marketing/getAllAdvertisingProduct').then(function (AdvProducts) {
		$scope.advproducts = AdvProducts.data;
	});

	$scope.addstock = {
		items: []
	};

	$scope.add = function (id) {
		var presnet = false;
		var keepGoing = true;
		angular.forEach($scope.addstock.items, function (item) {
			if(keepGoing)
			{
				if(item.product_id == id)
				{
					presnet = true;
					keepGoing = false;
				}
			}
		});
		if(!presnet)
		{
			var productname = $.grep($scope.advproducts, function (products) {
                    return products.id == id;
                })[0].name;
			$scope.addstock.items.push({
				name: productname,
				product_id: id,
				quantity: 0,
			});
			$scope.loaditems = true;
		}
		else
		{
			alert("This product is already present.");
		}
		
	};

	$scope.GetProduct = (function (search) {
		var deferred = $q.defer();
		$timeout(function () {
			deferred.resolve($scope.advproducts);
		}, Math.random() * 500, false);
		return deferred.promise;
	});		

	$scope.remove = function (index) {
		$scope.addstock.items.splice(index, 1);
	};


	$scope.AddStock = function () {
		$scope.savingStock = true;
		$scope.tempArr = [];
		angular.forEach($scope.custom_fields, function (value) {
			if (value.type === 'input') {
				$scope.field_data = value.data;
			}
			if (value.type === 'textarea') {
				$scope.field_data = value.data;
			}
			if (value.type === 'date') {
				$scope.field_data = moment(value.data).format("YYYY-MM-DD");
			}
			if (value.type === 'select') {
				$scope.field_data = JSON.stringify(value.selected_opt);
			}
			$scope.tempArr.push({
				id: value.id,
				name: value.name,
				type: value.type,
				order: value.order,
				data: $scope.field_data,
				relation: value.relation,
				permission: value.permission,
			});
		});

		if (!$scope.addstock) {
			var dataObj = $.param({
				date: '',
				salesperson_id: '',
				items: '',
				custom_fields: '',
			});
		} else {
			var dataObj = $.param({
				date: moment($scope.addstock.date).format("YYYY-MM-DD"),
				salesperson_id: $scope.addstock.salesperson_id,
				items: $scope.addstock.items,
				totalItems: $scope.addstock.items.length
			});
		}
		var posturl = BASE_URL + 'marketing/add_adv_stock';
		$http.post(posturl, dataObj, config)
		.then(
			function (response) {
				var types = response.data.type;
				$scope.savingStock = false;
				if (response.data.success == true) {
						//showToast(NTFTITLE, response.data.message, ' success');
						$mdSidenav('NewExpense').close();
						window.location.href = BASE_URL + 'marketing/adv_stock';
					} else {
						showToast(NTFTITLE, response.data.message, ' danger');
					}
				},
				function (response) {
					$scope.savingStock = false;
					console.log(response);
				}
				);
	};
}

LwCRM.controller('Adv_Material_Controller', Adv_Material_Controller);
LwCRM.controller('Distribution_Info_Controller', Distribution_Info_Controller);
LwCRM.controller('Stock_Controller', Stock_Controller);
LwCRM.controller('AddStock_Controller', AddStock_Controller);