<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Marketing extends LW_Controller {

	function __construct() {
		parent::__construct();
		$path = $this->uri->segment( 1 );
		if ( !$this->Privileges_Model->has_privilege( $path ) ) {
			$this->session->set_flashdata( 'ntf3', '' . lang( 'you_dont_have_permission' ) );
			redirect( 'panel/' );
		}
	}

	function adv_material() 
    {
        if ( $this->Privileges_Model->check_privilege( 'marketing/adv_material', 'all' ) ) {
            $data[ 'title' ] = lang( 'advertising_material' );  
            $this->load->view( 'marketing/adv_material', $data );
        }
    }

    function create() {
        $data[ 'title' ] = lang( 'add_adv_material' );
        if ( $this->Privileges_Model->check_privilege( 'marketing/adv_material', 'create' ) )
        {
            if ( isset( $_POST ) && count( $_POST ) > 0 ) {
                $hasError = false;
                $data['message'] = '';
                if ($this->input->post( 'name' ) == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('name');
                } else if ($this->input->post( 'unit_id' ) == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('unit');
                } else if ($this->input->post( 'category' ) == '' ) {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('category');
                } 

                if ($hasError) {
                    $data['success'] = false;
                    echo json_encode($data);
                }
                if (!$hasError) {
                    $dup_params = array(
                        'name' => $this->input->post( 'name' ),
                        'unit_id' => $this->input->post( 'unit_id' ),
                        'category' => $this->input->post( 'category' )
                    );

                    $result = $this->Marketing_Model->check_data_allready_exist($dup_params);
                    if(!$result)
                    {
                        $appconfig = get_appconfig();
                        if (isset($_FILES['file']) && $_FILES['file']['name'] != '') {
                            $config[ 'upload_path' ] = './uploads/attachments/marketing_materials/';
                            $config[ 'allowed_types' ] = 'zip|rar|tar|gif|jpg|png|jpeg|gif|pdf|doc|docx|xls|xlsx|txt|csv|ppt|opt';
                            $config['max_size'] = '9000';
                            $new_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', basename($_FILES["file"]['name']));
                            $config['file'] = $new_name;
                            $this->load->library( 'upload', $config );
                            $this->upload->do_upload('file');
                            $data_upload_files = $this->upload->data();
                            $image_data = $this->upload->data();
                            $filename = $image_data['file_name'];
                        } else {
                            $filename = NULL;
                        }

                        $params = array(
                            'name' => $this->input->post( 'name' ),
                            'unit_id' => $this->input->post( 'unit_id' ),
                            'category' => $this->input->post( 'category' ),
                            'description' => $this->input->post( 'description' ),
                            'image' => $filename
                        );
                    
                        $this->session->set_flashdata( 'ntf1', lang('marketing_material').' '.lang('createmessage') );
                        $adv_id = $this->Marketing_Model->add_adv_material( $params );
                        if ($adv_id) 
                        {
                            if ( $this->input->post( 'custom_fields' ) ) 
                            {
                                $custom_fields = array(
                                    'custom_fields' => $this->input->post( 'custom_fields' )
                                );
                                $this->Fields_Model->custom_field_data_add_or_update_by_type( $custom_fields, 'add_adv_material', $adv_id );
                            } 
                            $data['success'] = true;
                            $data['message'] = lang('adv_material').' '.lang('createmessage');
                            $data['id'] = $adv_id;                        
                            echo json_encode($data);
                        } else {
                            $data['success'] = false;
                            $data['message'] = lang('errormessage');
                            echo json_encode($data);
                        }
                    }
                    else
                    {
                        $data['success'] = false;
                        $data['message'] = lang('adv_material').' '.lang('alreadyexist');
                        echo json_encode($data);
                    }
                }
            }
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
    }

    function download_file($image) {
        if (isset($image)) 
        {
            if (is_file('./uploads/attachments/marketing_materials/' . $image)) 
            {
                $this->load->helper('file');
                $this->load->helper('download');
                $data = file_get_contents('./uploads/attachments/marketing_materials/' . $image);
                force_download($image, $data);
            } else {
                $this->session->set_flashdata( 'ntf4', lang('filenotexist'));
                redirect('marketing/adv_material');
            }          
                
        }
    }
   
    function remove_adv_material( $id ) { 
        if ( $this->Privileges_Model->check_privilege( 'marketing/adv_material', 'all' ) ) {
            $material = $this->Marketing_Model->get_material_by_privileges( $id );
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
        if($material) {
            if ( $this->Privileges_Model->check_privilege( 'marketing/adv_material', 'delete' ) ) {
                if ( isset( $material[ 'id' ] ) ) {
                    $result = $this->Marketing_Model->delete_adv_material( $id ,$material['name']);
                    $material = lang('adv_material');
                    if ( $result ) {
                        $data['message'] = sprintf( lang( 'success_delete' ), $material . '' );
                        $data['success'] = true;
                        echo json_encode($data);
                    } else {
                        $data['message'] = sprintf( lang( 'cant_delete' ), $material . '' );
                        $data['success'] = false;
                        echo json_encode($data);
                    }
                } else {
                    show_error( 'Adv material not deleted' );
                }
            } else {
                $data['success'] = false;
                $data['message'] = lang('you_dont_have_permission');
                echo json_encode($data);
            }
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('units'));
        }
    }


    function update_adv_material($id) 
    {
        if ( $this->Privileges_Model->check_privilege( 'marketing/adv_material', 'edit' ) ) 
        {
            if (isset($id) && isAdmin()) 
            {
                if (isset($_POST) && count($_POST) > 0 ) 
                {                   
                    $hasError = false;
                    $data['message'] = '';
                    if ($this->input->post( 'name' ) == '') 
                    {
                        $hasError = true;
                        $data['message'] = lang('invalidmessage'). ' ' .lang('name');
                    } 
                    else if ($this->input->post( 'unit_id' ) == '') 
                    {
                        $hasError = true;
                        $data['message'] = lang('invalidmessage'). ' ' .lang('unit');
                    } 
                    else if ($this->input->post( 'category' ) == '' ) 
                    {
                        $hasError = true;
                        $data['message'] = lang('invalidmessage'). ' ' .lang('category');
                    } 
                    
                    if ($hasError) {
                        $return['success'] = false;
                        echo json_encode($data);
                    }
                    if (!$hasError) 
                    {
                        $dup_params = array(
                            'name' => $this->input->post( 'name' ),
                            'unit_id' => $this->input->post( 'unit_id' ),
                            'category' => $this->input->post( 'category' )
                        );

                        $result = $this->Marketing_Model->check_data_allready_exist($dup_params, $id);
                        if(!$result)
                        {   
                            $adv_product = $this->Marketing_Model->get_material_by_privileges($id);

                            // Upload new file.
                            $appconfig = get_appconfig();
                            if (isset($_FILES['file']) && $_FILES['file']['name'] != '') {
                                $config[ 'upload_path' ] = './uploads/attachments/marketing_materials/';
                                $config[ 'allowed_types' ] = 'zip|rar|tar|gif|jpg|png|jpeg|gif|pdf|doc|docx|xls|xlsx|txt|csv|ppt|opt';
                                $config['max_size'] = '9000';
                                $new_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', basename($_FILES["file"]['name']));
                                $config['file'] = $new_name;
                                $this->load->library( 'upload', $config );
                                $this->upload->do_upload('file');
                                $data_upload_files = $this->upload->data();
                                $image_data = $this->upload->data();
                                $filename = $image_data['file_name'];

                                // Delete old file.
                                if(!empty($adv_product['image']))
                                {
                                    $file = './uploads/attachments/marketing_materials/' . $adv_product['image'];
                                    if(is_file($file))
                                    {
                                        unlink($file);
                                    }                            
                                }
                            } else {
                                $filename = $adv_product['image'];
                            }

                            $params = array(
                                'name' => $this->input->post( 'name' ),
                                'unit_id' => $this->input->post( 'unit_id' ),
                                'category' => $this->input->post( 'category' ),
                                'description' => $this->input->post( 'description' ),
                                'image' => $filename
                            );

                            $this->db->where('id', $id)->update('free_sample_products', $params);
                            $return['success'] = true;
                            $return['message'] = lang('adv_material').' '.lang('updatemessage');
                            echo json_encode($return);
                        }
                    } 
                }
                else
                {
                    $return['message'] = lang('errormessage');
                    $return['success'] = false;
                    echo json_encode($return);
                }
            }
            else
            {
                $return['message'] = lang('errormessage');
                $return['success'] = false;
                echo json_encode($return);
            }
        }
        else
        {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }            
    }	

    // Distribution Information
    function distribution() 
    {
        if ( $this->Privileges_Model->check_privilege( 'marketing/distribution', 'all' ) ) {
            $data[ 'title' ] = lang( 'distribution_information' );  
            $this->load->view( 'marketing/distribution', $data );
        }
    }

    // Stock Information
    function adv_stock() 
    {
        if ( $this->Privileges_Model->check_privilege( 'marketing/adv_stock', 'all' ) ) {
            $data[ 'title' ] = lang( 'adv_stock_information' );  
            $this->load->view( 'marketing/stock', $data );
        }
    }

    function getAllAdvertisingProduct() 
    { 
        $products = $this->Marketing_Model->getAllAdvertisingProduct();
        $data_products = array();
        foreach ( $products as $product ) {
            $data_products[] = array(
                'name' => $product[ 'name' ],
                'id' => $product[ 'id' ],
            );
        };
        echo json_encode( $data_products );
    }

    function update_stock($id) {
        if ( $this->Privileges_Model->check_privilege( 'marketing/adv_stock', 'edit' ) ) {
            if (isset($id) && isAdmin()) {
                if (isset($_POST) && count($_POST) > 0 ) {
                    $date = $this->input->post('date');
                    $salesman_id = $this->input->post('salesman_id');
                    $product_id = $this->input->post('product_id');
                    $quantity = $this->input->post('quantity');
                    $hasError = false;
                    if ($date == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('date');
                    } else if ($salesman_id == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('salesperson');
                    } else if ($product_id == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('product');
                    } else if ($quantity == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('quantity');
                    }                     
                    
                    if ($hasError) {
                        $return['success'] = false;
                        echo json_encode($data);
                    }
                    if (!$hasError) {
                         $params = array(
                            'date' => $date,
                            'salesman_id' => $salesman_id,
                            'product_id' => $product_id,
                            'quantity' => $quantity
                        );

                        $result = $this->Marketing_Model->check_stock_allready_exist($params,$id);

                        if(!$result)
                        {
                            $this->db->where('id', $id)->update('stock', $params);
                            $return['success'] = true;
                            $return['message'] = lang('stock').' '.lang('updatemessage');
                            echo json_encode($return);
                        }
                        else
                        {
                            $data['success'] = false;
                            $data['message'] = lang('stock').' '.lang('alreadyexist');
                            echo json_encode($data);
                        }
                    }
                } else {
                    $return['message'] = lang('errormessage');
                    $return['success'] = false;
                    echo json_encode($return);
                }
            } else {
                $return['message'] = lang('errormessage');
                $return['success'] = false;
                echo json_encode($return);
            }
        } else {
            $return['success'] = false;
            $return['message'] = lang('you_dont_have_permission');
            echo json_encode($return);
        }
    }

    function remove_stock( $id ) { 
        if ( $this->Privileges_Model->check_privilege( 'marketing/adv_stock', 'all' ) ) {
            $stock = $this->Marketing_Model->get_stock_by_privileges( $id );
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
        if($stock) {
            if ( $this->Privileges_Model->check_privilege( 'marketing/adv_stock', 'delete' ) ) {
                if ( isset( $stock[ 'id' ] ) ) {
                    $result = $this->Marketing_Model->delete_stock( $id);
                    $stock = lang('stock');
                    if ( $result ) {
                        $data['message'] = sprintf( lang( 'success_delete' ), $stock . '' );
                        $data['success'] = true;
                        echo json_encode($data);
                    } else {
                        $data['message'] = sprintf( lang( 'cant_delete' ), $stock . '' );
                        $data['success'] = false;
                        echo json_encode($data);
                    }
                } else {
                    show_error( 'Stock not deleted' );
                }
            } else {
                $data['success'] = false;
                $data['message'] = lang('you_dont_have_permission');
                echo json_encode($data);
            }
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('units'));
        }
    }

    // Add advertisindg stock.
    function add_adv_stock() 
    { 
        if ( $this->Privileges_Model->check_privilege( 'marketing/adv_stock', 'create' ) ) {
            if ( isset( $_POST ) && count( $_POST ) > 0 ) {
                $hash = '';
                $hash = lw_Hash();
                $date = $this->input->post( 'date' );
                $salesperson_id = $this->input->post( 'salesperson_id' );
                $total = filter_var($this->input->post('total'), FILTER_SANITIZE_NUMBER_INT);

                $hasError = false;
                if ($date == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('date');
                } else if ($salesperson_id == '') {
                    $hasError = true;
                    $data['message'] = lang('selectinvalidmessage'). ' ' .lang('salesperson');
                }  else if ($this->input->post('totalItems') < 1) {
                    $hasError = true;
                    $data['message'] = lang('invalid_adv_stock');
                } else if (((int)($this->input->post('totalItems'))) == 0) {
                    $hasError = true;
                    $data['message'] = lang('invalid_adv_stock_value');
                } 

                if ($hasError) {
                    $data['success'] = false;
                    echo json_encode($data);
                }

                if (!$hasError) {
                    $appconfig = get_appconfig();
                    $date = $date;
                    $salesperson_id = $salesperson_id;
                    $this->Marketing_Model->deleteStockByDateAndSalesmanid($date,$salesperson_id);

                    for($i = 0 ; $i < $this->input->post('totalItems') ; $i++ )
                    {
                        $params[] = array(
                            'date' => $date,
                            'salesman_id' => $salesperson_id,
                            'product_id' => $this->input->post('items')[$i]['product_id'],
                            'quantity' => $this->input->post('items')[$i]['quantity'],
                        );
                    }
                    
                    $this->Marketing_Model->add_adv_stock($params , $this->input->post('totalItems'));
                    
                    $data['success'] = true;
                    $data['message'] = lang('adv_stock'). ' ' .lang('createmessage');
                    echo json_encode($data);
                }
            } else {
                $data['title'] = lang('addadvstock');
                $data[ 'products' ] = $this->Marketing_Model->getAllAdvertisingProduct();
                $data[ 'all_accounts' ] = $this->Staff_Model->get_staff_by_role(4);
                $this->load->view( 'marketing/addstock', $data );
            }
        } else {
            $this->session->set_flashdata( 'ntf3', lang( 'you_dont_have_permission' ) );
            redirect(base_url('expenses'));
        }
    }
}