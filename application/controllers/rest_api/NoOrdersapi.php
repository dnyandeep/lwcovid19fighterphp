<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class NoOrdersapi extends CI_Controller {

	function __construct() {
		parent::__construct();	
        $this->load->model( 'rest_api_models/Location_Model' );
		$this->load->library('form_validation');
	}

    function add()
    {

        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        if($receivedTokan == $this->token)
        {
            $this->form_validation->set_rules('customer_id', 'Customer', 'trim|required');
            $this->form_validation->set_rules('biller', 'Biller', 'trim|required');
            if ($this->form_validation->run() == true) 
            {
                $date = date('Y-m-d H:i:s');
                $customer_id = $this->input->post('customer_id');
                $biller_id = $this->input->post('biller');
                $latitude = $this->input->post('latitude');
                $longitude = $this->input->post('longitude');
                $no_order_reason = $this->input->post('noorder_reason');
                $next_order_date = $this->input->post('next_order_date') ? $this->input->post('next_order_date') : NULL;
                $note = $this->input->post('note') ? $this->input->post('note') : NULL;

                // invoice serial number                
                //$invoicePrefix = $biller_details->invoice_prefix;
                //$maxInvoiceSerialNumber = $this->no_orders_model->getMaxInvoiceSerialNumber($company_id);
                //$maxInvoiceSerialNumber = ++$maxInvoiceSerialNumber;   //add 1 to last count
                    //$invoiceNumber = $invoicePrefix.$maxInvoiceSerialNumber;
                //$invoiceNumber = 'No_order_'.$maxInvoiceSerialNumber;
                $invoiceNumber = 'No_order_'.1;

                $data = array(
                    'date' => $date,
                    'reference_no'      => $invoiceNumber,
                    'invoice_serial_no' => $maxInvoiceSerialNumber,
                    'customer_id' => $customer_id,
                    'biller_id' => $biller_id,
                    'noorder_reason'=>$no_order_reason,
                    'next_order_date'=>$next_order_date,
                    'note'   => $note,
                    'sale_status' => $sale_status,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                );

                if(!empty($data))
                {
                    if ($order_id = $this->Orders_Model->addNoOrder($data)) 
                    {
                        $order_details = $this->Orders_model->getOrderById($order_id);
                        $result = array('sale_details' => $order_details);
                        $response = array('success' => true, 'data' => $result, 'message' => ("Successfully added"));
                        echo json_encode($response);

                    }else{
                        $response = array('success' => false, 'message' => ("Something went wrong while inserting record, Please try after sometime."));
                        echo json_encode($response);
                    }
                }
            }
            else
            {
                $response = array('success' => false, 'message' => (validation_errors()));
            }
            echo json_encode($response);
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }   
    }
}