<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class PersonAPI extends CI_Controller {

	function __construct() {
		parent::__construct();	
		$this->load->library('form_validation');
    $this->load->model('Quarantined_Person_Model');
  }

  function addQuarantinedPerson($id=NULL)
  {


    $receivedTokan = $this->input->post('token') ? $this->input->post('token') :
    '';

    
    $fname = $this->input->post('fname');
    $mname = $this->input->post('mname');
    $lname = $this->input->post('lname');
    $device_id = $this->input->post('device_id');
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $mobile_no = $this->input->post('mobile_no');
    $alternate_no = $this->input->post('alternate_no');
    $whatsapp_no = $this->input->post('whatsapp_no');
    $email = $this->input->post('email');
    $gender = $this->input->post('gender');
    $dob = $this->input->post('dob');
    $today = date("Y-m-d");
    $diff = date_diff(date_create($dob), date_create($today));
    $age = $diff;
    $aadhar_no = $this->input->post('aadhar_no');
    $nationality = $this->input->post('nationality');
    $citizen_type = $this->input->post('citizen_type');
    $state = $this->input->post('state');
    $district_id = $this->input->post('district_id');
    $tahsil_name = $this->input->post('tahsil_name');
    $res_country_id = $this->input->post('res_country_id');
    $travel_history = $this->input->post('travel_history');
    $last_travel_country_id = $this->input->post('last_travel_country_id');
    $date_of_arrival_india = $this->input->post('date_of_arrival_india');
    $pincode = $this->input->post('pincode');

    if(!$date_of_arrival_india)
    {
      $date_of_arrival_india=NULL;
    }
    $passport_no = $this->input->post('passport_no');
    $is_covid19_confirmed = $this->input->post('is_covid19_confirmed');
    $is_suspect_patient = $this->input->post('is_suspect_patient');
    $address1 = $this->input->post('address1');
    $address2 = $this->input->post('address2');

    $this->form_validation->set_rules('fname', 'First Name','trim|required');
    $this->form_validation->set_rules('lname', 'Last Name','trim|required');
     // $this->form_validation->set_rules('device_id', 'Device Id','trim|required');
    $this->form_validation->set_rules('username','Username','trim|required');
    $this->form_validation->set_rules('password', 'Password','trim|required');
    $this->form_validation->set_rules('mobile_no', 'Contact Number','trim|required|regex_match[/^[0-9]{10}$/]');


    if($alternate_no)
    {
      $this->form_validation->set_rules('alternate_no', 'Alternate Number','trim|required|regex_match[/^[0-9]{10}$/]');
    }

    if($whatsapp_no)
    {
      $this->form_validation->set_rules('whatsapp_no', 'Whatsapp Number','trim|required|regex_match[/^[0-9]{10}$/]');
    }

    $this->form_validation->set_rules('gender', 'Gender','trim|required');
    $this->form_validation->set_rules('dob', 'DOB','trim|required');
   // $this->form_validation->set_rules('age', 'Age','trim|required');

    $this->form_validation->set_rules('email', 'Email','trim|required|regex_match[/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix]');

    $this->form_validation->set_rules('aadhar_no', 'Aadhar Number','trim|required|regex_match[/^[0-9]{12}$/]');

    $this->form_validation->set_rules('nationality', 'Nationality','trim|required');
    $this->form_validation->set_rules('citizen_type', 'Citizen Type','trim|required');

    $this->form_validation->set_rules('state', 'State','trim|required'); 
    $this->form_validation->set_rules('district_id', 'District','trim|required');
    $this->form_validation->set_rules('res_country_id', 'Country','trim|required');
    $this->form_validation->set_rules('is_covid19_confirmed', 'covid19 confirmed','trim|required');
    $this->form_validation->set_rules('is_suspect_patient', 'is suspect patient','trim|required');

    if($receivedTokan == $this->token)
    {
     if($this->form_validation->run() == true)
     {

      if($this->getPersonDetailsByEmail($email,$id))
      {

        $response = array('success' => false, 'message' => ("Email already used"));
        echo json_encode($response);
      }
      else if($this->getPersonDetailsByContactNo($mobile_no,$id))
      {

        $response = array('success' => false, 'message' => ("Mobile No already used"));
        echo json_encode($response);
      }
      else if($this->getPersonDetailsByUserName($username,$id))
      {
        $response = array('success' => false, 'message' => ("Username already used"));
        echo json_encode($response);

      }
      else
      {

        $data = array(
          'fname' => $this->input->post('fname'),
          'mname' => $this->input->post('mname'),
          'lname' => $this->input->post('lname'),
          'device_id' => $this->input->post('device_id'),
          'username' => $this->input->post('username'),
          'password' => md5($this->input->post('password')),
          'mobile_no' => $this->input->post('mobile_no'),
          'alternate_no' => $this->input->post('alternate_no'),
          'whatsapp_no' => $this->input->post('whatsapp_no'),
          'email' => $this->input->post('email'),
          'gender' => $this->input->post('gender'),
          'dob' => $this->input->post('dob'),
          'age' => $this->input->post('age'),
          'aadhar_no' => $this->input->post('aadhar_no'),
          'nationality' => $this->input->post('nationality'),
          'citizen_type'=>$this->input->post('citizen_type'),
          'state'=>$this->input->post('state'),
          'district_id'=>$this->input->post('district_id'),
          'tahsil_name'=> $this->input->post('tahsil_name'),
          'res_country_id'=> $this->input->post('res_country_id'),
          'travel_history'=> $this->input->post('travel_history'),
          'last_travel_country_id'=>$this->input->post('last_travel_country_id'),
          'date_of_arrival_india'=> $this->input->post('date_of_arrival_india'),
          'passport_no'=> $this->input->post('passport_no'),
          'is_covid19_confirmed'=> $this->input->post('is_covid19_confirmed'),
          'is_suspect_patient'=> $this->input->post('is_suspect_patient'),
          'address1'=> $this->input->post('address1'),
          'address2'=> $this->input->post('address2'),
          'pincode'=> $this->input->post('pincode')

        );

        if($id)
        {

          $result=$this->Quarantined_Person_Model->isPresent($id);

          if($result)
          {
            $this->db->where('id', $id);
            $this->db->update('tbl_quarantined_person_details', $data);
            $response = array('success' => true, 'message' => ("Details updated successfuly"));
            echo json_encode($response);
          }
        }
        else{

          $id=$this->db->insert( 'tbl_quarantined_person_details', $data );
          $data['id']=$id;
          $response = array('success' => true, 'message' => ("Details Saved successfuly"));
          echo json_encode($response);
        }


      }


    }
    else
    {
      $response = array('success' => false, 'message' => (validation_errors()));
      echo json_encode($response);
    }
  }
  else
  {
   $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
   echo json_encode($response);
 }
}

function login()
{

  $username=$this->input->post('username');
  $password=$this->input->post('password');
  $receivedTokan = $this->input->post('token') ? $this->input->post('token') :
  '';
  $this->form_validation->set_rules('username','Username','trim|required');
  $this->form_validation->set_rules('password', 'Password','trim|required');  
  if($receivedTokan == $this->token)
  {  
    if($this->form_validation->run() == true)
    {
      
      $result=$this->getPersonDetailsByUserNamePassword($username,$password);
      if($result)
      {
        $response = array('success' => true, 'message' => ("Logged in successfuly"));
        echo json_encode($response);
      }
      else
      {
        $response = array('success' => false, 'message' => ("Invalid username or password"));
        echo json_encode($response);

      }
      
    }
    else
    {
      $response = array('success' => false, 'message' => (validation_errors()));
      echo json_encode($response);
    }

  }
  else
  {
   $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
   echo json_encode($response);
 }
 

}




function getPersonDetailsByEmail($emailID,$id=NULL)
{
  if($id)
  {


      //return $this->db->query( "SELECT * FROM tbl_quarantined_person_details WHERE email='"+$emailID+"' AND id !='"+$id+"'")->row_array();

    return $this->db->get_where( 'tbl_quarantined_person_details', array( 'email' => $emailID ,'id!=' => $id) )->row_array(); 
  }
  else
  {
    return $this->db->get_where( 'tbl_quarantined_person_details', array( 'email' => $emailID ) )->row_array(); 
  }
  
}

function getPersonDetailsByContactNo($mobile_no,$id=NULL)
{
  if($id)
  {
    return $this->db->get_where( 'tbl_quarantined_person_details', array( 'mobile_no' => $mobile_no ,'id!=' => $id) )->row_array(); 
  }
  else
  {
    return $this->db->get_where( 'tbl_quarantined_person_details', array( 'mobile_no' => $mobile_no ) )->row_array();
  }
}

function getPersonDetailsByUserName($username,$id=NULL)
{
  if($id)
  {
    return $this->db->get_where( 'tbl_quarantined_person_details', array( 'username' => $username ,'id!=' => $id) )->row_array(); 
  }
  else
  {
    return $this->db->get_where( 'tbl_quarantined_person_details', array( 'username' => $username ) )->row_array();
  }
}

function getAllPersonDetails()
{
 $data=$this->db->query("select qd.*,s.name as stateName,d.name as districtName ,cn.countries_name as resiCountry,cn1.countries_name as travelCountry from tbl_quarantined_person_details qd inner join states s on s.id=qd.state inner join districts d
  on d.id=qd.district_id inner join countries cn on cn.countries_id = qd.res_country_id left join countries cn1
  on cn1.countries_id=qd.last_travel_country_id")->result_array();
 $response = array('success' => true, 'message' => ("found"),'data'=>$data);
 print_r(  json_encode($response));

}

function getPersonDetailsByUserNamePassword($username,$password)
{
 return $this->db->get_where( 'tbl_quarantined_person_details', array( 'username' => $username ,'password'=> md5($password) ))->row_array();
}





}