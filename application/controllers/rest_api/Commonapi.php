<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Commonapi extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model( 'Cust_Categories_Model' );
		$this->load->model( 'Cust_Subcategories_Model' );
		$this->load->model( 'Products_Model' );
		$this->load->model( 'Countries_Model' );
        $this->load->model( 'State_Model' );
		$this->load->model( 'District_Model' );
		$this->load->model( 'Emails_Model' );
		$this->load->model( 'Settings_Model' );
		$this->load->model( 'FAQ_Model' );
		$this->load->model( 'Contact_Us_Model' );
		$this->load->model( 'KWSolarRateCard_Model' );
		$this->load->model( 'TableVersions_model' );
		$this->load->model( 'rest_api_models/Common_Model' );
		$this->load->library('form_validation');
	}

	

	function getAllStates()
	{
		
    		$states = $this->State_Model->get_states();
    		if($states)
    		{
    			$response=array('success' => true, 'data'=>$states);
        		echo json_encode($response);
    		}
		
	}

	function getAllDistricts()
	{
		
    		$districts = $this->District_Model->get_districts();
    		if($districts)
    		{
    			$response=array('success' => true, 'data'=>$districts);
        		echo json_encode($response);
    		}
		
	}

    function getAllDistrictsByState($state)
    {
        
            $districts = $this->District_Model->get_district_by_state_id($state);
            if($districts)
            {
                $response=array('success' => true, 'data'=>$districts);
                echo json_encode($response);
            }
        
    }


    function getAllCountries()
    {
        
            $states = $this->Countries_Model->get_countries();
            if($states)
            {
                $response=array('success' => true, 'data'=>$states);
                echo json_encode($response);
            }
       
    }



	function contact_us()
	{
		$receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
    	if($receivedTokan == $this->token)
    	{
    		$this->form_validation->set_rules('visitor_name', 'Visitor Name','trim|required');
    		$this->form_validation->set_rules('visitor_contact', 'Visitor Contact Number','trim|required');
    		$this->form_validation->set_rules('description', 'Description','trim|required');

    		if($this->form_validation->run() == true)
    		{
    			$data = array(
    				'visitor_name' => $this->input->post('visitor_name'),
    				'visitor_email' => $this->input->post('visitor_email'),
    				'visitor_contact' => $this->input->post('visitor_contact'),
    				'description' => $this->input->post('description'),
    				'created_date' => date('Y-m-d H:i:s'),
    			);
    			if($this->Contact_Us_Model->add( $data ))
    			{
    				$setconfig = $this->Settings_Model->get_settings_lw();
    				$this->load->library('mail'); 
    				
    				if(!empty($this->input->post('visitor_email')))
    				{
    					$to = $this->input->post('visitor_email');
    					$template = $this->Emails_Model->get_template('contactus', 'contact_us_email');    					
						$message_vars = array(
							'{visitor_name}' => $this->input->post('visitor_name')
						);
						$subject = strtr($template['subject'], $message_vars);
						$message = strtr($template['message'], $message_vars);
						$param = array(
							'from_name' => $template['from_name'],
							'email' => $to,
							'subject' => $subject,
							'message' => $message,
							'created' => date( "Y.m.d H:i:s" ),
							'status' => 0
						);
						if($to) 
						{							
							$data = $this->mail->send_email($to, $template['from_name'], $subject, $message);
							if ($data['success'] == true) {
								$return['success'] = true;
								$return['message'] = $data['message'];
								$this->db->insert( 'email_queue', $param );
								echo json_encode($return);
							} else {
								$return['success'] = false;
								$return['message'] = lang('errormessage');
								echo json_encode($return);
							}
    					}
    					else
    					{
    						$response = array('success' => false, 'message' => 'Email field is required');
    						echo json_encode($response);
    					}
    				}
    				
    				// Sending email to staff
    				$emailto = 'logicowise@gmail.com';  // Staff email id
    				$template = $this->Emails_Model->get_template('contactus', 'new_entry_email');
    				$message_vars = array(
    					'{staffname}' => 'Sir/Madam',
						'{visitor_name}' => $this->input->post('visitor_name'),
						'{visitor_contact}' => $this->input->post('visitor_contact'),
						'{description}' => $this->input->post('description')
					);

					$subject = strtr($template['subject'], $message_vars);
					$message = strtr($template['message'], $message_vars);
					$param = array(
						'from_name' => $template['from_name'],
						'email' => $to,
						'subject' => $subject,
						'message' => $message,
						'created' => date( "Y.m.d H:i:s" ),
						'status' => 0
					);
					
					if($emailto) 
					{							
						$data = $this->mail->send_email($emailto, $template['from_name'], $subject, $message);
						if ($data['success'] == true) {
							$return['success'] = true;
							$return['message'] = 'Email sent Succesfully';
							$this->db->insert( 'email_queue', $param );
							echo json_encode($return);
						} else {
							$return['success'] = false;
							$return['message'] = lang('errormessage');
							echo json_encode($return);
						}
    				}
    				else
    				{
    					$response = array('success' => false, 'message' => 'Email field is required');
    					echo json_encode($response);
    				}    				
    			}
    			else
    			{
    				$response = array('success' => false, 'message' => ("Something went wrong during inserting record,please try after sometime."));
      				echo json_encode($response);
    			}    			
    		}
    		else
    		{
    			$response = array('success' => false, 'message' => (validation_errors()));
    			echo json_encode($response);
    		}
		}
		else
		{
			$response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
      		echo json_encode($response);
		}
	}

	

	function allTableVersions()
	{
		$receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
    	if($receivedTokan == $this->token)
    	{
    		if($this->input->post())
    		{
    			$data = $this->input->post('tablename');
				$tablelist = explode(',',$data);
				$details = $this->TableVersions_model->getAllTableVersions($tablelist);
				if($details)
				{
					$response = array('success' => true, 'data' => $details);
      				echo json_encode($response);
				}
				else
				{
					$response = array('success' => false, 'message' => ("No data available in table."));
      				echo json_encode($response);
				}
    		}
		}
		else
		{
			$response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
      		echo json_encode($response);
		}
	}

	//
	public function login_process()
	{
		$receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
    	if($receivedTokan == $this->token)
    	{
    		$this->form_validation->set_rules('email', 'Email','trim|required');
            $this->form_validation->set_rules('password', 'Password','trim|required');
            if ($this->form_validation->run() == true) 
            {
            	$email = $this->input->post('email');
            	$password = $this->input->post('password');
            	$result = $this->Common_Model->getUserDetails($email, $password);
            	if($result)
            	{
            		$response = array('success' => true, 'data' => $result, 'message' => ("Login Successfully"));
            	}
            	else
            	{
            		$response = array('success' => false, 'message' => ("Invalid Login Details"));
            	}
            }
            else
            {
            	$response = array('success' => false, 'message' => (validation_errors()));
            }
            echo json_encode($response);
    	}
    	else
		{
			$response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
      		echo json_encode($response);
		}
	}

	//get login details
    public function forgot_pasword()
    {
    	$receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
    	if($receivedTokan == $this->token)
    	{
    		$this->form_validation->set_rules('email', 'Email','trim|required');
    		if ($this->form_validation->run() == true) 
            {
            	$email = $this->input->post('email');
				$result = $this->Common_Model->getUserDetailsByEmail($email);
				if($result)
				{
					$password = $this->generate_password();
					if($password)
					{
						$data[ 'password' ] = md5($password);
						$data[ 'user_id' ] = $result['user_id'];
						if($this->Common_Model->updatePassword($data))
						{
							// send email
						}
						else
						{
							$response = array('success' => false, 'message' => ("Something went wrong during update record, Please try after sometime."));
						}						
					}
				}
				else
				{
					$response = array('success' => false, 'message' => ("Email is not resgistered with us."));
				}
            }
            else
            {
            	$response = array('success' => false, 'message' => (validation_errors()));
            }
            echo json_encode($response);
    	}
    	else
		{
			$response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
      		echo json_encode($response);
		}

    }


    function generate_password( $length = 12, $special_chars = true, $extra_special_chars = false ) 
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        if ( $special_chars )
            $chars .= '!@#$%^&*()';
        if ( $extra_special_chars )
            $chars .= '-_ []{}<>~`+=,.;:/?|';

        $password = '';
        for ( $i = 0; $i < $length; $i++ ) 
        {
            $password .= substr($chars, rand(0, strlen($chars) - 1), 1);
        }
        return $password;
    }
}