<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Locationapi extends CI_Controller {

	function __construct() {
		parent::__construct();	
        $this->load->model( 'rest_api_models/Location_Model' );
		$this->load->library('form_validation');
	}

    public function updateSalesmanLocation()
    {
        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        if($receivedTokan == $this->token)
        {
            $this->form_validation->set_rules('salesman_id', 'Salesman Id','trim|required');
            if ($this->form_validation->run() == true) 
            {
                $data = array(
                    'latitude'=> $this->input->post('latitude'),
                    'longitude' => $this->input->post('longitude'),
                    'status' => $this->input->post('status'),
                    'date' => date('Y-m-d H:i:s')
                );

                $salesman_id = $this->input->post('salesman_id');
                $result = $this->Location_Model->addSalesmanCurrentLocation($data, $salesman_id);
                if($result)
                {
                    $response = array('success' => true, 'message' => ("Successfully Stored"));
                }
                else
                {
                    $response = array('success' => false, 'message' => ("Something went wrong while inserting record,try after sometime."));
                }
            }
            else
            {
                $response = array('success' => false, 'message' => (validation_errors()));
            }

            echo json_encode($response);
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }
    }


    public function updateSalesmanStatus()
    {
        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        if($receivedTokan == $this->token)
        {
            $this->form_validation->set_rules('salesman_id', 'Salesman Id','trim|required');
            if ($this->form_validation->run() == true) 
            {
                $data = array(
                    'status' => $this->input->post('status'),
                    'date' => date('Y-m-d H:i:s'),
                );
                $salesman_id = $this->input->post('salesman_id');
                $result = $this->Location_Model->addSalesmanCurrentLocation($data, $salesman_id);
                if($result)
                {
                    $response = array('success' => true, 'message' => ("Status Updated uccessfully"));
                }
                else
                {
                    $response = array('success' => false, 'message' => ("Something went wrong during updating status,Please try after sometime."));
                }
            }
            else
            {
                $response = array('success' => false, 'message' => (validation_errors()));
            }
            echo json_encode($response);
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }
    }

    //store salesman route info 
    public function store_saleman_route()
    {
        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        if($receivedTokan == $this->token)
        {
            $this->form_validation->set_rules('salesman_id', 'Salesman Id','trim|required');
            if ($this->form_validation->run() == true) 
            {
                $offlineRouteId = $this->input->post('offlineRouteId');
                $end_date = date('Y-m-d',strtotime($this->input->post('end_time')));
                $salesman = $this->input->post('salesman_id');
                $distance = $this->input->post('distance');

                $data=array(
                    'salesman_id' => $salesman,
                    'end_time' => $this->input->post('end_time'),   
                    'start_time' => $this->input->post('start_time'),
                    'distance' => $this->input->post('distance'),
                );

                $return_data = $this->Location_Model->checkRoutePresentOrNot($data);
                $duplicateDist = $this->Location_Model->checkDistancePresentForSameDate($salesman,$end_date,$distance);
                if($return_data || $duplicateDist)
                {
                    $response = array('success' => false, 'message' => ("This route is present already"));
                    echo json_encode($response);
                }
                else
                {
                    $post_params = array(
                        'salesman_id' => $salesman,                    
                        'end_time'=>  $this->input->post('end_time'),   
                        'start_time'=>  $this->input->post('start_time'),
                        'route_info' => $this->input->post('route_info'),
                        'start_address' => $this->input->post('start_address'),
                        'end_address' => $this->input->post('end_address'),
                        'distance' => $this->input->post('distance'),
                        'end_date' => $end_date,
                    );

                    $result = $this->Location_Model->add_salesman_route($post_params);
                    if($result)
                    {
                        $response = array('success' => true, 'offlineRouteId' => $offlineRouteId , 'data' => $result, 'message' => ("Successfully stored"));
                    }
                    else
                    {
                        $response = array('success' => false, 'message' => ("Something went wrong during storing root, Please try after sometime."));
                    }
                    echo json_encode($response);
                }
            }
            else
            {
                $response = array('success' => false, 'message' => (validation_errors()));
            }
            echo json_encode($response);
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }
    }

}