<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class EmployeeAPI extends CI_Controller {

	function __construct() {
		parent::__construct();	
		$this->load->library('form_validation');
        $this->load->model('EmployeeModel');
	}

    public function addEmployee()
    {
        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        if($receivedTokan == $this->token)
        {
            $this->form_validation->set_rules('name', 'Employee','trim|required');
            $this->form_validation->set_rules('department', 'Employee','trim|required');
            $this->form_validation->set_rules('joiningdate', 'Employee','trim|required');
            $this->form_validation->set_rules('salary', 'Employee','trim|required');
            if ($this->form_validation->run() == true) 
            {
                $name = $this->input->post('name');
                $department = $this->input->post('department');
                $joiningdate = $this->input->post('joiningdate');
                $salary = $this->input->post('salary');
                 $id = $this->input->post('id');
                 if($id=='' || $id==null)
                 {
                     $id=0;

                 }
                
                $arr=array("name"=>$name,"department"=>$department,"joiningdate"=>$joiningdate,"salary"=>$salary,"id"=>$id);
                if ($this->EmployeeModel->addEmployee($arr)) 
                {
                    $response = array('success' => true, 'message' => ("Successfully stored"));
                    echo json_encode($response);
                }
            }
            else
            {
                $response = array('success' => false, 'message' => (validation_errors()));
                echo json_encode($response);
            }
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }
    }

    
    public function getAllEmployees()
    {
        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        if($receivedTokan == $this->token)
        {
             $this->db->order_by( 'id', 'asc' );
             $data=$this->db->get_where( 'employee', array( '' ) )->result_array();
              $response = array('success' => true,'data' =>$data, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }

    }

}