<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Leadapi extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model( 'Leads_Model' );
		$this->load->library('form_validation');
        $this->load->library('mail'); 
        $this->load->model( 'Products_Model' );
        $this->load->model( 'State_Model' );
        $this->load->model( 'District_Model' );
        $this->load->model( 'Settings_Model' );
        $this->load->model( 'Emails_Model' );
        $this->load->model( 'CalculationConfig_Model' );
	}

	function addLead()
	{        
		$receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
    	if($receivedTokan == $this->token)
    	{
    		$this->form_validation->set_rules('first_name', 'First Name','trim|required');
    		$this->form_validation->set_rules('last_name', 'Last Name','trim|required');
    		$this->form_validation->set_rules('phone', 'Phone Number','trim|required|regex_match[/^[0-9]{10}$/]');
    		$this->form_validation->set_rules('state_id', 'State','trim|required');	
    		$this->form_validation->set_rules('district_id', 'District','trim|required');
    		
            $this->form_validation->set_rules('city', 'City','trim|required');
            $this->form_validation->set_rules('calculation_type', 'Calculation Type','trim|required');
            if($this->input->post('calculation_type') == 'unit')
            {
                $this->form_validation->set_rules('consumer_number', 'Consumer Number','trim|required');
                $this->form_validation->set_rules('units', 'Units','trim|required');
                $this->form_validation->set_rules('unit_type', 'Unit Type','trim|required');
                $this->form_validation->set_rules('approx_unit_rate', 'Approx Unit Rate','trim|required');
            }
            else if($this->input->post('calculation_type') == 'fileupload')
            {
                $this->form_validation->set_rules('is_file_attached', 'Is File Attached','trim|required');
            }            

    		if($this->input->post('unit_type') == 'Monthly')
    		{
    			$this->form_validation->set_rules('no_of_months', 'No Of Months','trim|required');
    		}
    		if ($this->form_validation->run() == true) 
    		{
                $otp = $this->input->post('otp') ? $this->input->post('otp') : '';
                // send otp if otp is empty.
                if(empty($otp))
                {
                    // generate otp
                    $new_otp = $this->generateOTP();
                    $mobileNum = $this->input->post('phone');
                    $message = '%3C%23%3E Hello '.$this->input->post('first_name').', Welcome to AFM Solar System Pvt. Ltd. Your OTP is ' . $new_otp . ' pfH7juRe9tO';  
                    //$otpResponse = $this->sendsmsGET($mobileNum, $message);
                    $new_otp = '123456';
                    $otpResponse=true;
                    if($otpResponse)
                    {
                        $response = array('success' => true, 'mobile_number' => $mobileNum, 'otp' => $new_otp, 'message' => ("OTP Sent Successfully"));
                        echo json_encode($response);
                    }
                    else
                    {
                        $response = array('success' => false, 'message' => ("Something went wrong while sending OTP"));
                        echo json_encode($response);
                    }
                }
                else
                {
                    $data = array(
                        'name' => $this->input->post('first_name'),
                        'middle_name' => $this->input->post('middle_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('phone'),
                        'address' => $this->input->post('address'),
                        'pin_code' => $this->input->post('pin_code'),
                        'city' => $this->input->post('city'),
                        'state_id' => $this->input->post('state_id'),
                        'district_id' => $this->input->post('district_id'),
                        'cust_category_id'=>$this->input->post('cust_category_id'),
                        'cust_subcategory_id'=>$this->input->post('cust_subcategory_id'),
                        'product_id'=>$this->input->post('product_id'),
                        'consumer_number'=> $this->input->post('consumer_number'),
                        'supplier'=> $this->input->post('supplier'),
                        'units'=> $this->input->post('units'),
                        'unit_type'=> $this->input->post('unit_type'),
                        'no_of_months'=> $this->input->post('no_of_months'),
                        'approx_unit_rate'=> $this->input->post('approx_unit_rate'),

                        'status' => 1,
                        'source' => 1,
                        'date_contacted' => '0000-00-00 00:00:00',
                        'assigned_id'=>1,
                        'staff_id'=>1,
                        'created' => date( 'Y-m-d H:i:s' )                    
                    );

                    if($this->input->post('calculation_type') == 'fileupload')
                    {
    			        $is_file_attached = $this->input->post('is_file_attached');
                        $act_file_path = "";
				        if($is_file_attached == '1')
				        {
					        $file_path = "uploads/lead_attachments/";
					        if (!is_dir($file_path)) 
                	        {
                    	       mkdir($file_path, 0777, TRUE);
                	        }
                            $date = date('Y-m-d H:i:s');
                            $name = strtotime($date);

                            $ext = pathinfo(basename( $_FILES['file']['name']), PATHINFO_EXTENSION);
                       
					       //$act_file_path = $file_path . basename( $_FILES['file']['name']);
                            $act_file_path = $file_path . $name.'.'.$ext;
					       move_uploaded_file($_FILES['file']['tmp_name'], $act_file_path);
				        }
                        $data['attachment'] = $act_file_path;
                    }

                    $lead_id = $this->Leads_Model->add_lead( $data );
                    if($lead_id)
                    {                 
                        // generate lead id
                        $year= date("Y");
                        $statePadded = sprintf("%02d", $this->input->post('state_id'));
                        $districtPadded = sprintf("%04d",$this->input->post('district_id'));
                        $idPadded = sprintf("%06d", $lead_id);
                        $code=$year.$statePadded.$districtPadded.$idPadded;
                        $lead_array = array('lead_number' => $code );
                        $rows=$this->Leads_Model->edit_lead($lead_array,$lead_id);
                        if($rows)
                        {
                            $lead_detail = $this->Leads_Model->get_lead_by_id( $lead_id );
                            $config_detail = $this->CalculationConfig_Model->getLatestCalConfiguration();
                            if($config_detail)
                            {
                                $lead_detail['configuration'] = $config_detail;
                            }
                            else
                            {
                                $lead_detail['configuration'] = '';
                            }
                            
                            // Send email to support team
                            // get email from table
                            $setconfig = $this->Settings_Model->get_settings_lw();
                            $to = 'support@afmsolarsystem.com';
                            $state = $this->State_Model->get_state_by_id($this->input->post('state_id'));
                            $district = $this->District_Model->get_district_by_id($this->input->post('district_id'));
                            $product = $this->Products_Model->get_product_by_id($this->input->post('product_id'));
                       
                            $name = $this->input->post('first_name').' '.$this->input->post('middle_name').' '.$this->input->post('last_name');
                            $template = $this->Emails_Model->get_template('newlead', 'new_lead_generated');
                            $message_vars = array(
                                '{staffname}' => 'Sir/madam',
                                '{name}' => $name,
                                '{contact_no}' => $this->input->post('phone'),
                                '{email}' => $this->input->post('email'),
                                '{address}' => $this->input->post('address'),
                                '{city}' => $this->input->post('city'),
                                '{state}' => $state['name'],
                                '{district}' => $district['name'],
                            );

                            $subject = strtr($template['subject'], $message_vars);
                            $message = strtr($template['message'], $message_vars);
                            $param = array(
                                'from_name' => $template['from_name'],
                                'email' => $to,
                                'subject' => $subject,
                                'message' => $message,
                                'created' => date( "Y.m.d H:i:s" ),
                                'status' => 0
                            );
                            $path = '';
                            if(!empty($data['attachment']))
                            {
                                $path = $data['attachment'];                            
                            }
                    
                            /*if($to) 
                            {                           
                                $data = $this->mail->send_email($to, $template['from_name'], $subject, $message,$path);
                                if ($data['success'] == true) {
                                    $response = array('success' => true, 'data' => $lead_detail);
                                    echo json_encode($response);
                                } else {
                                    $return['success'] = false;
                                    $return['message'] = lang('errormessage');
                                    echo json_encode($return);
                                }
                            }*/
                            $response = array('success' => true, 'data' => $lead_detail);
      				        echo json_encode($response);
                        }
                        else
                        {
                            $response = array('success' => false, 'message' => 'Something went wrong during inserting lead number.');
                            echo json_encode($response);
                        }
      			    }
      			    else
      			    {
      				    $response = array('success' => false, 'message' => 'Something went wrong during inserting record,please try after sometime');
      				    echo json_encode($response);
      			    }
                }
    		}
    		else
    		{
	    		$response = array('success' => false, 'message' => (validation_errors()));
	    		echo json_encode($response);
    		}
		}
		else
		{
			$response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
      		echo json_encode($response);
		}
	}


    function generateLead()
    {        
        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        if($receivedTokan == $this->token)
        {
            $this->form_validation->set_rules('first_name', 'First Name','trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name','trim|required');
            $this->form_validation->set_rules('contact_no', 'Contact Number','trim|required|regex_match[/^[0-9]{10}$/]');
            $this->form_validation->set_rules('state', 'State','trim|required'); 
            $this->form_validation->set_rules('district', 'District','trim|required');  
            $this->form_validation->set_rules('city', 'City','trim|required');
            $this->form_validation->set_rules('current_product', 'Current Product','trim|required');
            $this->form_validation->set_rules('explained_product_id', 'Explained Product','trim|required');
            $this->form_validation->set_rules('rating', 'Rating','trim|required');
            $this->form_validation->set_rules('demo_incharge', 'Demo Incharge','trim|required');           
            $this->form_validation->set_rules('created_by', 'Created By','trim|required');
            //$this->form_validation->set_rules('beat_id', 'Beat/Area','trim|required'); 
            if ($this->form_validation->run() == true) 
            {
                $data = array(
                    'company' => $this->input->post('customer_company'),
                    'name' => $this->input->post('first_name'),
                    'middle_name' => $this->input->post('middle_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('contact_no'),
                    'email' => $this->input->post('email'),
                    'city' => $this->input->post('city'),
                    'pin_code' => $this->input->post('pincode'),
                    'state_id' => $this->input->post('state'),
                    'district_id' => $this->input->post('district'),
                    'address' => $this->input->post('address'),
                    'current_product' => $this->input->post('current_product'),
                    'current_product_name' => $this->input->post('current_product_name'),
                    'installed_date' => $this->input->post('installed_date'),
                    'product_company' => $this->input->post('product_company'),
                    'product_price' => $this->input->post('product_price'),
                    'product_id' => $this->input->post('explained_product_id'), 
                    'rating' => $this->input->post('rating'),
                    'next_meeting_date' => $this->input->post('next_meeting_date'),
                    'demo_incharge' => $this->input->post('demo_incharge'),
                    'business_start_date' => $this->input->post('business_start_date'),
                    'capacity' => $this->input->post('capacity'),
                    'no_of_kitchens' => $this->input->post('no_of_kitchens'),
                    'no_of_tables' => $this->input->post('no_of_tables'),
                    'no_of_menus' => $this->input->post('no_of_menus'),
                    'latitude' => $this->input->post('latitude'),
                    'longitude' => $this->input->post('longitude'), 
                    //'beat_id' => $this->input->post('beat_id'),   
                    'lead_status' => 'New',
                    'created' => date('Y-m-d H:i:s'),
                    'created_by' => $this->input->post('created_by'),

                    'status' => 1,
                    'source' => 1,
                    'date_contacted' => '0000-00-00 00:00:00',
                    'assigned_id'=>1,
                    'staff_id'=>1,
                    'created' => date( 'Y-m-d H:i:s' )
                );
                
                $lead_id = $this->Leads_Model->add_lead( $data );
                if($lead_id)
                {                 
                    // generate lead id
                    $year= date("Y");
                    $statePadded = sprintf("%02d", $this->input->post('state'));
                    $districtPadded = sprintf("%04d",$this->input->post('district'));
                    $idPadded = sprintf("%06d", $lead_id);
                    $code=$year.$statePadded.$districtPadded.$idPadded;
                    $lead_array = array('lead_number' => $code );
                    $rows=$this->Leads_Model->edit_lead($lead_array,$lead_id);
                    if($rows)
                    {
                        $lead_detail = $this->Leads_Model->get_lead_by_id( $lead_id );
                        $response = array('success' => true, 'data' => $lead_detail);
                        echo json_encode($response);
                    }
                    else
                    {
                        $response = array('success' => false, 'message' => 'Something went wrong during inserting lead number.');
                        echo json_encode($response);
                    }
                }
                else
                {
                    $response = array('success' => false, 'message' => 'Something went wrong during inserting record,please try after sometime');
                    echo json_encode($response);
                }                
            }
            else
            {
                $response = array('success' => false, 'message' => (validation_errors()));
                echo json_encode($response);
            }
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }
    }

    function getLeads()
    {        
        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        $salesperson_id= $this->input->post('salesperson_id') ? $this->input->post('salesperson_id') : '';
        if($receivedTokan == $this->token)
        {
            if($salesperson_id)
            {
                $data =  $this->Leads_Model->get_all_leads_by_salesperson($salesperson_id);
                $response = array('success' => true, 'data'=>$data,'message' => ("Data found"));
                echo json_encode($response);
            }
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }
    }


}