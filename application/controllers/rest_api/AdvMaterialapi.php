<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class AdvMaterialapi extends CI_Controller {

	function __construct() {
		parent::__construct();	
		$this->load->library('form_validation');
	}

    public function addDistributionMaterials()
    {
        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        if($receivedTokan == $this->token)
        {
            $this->form_validation->set_rules('salesman_id', 'Salesman','trim|required');
            if ($this->form_validation->run() == true) 
            {
                $salesman_id = $this->input->post('salesman_id');
                $product_id = $this->input->post('materials');
                $json = json_decode($product_id);
                if ($this->Marketing_Model->addDiastributionMaterial($json, $salesman_id)) 
                {
                    $response = array('success' => true, 'message' => ("Successfully stored"));
                    echo json_encode($response);
                }
            }
            else
            {
                $response = array('success' => false, 'message' => (validation_errors()));
                echo json_encode($response);
            }
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }
    }

    public function getMaterialsData()
    {
        $receivedTokan = $this->input->post('token') ? $this->input->post('token') : '';
        if($receivedTokan == $this->token)
        {
            $this->form_validation->set_rules('salesman_id', 'Salesman','trim|required');
            if ($this->form_validation->run() == true) 
            {
                $salesman_id = $this->input->post('salesman_id');
                $iscustomersFound = false;
                $isMaterialsFound = false;

                // get all customers.
                $customers = $this->Customers_model->getAllCustomers();
                if($customers)
                {
                    $data['customers'] = $customers;
                    $iscustomersFound = true;
                }
                else
                {
                    $iscustomersFound = false;
                }

                // get all advertising material by salesman id.
                $materials = $this->Marketing_Model->getAllMaterials($salesman_id);
                if($materials)
                {
                    $data['materials'] = $materials;
                    $isMaterialsFound = true;
                }
                else
                {
                    $isMaterialsFound = false;
                }

                $response = array('success' => true, 'iscustomersFound' => $iscustomersFound, 'isMaterialsFound' => $isMaterialsFound , 'data' => $data, 'message' => (""));
                echo json_encode($response);
            }
            else
            {
                $response = array('success' => false, 'message' => (validation_errors()));
                echo json_encode($response);
            }
        }
        else
        {
            $response = array('success' => false, 'message' => ("You have provided an invalid token,Please provide valid token"));
            echo json_encode($response);
        }

    }

}