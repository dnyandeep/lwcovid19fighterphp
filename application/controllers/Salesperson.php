<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Salesperson extends LW_Controller {

	function __construct() {
		parent::__construct();
		$path = $this->uri->segment( 1 );
		if ( !$this->Privileges_Model->has_privilege( $path ) ) {
			$this->session->set_flashdata( 'ntf3', '' . lang( 'you_dont_have_permission' ) );
			redirect( 'panel/' );
		}
	}

	function currentlocation() {
		$data[ 'title' ] = lang( 'salesperson_location' );
		$this->load->view( 'salesperson/currentlocation', $data );
	}

	function getLatLong()
	{
		$salesperson_id = $this->input->post('salesperson_id');
		if ($rows = $this->Staff_Model->get_LatLong_By_ID($salesperson_id)) 
		{             
            $today = date('Y-m-d H:i:s');
            $now = strtotime(date('Y-m-d H:i:s',strtotime($today))); 
            $your_date = strtotime(date('Y-m-d H:i:s',strtotime($rows->date)));
            $datediff = $now - $your_date;
            if($datediff  > (60 * 60 * 24))
            {
                $this->Staff_Model->updateStatus($salesperson_id);
                $new_row = $this->Staff_Model->get_LatLong_By_ID($salesperson_id);
            }
            else
            {
                $new_row = $rows;
            }
            $data['success'] = true;
            $data['row'] = $new_row;
        } else {
        	$data['success'] = false;
        	$data['message'] = lang( 'location_not_found' );
            $data['row'] = NULL;

        }
        echo json_encode($data);
	}

    function tracksalesperson() {
        $data[ 'title' ] = lang( 'track_salesperson' );
        $this->load->view( 'salesperson/tracksalesperson', $data );
    }

    function getXmldatabetweendate()
    {
        if ( isset( $_POST ) && count( $_POST ) > 0 ) {
            $salesperson_id = $this->input->post( 'id' );
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');

            $hasError = false;
            $data['message'] = '';
            if ($salesperson_id == '') {
                $hasError = true;
                $data['message'] = lang('selectinvalidmessage'). ' ' .lang('salesperson');
            } else if ($from_date == '') {
                $hasError = true;
                $data['message'] = lang('selectinvalidmessage'). ' ' .lang('from_date');
            } else if ($to_date == '') {
                $hasError = true;
                $data['message'] = lang('selectinvalidmessage'). ' ' .lang('to_date');
            }

            if ($hasError) {
                $data['success'] = false;
                echo json_encode($data);
            }

            if (!$hasError) {
                $from_date = $from_date . ' ' . '00:00:00';
                $to_date = $to_date . ' ' . '23:59:59';

                $track_data = $this->Salesperson_Model->getXmlDataOfSalespersonBetweenDate($salesperson_id,$from_date,$to_date);
                $data_map = array();
                $settings = $this->Settings_Model->get_settings_lw();
                $appconfig = get_appconfig();
                if($track_data)
                {
                    foreach ( $track_data as $track ) 
                    {
                        $data_map[] = array(
                            'id' => $track[ 'id' ],
                            'start_time' => date(get_dateTimeFormat(),strtotime($track[ 'start_time' ])),
                            'end_time' => date(get_dateTimeFormat(),strtotime($track[ 'end_time' ])),
                            'start_address' => $track[ 'start_address' ],
                            'end_address' => $track[ 'end_address' ],
                            'distance' => $track[ 'distance' ]
                        );
                    };
                }
                $data['success'] = true;
                $data['track'] = $data_map;
                echo json_encode( $data );
            }
        }
    }

    function displayMap($salesperson_location_id)
    {
        $temp =$this->getXmldata($salesperson_location_id);
        $data['xml_data']= $temp;
        $this->load->view( 'salesperson/popupview', $data );
    }

    function getXmldata($id = NULL)
    {
        $this->load->helper('xml');
        if ($rows = $this->Salesperson_Model->getXmldata($id)) {
            $data1['ob']= simplexml_load_string($rows['route_info']);
            $data1['start_address']=$rows['start_address'];
            $data1['end_address']=$rows['end_address'];
            $data1['staffname']=$rows['staffname'];
            $data1['start_time']=$rows['start_time'];
            $data1['end_time']=$rows['end_time'];
            $data1['distance']=$rows['distance'];
            $data = json_encode($data1);
        } else {
            $data = json_encode(NULL);
        }
        return $data;
    }

    function getLatLongOfAllSalespersons()
    {
        $result = $this->Staff_Model->get_LatLong_Of_All_salesperson();
        if ($result) 
        {             
            $today = date('Y-m-d H:i:s');
            $now = strtotime(date('Y-m-d H:i:s',strtotime($today))); 
            foreach($result as $row)
            {
                $your_date = strtotime(date('Y-m-d H:i:s',strtotime($row['date'])));
                $datediff = $now - $your_date;
                if($datediff  > (60 * 60 * 24))
                {
                    $this->Staff_Model->updateStatus($row['salesman_id']);
                    $new_row[] = $this->Staff_Model->get_LatLong_And_salespersonInfo_By_ID($row['salesman_id']);
                }
                else
                {
                    $new_row[] = $row;
                }
            }            
            $data['success'] = true;
            $data['length'] = sizeof($result);
            $data['row'] = $new_row;
        } else {
            $data['success'] = false;
            $data['message'] = lang( 'location_not_found' );
            $data['row'] = NULL;
        }
        echo json_encode($data);
    }

	
}