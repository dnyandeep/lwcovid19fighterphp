<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Area extends LW_Controller {

	function __construct() {
		parent::__construct();
		$path = $this->uri->segment( 1 );
		if ( !$this->Privileges_Model->has_privilege( $path ) ) {
			$this->session->set_flashdata( 'ntf3', '' . lang( 'you_dont_have_permission' ) );
			redirect( 'panel/' );
		}
	}

	function index() 
    {
        $data[ 'title' ] = lang( 'bit/area' );  
        $this->load->view( 'bit/index', $data );
    }

    function create() {
        if ( $this->Privileges_Model->check_privilege( 'area', 'create' ) ) {
            if ( isset( $_POST ) && count( $_POST ) > 0 ) {
                $city = $this->input->post( 'city' );
                $bit = $this->input->post( 'bit' );
                $distributor_email = $this->input->post( 'distributor_email' );
                $hasError = false;
                $data['message'] = '';
                if ($city == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('city');
                } else if ($bit == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('bit/area');
                } else if ($distributor_email == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('distributer_email');
                } 

                if ($hasError) {
                    $data['success'] = false;
                    echo json_encode($data);
                }
                if (!$hasError) {
                    $params = array(
                        'city' => $city,
                        'bit' => $bit,
                        'distributor_email' => $distributor_email,
                        'day' => $this->input->post( 'day' ),
                    );

                    $data = array(
                        'city' => $city,
                        'bit' => $bit,
                        'distributor_email' => $distributor_email,
                    );

                    $result = $this->Bit_Model->check_data_allready_exist($params);
                    if(!$result)
                    {
                       $area_id = $this->Bit_Model->add_area( $params );

                        if ( $this->input->post( 'custom_fields' ) ) {
                            $custom_fields = array(
                                'custom_fields' => $this->input->post( 'custom_fields' )
                            );
                            $this->Fields_Model->custom_field_data_add_or_update_by_type( $custom_fields, 'area', $area_id );
                        }
                        $data['success'] = true;
                        $data['id'] = $area_id;
                        $data['message'] = lang('bit/area').' '.lang('createmessage');
                        echo json_encode($data); 
                    }
                    else
                    {
                        $data['success'] = false;
                        $data['message'] = lang('bit/area').' '.lang('alreadyexist');
                        echo json_encode($data);
                    }                    
                }
            }
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
    }

    function area( $id ) {
        $data[ 'title' ] = lang( 'update' ).' '.lang( 'bit/area' );
        if ( $this->Privileges_Model->check_privilege( 'area', 'all' ) ) {
            $area = $this->Bit_Model->get_area_by_privileges( $id );
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('customers'));
        }
        if($area) {
            if ( isset( $area[ 'id' ] ) ) {
                if ( isset( $_POST ) && count( $_POST ) > 0 ) {
                    if ( $this->Privileges_Model->check_privilege( 'area', 'edit' ) ) {
                        $city = $this->input->post( 'city' );
                        $bit = $this->input->post( 'bit' );
                        $distributor_email = $this->input->post( 'distributor_email' );
            
                        $hasError = false;
                        $data['message'] = '';
                        if ($city == '') {
                            $hasError = true;
                            $data['message'] = lang('invalidmessage'). ' ' .lang('city');
                        } else if ($bit == '') {
                            $hasError = true;
                            $data['message'] = lang('invalidmessage'). ' ' .lang('bit/area');
                        } else if ($distributor_email == '') {
                            $hasError = true;
                            $data['message'] = lang('invalidmessage'). ' ' .lang('distributer_email');
                        }   
                        if ($hasError) {
                            $data['success'] = false;
                            echo json_encode($data);
                        }
                        if (!$hasError) {
                            $params = array(
                                'city' => $city,
                                'bit' => $bit,
                                'distributor_email' => $distributor_email,
                                'day' => $this->input->post( 'day' ),
                            );
                            $this->Bit_Model->update_area( $id, $params );
                            if ( $this->input->post( 'custom_fields' ) ) {
                                $custom_fields = array(
                                    'custom_fields' => $this->input->post( 'custom_fields' )
                                );
                                $this->Fields_Model->custom_field_data_add_or_update_by_type( $custom_fields, 'area', $id );
                            }
                            $data['success'] = true;
                            $data['message'] = lang('bit/area').' '.lang('updatemessage');
                            echo json_encode($data);
                        }
                    } else {
                        $data['success'] = false;
                        $data['message'] = lang( 'you_dont_have_permission' );
                        echo json_encode($data);
                    }
                } else {
                    $data[ 'areas' ] = $this->Bit_Model->get_areas( $id );
                    $this->load->view( 'bit/area', $data );
                }
            } else {
                show_error( 'Eror' );
            }
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('area'));
        }
    }

    function get_area( $id ) {
        $area = array();
        if ( $this->Privileges_Model->check_privilege( 'area', 'all' ) ) {
            $area = $this->Bit_Model->get_area_by_privileges( $id );
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('area'));
        }
        if($area) {
            $data_areadetail = array(
                'id' => $area[ 'id' ],
                'city' => $area[ 'city' ],
                'bit' => $area[ 'bit' ],
                'distributor_email' => $area[ 'distributor_email' ],
                'day' => $area[ 'day' ]
            );
            echo json_encode( $data_areadetail );
        }
    }

    function remove( $id ) {
        if ( $this->Privileges_Model->check_privilege( 'area', 'all' ) ) {
            $area = $this->Bit_Model->get_area_by_privileges( $id );
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
        if($area) {
            if ( $this->Privileges_Model->check_privilege( 'area', 'delete' ) ) {
                if ( isset( $area[ 'id' ] ) ) {
                    $location = $area['bit'] . '( ' . $area['city'] . ' )';
                    $result = $this->Bit_Model->delete_area( $id ,$location);
                    $bit = lang('bit/area');
                    if ( $result ) {
                        $data['message'] = sprintf( lang( 'success_delete' ), $bit . '' );
                        $data['success'] = true;
                        echo json_encode($data);
                    } else {
                        $data['message'] = sprintf( lang( 'cant_delete' ), $bit . '' );
                        $data['success'] = false;
                        echo json_encode($data);
                    }
                } else {
                    show_error( 'Bit/Area not deleted' );
                }
            } else {
                $data['success'] = false;
                $data['message'] = lang('you_dont_have_permission');
                echo json_encode($data);
            }
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('area'));
        }
    }


    function update_area($id) {
        if ( $this->Privileges_Model->check_privilege( 'area', 'edit' ) ) {
            if (isset($id) && isAdmin()) {
                if (isset($_POST) && count($_POST) > 0 ) {
                    $city = $this->input->post('city');
                    $bit = $this->input->post('bit');
                    $distributor_email = $this->input->post('distributor_email');
                    
                    $hasError = false;
                    
                    if ($city == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('city');
                    } else if ($bit == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('bit/area');
                    } else if ($distributor_email == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('distributer_email');
                    } 
                    
                    if ($hasError) {
                        $return['success'] = false;
                        echo json_encode($return);
                    }
                    if (!$hasError) {
                        $params = array(
                            'city' => $this->input->post('city'),
                            'bit' => $this->input->post('bit'),
                            'distributor_email' => $this->input->post('distributor_email'),
                            'day' => $this->input->post('day')
                        );
                        $this->db->where('id', $id)->update('bit', $params);
                        $return['success'] = true;
                        $return['message'] = lang('bit/area').' '.lang('updatemessage');
                        echo json_encode($return);
                    }
                } else {
                    $return['message'] = lang('errormessage');
                    $return['success'] = false;
                    echo json_encode($return);
                }
            } else {
                $return['message'] = lang('errormessage');
                $return['success'] = false;
                echo json_encode($return);
            }
        } else {
            $return['success'] = false;
            $return['message'] = lang('you_dont_have_permission');
            echo json_encode($return);
        }
    }


	
}