<?php
require_once APPPATH . '/third_party/vendor/autoload.php';
use Dompdf\Dompdf;
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
include_once(APPPATH . 'third_party/script/app_configuration.php');
include_once(APPPATH . 'third_party/script/app_functions.php');
class Solar_Calculator extends CI_Controller {
	public $inactive;
	public $roles;

	function __construct() {
		parent::__construct();
		$this->load->library( 'Google' );
		$this->load->model( 'Settings_Model' );
		define( 'LANG', $this->Settings_Model->get_crm_lang() );
		$this->lang->load( LANG.'_default', LANG);
		$this->lang->load( LANG, LANG );
		$settings = $this->Settings_Model->get_settings( 'lw' );
		$timezone = $settings[ 'default_timezone' ];
		date_default_timezone_set( $timezone );
		$this->load->model( 'Staff_Model' );
		$this->load->model( 'Products_Model' );
        $this->load->model( 'State_Model' );
        $this->load->model( 'District_Model' );
		$this->load->model( 'Emails_Model' );
		$this->load->model( 'Report_Model' );
		$this->load->model( 'Cust_Categories_Model' );
		$this->load->model( 'Cust_Subcategories_Model' );
		$this->load->model( 'Leads_Model' );
		$this->load->model( 'CalculationConfig_Model' );
		$this->load->model( 'KWSolarRateCard_Model' );
		$this->load->library('mail');
		$this->load->library( 'form_validation' );
		$this->load->library('session');
		$this->form_validation->set_error_delimiters( '<div class="error">', '</div>' );
		$this->inactive = $this->config->item( 'inactive' );
		$this->roles = $this->config->item( 'roles' );
		$timezone = $settings[ 'default_timezone' ];
		date_default_timezone_set( $timezone );
	}

	function index() 
	{
		$settings = $this->Settings_Model->get_settings( 'lw' );
		/*$data['state'] = $this->State_Model->get_states();
		$data['districts'] = $this->District_Model->get_districts();
		$data['products'] = $this->Products_Model->get_products_withotjoin();
		$data['products'] = $this->Products_Model->get_products_withotjoin();
		$data['cust_categories'] = $this->Cust_Categories_Model->get_customer_categories();
		$data['cust_subcategories'] = $this->Cust_Subcategories_Model->get_customer_subcategories();*/
		$this->load->view( 'Solar_Calculator/index');
	}

	function states() {
		$states_data = $this->State_Model->get_states();
		print_r( json_encode($states_data) );
	}

	function districts($state_id) {
		$district_data = $this->District_Model->get_district_by_state_id($state_id);
		print_r( json_encode($district_data) );
	}

	function products() {
		$products = $this->Products_Model->get_products_withotjoin();
		print_r( json_encode($products) );
	}

	function customer_categories() {
		$cust_categories = $this->Cust_Categories_Model->get_customer_categories();
		print_r( json_encode($cust_categories) );
	}

	function customer_subcategories($cust_cat_id) {
		$cust_subcategories = $this->Cust_Subcategories_Model->get_customer_subcategories_by_categoryid($cust_cat_id);
		print_r( json_encode($cust_subcategories) );
	}

	function getOtp() 
	{
		if ( isset( $_POST ) && count( $_POST ) > 0 ) 
		{
			$this->form_validation->set_rules('phone','Phone', 'required|regex_match[/^[0-9]{10}$/]');
			if ($this->form_validation->run() == true) 
        	{
        		// generate otp
        		$resendotpcnt = $this->input->post('resendotpcnt') ? $this->input->post('resendotpcnt') : 0;
				$new_otp = $this->generateOTP();
                $mobileNum = $this->input->post( 'phone' );
                $message = '%3C%23%3E Hello , Welcome to AFM Solar System Pvt. Ltd. Your OTP is ' . $new_otp . ' pfH7juRe9tO';  
                //$otpResponse = $this->sendsmsGET($mobileNum, $message);
                $new_otp = '123456';
                $otpResponse = true;
                if($otpResponse)
                {
                	$resendotpcnt++;
                	$this->session->set_userdata( array(
						'generatedotp' => $new_otp,
						'phone' => $mobileNum,
						'resendotpcnt' => $resendotpcnt
					));
					$data['$resendotpcnt'] = $resendotpcnt;
                	$this->session->set_flashdata( 'ntf1', lang( 'otpsent' ).' '. $mobileNum );
                	redirect( site_url() . 'Solar_Calculator' );
                }
                else
                {
                	$this->session->set_flashdata( 'ntf4', lang( 'errormessage' ) );
					redirect( site_url() . 'Solar_Calculator' );
                }
        	}
        	else
        	{	
        		$this->session->set_flashdata( 'ntf4', lang( 'The Phone field is not in the correct format.' ) );
				redirect( site_url() . 'Solar_Calculator' );
        	}
		}
	}

	function create() 
	{	
		$this->load->library( 'import' );
		if ( isset( $_POST ) && count( $_POST ) > 0 ) 
		{
			$unit_type = $this->input->post('unittype');
			$units = $this->input->post('units');
			$no_of_months = $this->input->post('months');
			$approx_unit_rate = $this->input->post('approx_unit_rate');
			$params = array(
				'name' => $this->input->post('name'),
                'middle_name' => $this->input->post('middle_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
                'pin_code' => $this->input->post('pincode'),
                'city' => $this->input->post('city'),
                'state_id' => $this->input->post('state_id'),
                'district_id' => $this->input->post('district_id'),
                'cust_category_id'=>$this->input->post('cust_category_id'),
                'cust_subcategory_id'=>$this->input->post('cust_subcategory_id'),
                'product_id'=>$this->input->post('product_id'),
                'consumer_number'=> $this->input->post('consumer_number'),
                'supplier'=> $this->input->post('supplier'),
                'units'=> $units,
                'unit_type'=> $unit_type,
                'no_of_months'=> $no_of_months,
                'approx_unit_rate'=> $approx_unit_rate,

                'status' => 1,
                'source' => 1,
                'date_contacted' => '0000-00-00 00:00:00',
                'assigned_id'=>1,
                'staff_id'=>1,
                'created' => date( 'Y-m-d H:i:s' )
			);		

			if($this->input->post('calculationtype') == 'fileupload')
            {
            	$file_path = "uploads/lead_attachments/";
				if (!is_dir($file_path)) 
               	{
                   	mkdir($file_path, 0777, TRUE);
               	}
            	$config[ 'upload_path' ] = $file_path;
				$config[ 'allowed_types' ] = 'pdf|jpg|png|jpeg';
				$config[ 'max_size' ] = '1000';
				$this->load->library( 'upload', $config );
				$date = date('Y-m-d H:i:s');
                $name = strtotime($date);
                $ext = pathinfo(basename( $_FILES['electricity_bill']['name']), PATHINFO_EXTENSION);
                $act_file_path = $file_path . $name.'.'.$ext;
                $config['file_name'] = $name;
               
                /*move_uploaded_file($_FILES['electricity_bill']['tmp_name'], $act_file_path);
                die;*/
				if ( !move_uploaded_file($_FILES['electricity_bill']['tmp_name'], $act_file_path) ) 
				{
					$this->session->set_flashdata( 'ntf4', lang('something wrong during upload file,please try after sometime') );
				}
				else
				{
					$params['attachment'] = $act_file_path;
				}
            }
            
			
			$lead_id = $this->Leads_Model->add_lead( $params );
            if($lead_id)
            {                 
                // generate lead id
               	$year= date("Y");
                $statePadded = sprintf("%02d", $this->input->post('state_id'));
                $districtPadded = sprintf("%04d",$this->input->post('district_id'));
                $idPadded = sprintf("%06d", $lead_id);
                $code=$year.$statePadded.$districtPadded.$idPadded;
                $lead_array = array('lead_number' => $code );
                $rows=$this->Leads_Model->edit_lead($lead_array,$lead_id);
                if($rows)
                {
                	$lead_detail = $this->Leads_Model->get_lead_by_id( $lead_id );
                	$state = $this->State_Model->get_state_name_by_id($lead_detail['state_id']);
                   	$district = $this->District_Model->get_district_name_by_id($lead_detail['district_id']);
                   	// Send email to support team
                    // get email from table
                    $setconfig = $this->Settings_Model->get_settings_lw();
                    $to = 'support@afmsolarsystem.com';
                    $name = $this->input->post('name').' '.$this->input->post('middle_name').' '.$this->input->post('last_name');
                    $template = $this->Emails_Model->get_template('newlead', 'new_lead_generated');
                    $message_vars = array(
                        '{staffname}' => 'Sir/madam',
                        '{name}' => $name,
                       	'{contact_no}' => $this->input->post('phone'),
                        '{email}' => $this->input->post('email'),
                        '{address}' => $this->input->post('address'),
                        '{city}' => $this->input->post('city'),
                        '{state}' => $state,
                        '{district}' => $district,
                    );

                    $subject = strtr($template['subject'], $message_vars);
                    $message = strtr($template['message'], $message_vars);
                    $data = array(
                        'from_name' => $template['from_name'],
                        'email' => $to,
                        'subject' => $subject,
                        'message' => $message,
                        'created' => date( "Y.m.d H:i:s" ),
                        'status' => 0
                    );
                    $path = '';
                    if(!empty($params['attachment']))
                    {
                        $path = $params['attachment'];                            
                    }

                    if($to) 
                    {                           
                        //$this->mail->send_email($to, $template['from_name'], $subject, $message,$path);
                    }

                   	if($this->input->post('calculationtype') == 'unit')
                   	{
                   		$config_detail = $this->CalculationConfig_Model->getLatestCalConfiguration(); 
                   		$requiredKW = '';
                   		// Calculation and generate pdf
                   		if($unit_type=='Yearly')
                   		{
                   			$requiredKW = ($units/365)/$config_detail->required_kw_factor;
                   		}
                    	else if($unit_type=='Monthly')
                    	{
                    		$requiredKW = ($units/($no_of_months*30))/$config_detail->required_kw_factor;
                    	}
                    	$requiredKW = number_format((float)$requiredKW, 2, '.', '');
                    	$systemGeneratedUnits = $requiredKW * $config_detail->generated_units_factor * 365;
                   		$totalRevenueGenerated = $systemGeneratedUnits * $approx_unit_rate;
                   		$ratecard = $this->KWSolarRateCard_Model->get_all_KWSolarRateCard();

                   		for($i=0;$i<count($ratecard);$i++)
                   		{

                   			if($i==0)
                   			{
                   				if(($requiredKW < $ratecard[$i]['lower_kw']) || ($requiredKW >= $ratecard[$i]['lower_kw'] && $requiredKW <= $ratecard[$i]['higher_kw']))
                   				{
                   					$monoSystemCost = $ratecard[$i]['mono_rate_per_kw'] * $requiredKW;
                   					$polyAplusCost = $ratecard[$i]['poly_a_plus_rate_per_kw'] * $requiredKW;
                   					$polyACost = $ratecard[$i]['poly_a_rate_per_kw'] * $requiredKW;
                   				}                 				
                   			}
                   			else if($i==(count($ratecard)-1))
                   			{
                   				if(($requiredKW >= $ratecard[$i]['lower_kw'] && $requiredKW <= $ratecard[$i]['higher_kw']) || ($requiredKW < $ratecard[$i]['higher_kw']))
                   				{
                   					$monoSystemCost = $ratecard[$i]['mono_rate_per_kw'] * $requiredKW;
                   					$polyAplusCost = $ratecard[$i]['poly_a_plus_rate_per_kw'] * $requiredKW;
                   					$polyACost = $ratecard[$i]['poly_a_rate_per_kw'] * $requiredKW;
                   				}
                   			}
                   			else
                   			{
                   				if(($requiredKW >= $ratecard[$i]['lower_kw'] && $requiredKW <= $ratecard[$i]['higher_kw']))
                   				{
                   					$monoSystemCost = $ratecard[$i]['mono_rate_per_kw'] * $requiredKW;
                   					$polyAplusCost = $ratecard[$i]['poly_a_plus_rate_per_kw'] * $requiredKW;
                   					$polyACost = $ratecard[$i]['poly_a_rate_per_kw'] * $requiredKW;
                   				}
                   			}
                   		}

                   		$monoYears = $monoSystemCost/$totalRevenueGenerated;
                   		$polyAPlusYears = $polyAplusCost/$totalRevenueGenerated;
                   		$polyAYear = $polyACost/$totalRevenueGenerated;


                   		
                   		
                   		$category = $this->Cust_Categories_Model->get_category_name_by_id($lead_detail['cust_category_id']);
                   		$pdfdata = array(
                   			'lead_detail' => $lead_detail,
                   			'ratecard' => $ratecard,
                   			'monoSystemCost' => $monoSystemCost,
                   			'polyAplusCost' => $polyAplusCost,
                   			'polyACost' => $polyACost,
                   			'monoYears' => $monoYears,
                   			'polyAPlusYears' => $polyAPlusYears,
                   			'polyAYear' => $polyAYear,
                   			'totalRevenueGenerated' => $totalRevenueGenerated,
                   			'systemGeneratedUnits' => $systemGeneratedUnits,
                   			'requiredKW' => $requiredKW,
                   			'state' => $state,
                   			'district' => $district,
                   			'category' => $category
                   		);             
                   		$this->session->sess_destroy();
                   		$pdfresult = $this->generatePdf($pdfdata);
                   	}                   
                   	$this->session->sess_destroy();
                   	$data['calculationtype'] = $this->input->post('calculationtype');
                   	$this->load->view( 'Solar_Calculator/thanksmessage',$data);                 
				}
			}
		}		
	}


	function generatePdf($data)
	{
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		if (!is_dir('uploads/files/generated_pdf_files/'.'calculator')) {
				mkdir('./uploads/files/generated_pdf_files/'.'calculator', 0777, true);
			}
		$data[ 'settings' ] = $this->Settings_Model->get_settings_lw();
		$this->load->view( 'solar_calculator/pdf', $data );
		$appconfig = get_appconfig();
		$file_name =  $data['lead_detail']['name'].'_'.$data['lead_detail']['last_name'] .'.pdf';
		//echo '<pre>';print_r($file_name);die;
		$html = $this->output->get_output();
		require_once APPPATH . '/third_party/vendor/autoload.php';
		$this->dompdf = new DOMPDF();
		$this->load->library( 'dom' );
		$this->dompdf->loadHtml( $html );
		$this->dompdf->set_option( 'isRemoteEnabled', TRUE );
		$this->dompdf->setPaper( 'A4', 'portrait' );
		$this->dompdf->render();
		$canvas = $this->dompdf->getCanvas(); 
 
		// Get height and width of page 
		$w = $canvas->get_width(); 
		$h = $canvas->get_height(); 
 
		// Specify watermark image 
		$imageURL = FCPATH.'uploads/lw_settings/'.$data['settings']['app_logo']; 
		$imgWidth = 200; 
		$imgHeight = 150; 
 
		// Set image opacity 
		$canvas->set_opacity(.5); 
 
		// Specify horizontal and vertical position 
		$x = (($w-$imgWidth)/2); 
		$y = (($h-$imgHeight)/3); 
 
		// Add an image to the pdf 
		$canvas->image($imageURL, $x, $y, $imgWidth, $imgHeight);

		$output = $this->dompdf->output();
		$this->dompdf->stream($file_name,array('Attachment' => 0));
		if($output)
		redirect( base_url( 'solar_calculator'  ) );
		//return $file_name;
		
	}


	function validateotp() 
	{
		if($this->session->generatedotp)
		{
			if ( isset( $_POST ) && count( $_POST ) > 0 ) 
			{
				$this->form_validation->set_rules('otp','OTP', 'trim|required');
				if ($this->form_validation->run() == true) 
        		{
        			$oldotp = $this->session->generatedotp;
        			$newotp = $this->input->post('otp');
        			if($oldotp===$newotp)
        			{
        				$data['phone'] = $this->session->phone;
        				$data['state'] = $this->State_Model->get_states();
						$data['districts'] = $this->District_Model->get_districts();
						$data['products'] = $this->Products_Model->get_products_withotjoin();
						$data['products'] = $this->Products_Model->get_products_withotjoin();
						$data['cust_categories'] = $this->Cust_Categories_Model->get_customer_categories();
						$data['cust_subcategories'] = $this->Cust_Subcategories_Model->get_customer_subcategories();
						$this->session->set_flashdata( 'ntf1', lang( 'verification_msg' ) );	
						$this->load->view( 'Solar_Calculator/register', $data );
        			}
        			else
        			{
        				$this->session->set_flashdata( 'ntf4', lang( 'incorrectotp' ) );
						redirect( site_url() . 'Solar_Calculator' );        			
        			}
        		}
        		else
        		{	
        			$this->session->set_flashdata( 'ntf4', lang( 'customercanffindmail' ) );
					redirect( site_url() . 'Solar_Calculator' );
        		}
			}
		}
		else
		{
			$data['calculationtype'] = 'sessionexpire';
        	$this->load->view( 'Solar_Calculator/thanksmessage',$data);
		}
	}

}
