<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Brands extends LW_Controller {

	function __construct() {
		parent::__construct();
		$path = $this->uri->segment( 1 );
		if ( !$this->Privileges_Model->has_privilege( $path ) ) {
			$this->session->set_flashdata( 'ntf3', '' . lang( 'you_dont_have_permission' ) );
			redirect( 'panel/' );
		}
	}

	function index() 
    {
        $data[ 'title' ] = lang( 'brands' );  
        $this->load->view( 'brands/index', $data );
    }


    function create() {
        if ( $this->Privileges_Model->check_privilege( 'units', 'create' ) ) {
            if ( isset( $_POST ) && count( $_POST ) > 0 ) {
                $name = $this->input->post( 'name' );
                $manufacturer_id = $this->input->post( 'manufacturer_id' );
                $hasError = false;
                $data['message'] = '';
                if ($name == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('brand');
                } else if ($manufacturer_id == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('manufacturer');
                } 

                if ($hasError) {
                    $data['success'] = false;
                    echo json_encode($data);
                }
                if (!$hasError) {
                    $params = array(
                        'name' => $name,
                        'manufacturer_id' => $this->input->post('manufacturer_id')
                    );
                    
                    $result = $this->Brand_Model->check_data_allready_exist($params);
                    if(!$result)
                    {
                        $brand_id = $this->Brand_Model->add_brand( $params );

                        if ( $this->input->post( 'custom_fields' ) ) {
                            $custom_fields = array(
                                'custom_fields' => $this->input->post( 'custom_fields' )
                            );
                            $this->Fields_Model->custom_field_data_add_or_update_by_type( $custom_fields, 'brands', $brand_id );
                        }
                        $data['success'] = true;
                        $data['id'] = $brand_id;
                        $data['message'] = lang('brand').' '.lang('createmessage');
                        echo json_encode($data); 
                    }   
                    else
                    {
                        $data['success'] = false;
                        $data['message'] = lang('brand').' '.lang('alreadyexist');
                        echo json_encode($data);
                    }                                  
                }
            }
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
    }

    function remove( $id ) {
        if ( $this->Privileges_Model->check_privilege( 'brands', 'all' ) ) {
            $brand = $this->Brand_Model->get_brand_by_privileges( $id );
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
        if($brand) {
            if ( $this->Privileges_Model->check_privilege( 'brands', 'delete' ) ) {
                if ( isset( $brand[ 'id' ] ) ) {
                    $result = $this->Brand_Model->delete_brand( $id ,$brand['name']);
                    $brand = lang('brand');
                    if ( $result ) {
                        $data['message'] = sprintf( lang( 'success_delete' ), $brand . '' );
                        $data['success'] = true;
                        echo json_encode($data);
                    } else {
                        $data['message'] = sprintf( lang( 'cant_delete' ), $brand . '' );
                        $data['success'] = false;
                        echo json_encode($data);
                    }
                } else {
                    show_error( 'Unit not deleted' );
                }
            } else {
                $data['success'] = false;
                $data['message'] = lang('you_dont_have_permission');
                echo json_encode($data);
            }
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('units'));
        }
    }


    function update_brand($id) {
        if ( $this->Privileges_Model->check_privilege( 'brands', 'edit' ) ) {
            if (isset($id) && isAdmin()) {
                if (isset($_POST) && count($_POST) > 0 ) {
                    $name = $this->input->post('name');
                    $manufacturer_id = $this->input->post('manufacturer_id');                   
                    $hasError = false;
                    
                    if ($name == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('name');
                    } else if ($manufacturer_id == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('manufacturer');
                    } 
                    
                    if ($hasError) {
                        $return['success'] = false;
                        echo json_encode($data);
                    }
                    if (!$hasError) {
                        $params = array(
                            'name' => $this->input->post('name'),
                            'manufacturer_id' => $this->input->post('manufacturer_id'),
                        );

                        $result = $this->Brand_Model->check_data_allready_exist($params,$id);
                        if(!$result)
                        {
                            $this->db->where('id', $id)->update('brands', $params);
                            $return['success'] = true;
                            $return['message'] = lang('brand').' '.lang('updatemessage');
                            echo json_encode($return);
                        }
                        else
                        {
                            $data['success'] = false;
                            $data['message'] = lang('brand').' '.lang('alreadyexist');
                            echo json_encode($data);
                        }                        
                    }
                } else {
                    $return['message'] = lang('errormessage');
                    $return['success'] = false;
                    echo json_encode($return);
                }
            } else {
                $return['message'] = lang('errormessage');
                $return['success'] = false;
                echo json_encode($return);
            }
        } else {
            $return['success'] = false;
            $return['message'] = lang('you_dont_have_permission');
            echo json_encode($return);
        }
    }	
}