<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Units extends LW_Controller {

	function __construct() {
		parent::__construct();
		$path = $this->uri->segment( 1 );
		if ( !$this->Privileges_Model->has_privilege( $path ) ) {
			$this->session->set_flashdata( 'ntf3', '' . lang( 'you_dont_have_permission' ) );
			redirect( 'panel/' );
		}
	}

	function index() 
    {
        $data[ 'title' ] = lang( 'manufacturers' );  
        $this->load->view( 'units/index', $data );
    }

    function getBaseUnits() {
        $base_units = $this->Unit_Model->get_base_units();
        print_r( json_encode($base_units) );
    }

    function create() {
        if ( $this->Privileges_Model->check_privilege( 'units', 'create' ) ) {
            if ( isset( $_POST ) && count( $_POST ) > 0 ) {
                $name = $this->input->post( 'name' );
                
                $hasError = false;
                $data['message'] = '';
                if ($name == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('unit');
                } else if ($this->Unit_Model->isDuplicate( $this->input->post( 'name' ) )) {
                    $hasError = true;
                    $data['message'] = lang('unit_exist');
                } 

                if ($hasError) {
                    $data['success'] = false;
                    echo json_encode($data);
                }
                if (!$hasError) {
                    $params = array(
                        'name' => $name,
                        'base_unit' => $this->input->post('base_unit')
                    );
                    
                    $result = $this->Unit_Model->check_data_allready_exist($params);
                    if(!$result)
                    {
                        $unit_id = $this->Unit_Model->add_unit( $params );

                        if ( $this->input->post( 'custom_fields' ) ) {
                            $custom_fields = array(
                                'custom_fields' => $this->input->post( 'custom_fields' )
                            );
                            $this->Fields_Model->custom_field_data_add_or_update_by_type( $custom_fields, 'area', $unit_id );
                        }
                        $data['success'] = true;
                        $data['id'] = $unit_id;
                        $data['message'] = lang('unit').' '.lang('createmessage');
                        echo json_encode($data); 
                    }   
                    else
                    {
                        $data['success'] = false;
                        $data['message'] = lang('unit').' '.lang('alreadyexist');
                        echo json_encode($data);
                    }                                  
                }
            }
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
    }

    function remove( $id ) {
        if ( $this->Privileges_Model->check_privilege( 'units', 'all' ) ) {
            $unit = $this->Unit_Model->get_unit_by_privileges( $id );
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
        if($unit) {
            if ( $this->Privileges_Model->check_privilege( 'units', 'delete' ) ) {
                if ( isset( $unit[ 'id' ] ) ) {
                    $result = $this->Unit_Model->delete_unit( $id ,$unit['name']);
                    $unit = lang('unit');
                    if ( $result ) {
                        $data['message'] = sprintf( lang( 'success_delete' ), $unit . '' );
                        $data['success'] = true;
                        echo json_encode($data);
                    } else {
                        $data['message'] = sprintf( lang( 'cant_delete' ), $unit . '' );
                        $data['success'] = false;
                        echo json_encode($data);
                    }
                } else {
                    show_error( 'Unit not deleted' );
                }
            } else {
                $data['success'] = false;
                $data['message'] = lang('you_dont_have_permission');
                echo json_encode($data);
            }
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('units'));
        }
    }


    function update_unit($id) {
        if ( $this->Privileges_Model->check_privilege( 'units', 'edit' ) ) {
            if (isset($id) && isAdmin()) {
                if (isset($_POST) && count($_POST) > 0 ) {
                    $name = $this->input->post('name');
                    
                    $hasError = false;
                    
                    if ($name == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('name');
                    }  
                    
                    if ($hasError) {
                        $return['success'] = false;
                        echo json_encode($data);
                    }
                    if (!$hasError) {
                        $params = array(
                            'name' => $this->input->post('name'),
                            'base_unit' => $this->input->post('base_unit'),
                        );

                        $result = $this->Unit_Model->check_data_allready_exist($params,$id);
                        if(!$result)
                        {
                            $this->db->where('id', $id)->update('units', $params);
                            $return['success'] = true;
                            $return['message'] = lang('unit').' '.lang('updatemessage');
                            echo json_encode($return);
                        }
                        else
                        {
                            $data['success'] = false;
                            $data['message'] = lang('unit').' '.lang('alreadyexist');
                            echo json_encode($data);
                        }                        
                    }
                } else {
                    $return['message'] = lang('errormessage');
                    $return['success'] = false;
                    echo json_encode($return);
                }
            } else {
                $return['message'] = lang('errormessage');
                $return['success'] = false;
                echo json_encode($return);
            }
        } else {
            $return['success'] = false;
            $return['message'] = lang('you_dont_have_permission');
            echo json_encode($return);
        }
    }

    // For advertising material
    function getAllUnits() {
        $units = $this->Unit_Model->get_units();
        print_r( json_encode($units) );
    }
	
}