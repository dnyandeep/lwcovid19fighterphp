<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Cities extends LW_Controller {

	function __construct() {
		parent::__construct();
		$path = $this->uri->segment( 1 );
		if ( !$this->Privileges_Model->has_privilege( $path ) ) {
			$this->session->set_flashdata( 'ntf3', '' . lang( 'you_dont_have_permission' ) );
			redirect( 'panel/' );
		}
	}

	function index() 
    {
        $data[ 'title' ] = lang( 'brands' );  
        $this->load->view( 'cities/index', $data );
    }


    function create() {
        if ( $this->Privileges_Model->check_privilege( 'cities', 'create' ) ) {
            if ( isset( $_POST ) && count( $_POST ) > 0 ) {
                $city = $this->input->post( 'city' );
                $hasError = false;
                $data['message'] = '';
                if ($city == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('city');
                } else if ($this->Cities_Model->isDuplicate( $this->input->post( 'city' ) )){
                    $hasError = true;
                    $data['message'] = lang('city_exist');
                }  

                if ($hasError) {
                    $data['success'] = false;
                    echo json_encode($data);
                }
                if (!$hasError) {
                    $params = array(
                        'city' => $city
                    );
                    
                    $city_id = $this->Cities_Model->add_city( $params );
                    
                    if ( $this->input->post( 'custom_fields' ) ) {
                        $custom_fields = array(
                            'custom_fields' => $this->input->post( 'custom_fields' )
                        );
                        $this->Fields_Model->custom_field_data_add_or_update_by_type( $custom_fields, 'cities', $city_id );
                    }
                    $data['success'] = true;
                    $data['id'] = $city_id;
                    $data['message'] = lang('city').' '.lang('createmessage');
                    echo json_encode($data);
                }
            }
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
    }

    function remove( $id ) {
        if ( $this->Privileges_Model->check_privilege( 'cities', 'all' ) ) {
            $city = $this->Cities_Model->get_city_by_privileges( $id );
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
        if($city) {
            if ( $this->Privileges_Model->check_privilege( 'cities', 'delete' ) ) {
                if ( isset( $city[ 'id' ] ) ) {
                    $result = $this->Cities_Model->delete_city( $id ,$city['city']);
                    $city = lang('city');
                    if ( $result ) {
                        $data['message'] = sprintf( lang( 'success_delete' ), $city . '' );
                        $data['success'] = true;
                        echo json_encode($data);
                    } else {
                        $data['message'] = sprintf( lang( 'cant_delete' ), $city . '' );
                        $data['success'] = false;
                        echo json_encode($data);
                    }
                } else {
                    show_error( 'Unit not deleted' );
                }
            } else {
                $data['success'] = false;
                $data['message'] = lang('you_dont_have_permission');
                echo json_encode($data);
            }
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('units'));
        }
    }


    function update_city($id) {
        if ( $this->Privileges_Model->check_privilege( 'cities', 'edit' ) ) {
            if (isset($id) && isAdmin()) {
                if (isset($_POST) && count($_POST) > 0 ) {
                    $city = $this->input->post('city');

                    $hasError = false;
                    
                    if ($city == '') {
                        $hasError = true;
                        $data['message'] = lang('required_message').' '.lang('city');
                    } else if ($this->Cities_Model->isDuplicate( $this->input->post( 'city' ) ,$id)){
                        $hasError = true;
                        $data['message'] = lang('city_exist');
                    }
                    
                    if ($hasError) {
                        $data['success'] = false;
                        echo json_encode($data);
                    }
                    if (!$hasError) {
                        $params = array(
                            'city' => $this->input->post('city')
                        );

                        $this->db->where('id', $id)->update('cities', $params);
                        $return['success'] = true;
                        $return['message'] = lang('city').' '.lang('updatemessage');
                        echo json_encode($return);
                                               
                    }
                } else {
                    $return['message'] = lang('errormessage');
                    $return['success'] = false;
                    echo json_encode($return);
                }
            } else {
                $return['message'] = lang('errormessage');
                $return['success'] = false;
                echo json_encode($return);
            }
        } else {
            $return['success'] = false;
            $return['message'] = lang('you_dont_have_permission');
            echo json_encode($return);
        }
    }	
}