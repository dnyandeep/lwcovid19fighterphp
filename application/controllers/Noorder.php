<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Noorder extends LW_Controller {

	function index() 
    {
        $data[ 'title' ] = lang( 'no_orders_list' );  
        $this->load->view( 'noorder/index', $data );
    }

    function getNoOrderlist() 
    {
        $data['result'] = array();
        if ( isset( $_POST ) && count( $_POST ) > 0 ) 
        {
            $customer = $this->input->post('customer') ? $this->input->post('customer') : NULL ;
            $salesman_id = $this->input->post('salesman_id') ? $this->input->post('salesman_id') : NULL ;
            $beat = $this->input->post('beat') ? $this->input->post('beat') : NULL;
            $from = $this->input->post('from') ; 
            $to = $this->input->post('to') ;
            
            $hasError = false;
            $data['message'] = '';
            
            if( $from == '' ) {
                $hasError = true;
                $data['message'] = lang('selectinvalidmessage'). ' ' .lang('from').' '.lang('date');
            } else if ( $to == '') {
                $hasError = true;
                $data['message'] = lang('selectinvalidmessage'). ' ' .lang('to').' '.lang('date');
            }
            
            if ($hasError) {
                $data['success'] = false;
                echo json_encode($data);
            }
            if (!$hasError) {
                $data['result'] = $this->Orders_Model->getNoOrderlist($customer, $salesman_id, $beat, $from, $to);
                if(!empty($data['result']))
                {
                    $data['success'] = true;
                    echo json_encode($data);
                }
                else
                {
                    $data['success'] = false;
                    echo json_encode($data);
                }
                
            }
        }
    }

    function getLatLong()
    {
        $noorderid = $this->input->post('noorderid');
        if ($rows = $this->Orders_Model->getLatLongById($noorderid)) 
        {             
            $data['success'] = true;
            $data['row'] = $rows;
        } else {
            $data['success'] = false;
            $data['message'] = lang( 'location_not_found' );
            $data['row'] = NULL;
        }
        echo json_encode($data);

    }
    
}