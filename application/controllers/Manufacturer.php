<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Manufacturer extends LW_Controller {

	function __construct() {
		parent::__construct();
		$path = $this->uri->segment( 1 );
		if ( !$this->Privileges_Model->has_privilege( $path ) ) {
			$this->session->set_flashdata( 'ntf3', '' . lang( 'you_dont_have_permission' ) );
			redirect( 'panel/' );
		}
	}

	function index() 
    {
        $data[ 'title' ] = lang( 'manufacturers' );  
        $this->load->view( 'manufacturers/index', $data );
    }

    function create() {
        if ( $this->Privileges_Model->check_privilege( 'manufacturer', 'create' ) ) {
            if ( isset( $_POST ) && count( $_POST ) > 0 ) {
                $name = $this->input->post( 'name' );
                
                $hasError = false;
                $data['message'] = '';
                if ($name == '') {
                    $hasError = true;
                    $data['message'] = lang('invalidmessage'). ' ' .lang('manufacturer');
                } else if ($this->Manufacturer_Model->isDuplicate( $this->input->post( 'name' ) )) {
                    $hasError = true;
                    $data['message'] = lang('manufacturer_exist');
                } 

                if ($hasError) {
                    $data['success'] = false;
                    echo json_encode($data);
                }
                if (!$hasError) {
                    $params = array(
                        'name' => $name
                    );
                    
                    $manufacturer_id = $this->Manufacturer_Model->add_manufacturer( $params );

                    if ( $this->input->post( 'custom_fields' ) ) {
                        $custom_fields = array(
                            'custom_fields' => $this->input->post( 'custom_fields' )
                        );
                        $this->Fields_Model->custom_field_data_add_or_update_by_type( $custom_fields, 'area', $manufacturer_id );
                    }
                    $data['success'] = true;
                    $data['id'] = $manufacturer_id;
                    $data['message'] = lang('manufacturer').' '.lang('createmessage');
                    echo json_encode($data);                                      
                }
            }
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
    }

    function remove( $id ) {
        if ( $this->Privileges_Model->check_privilege( 'manufacturer', 'all' ) ) {
            $manufacturer = $this->Manufacturer_Model->get_manufacturer_by_privileges( $id );
        } else {
            $data['success'] = false;
            $data['message'] = lang('you_dont_have_permission');
            echo json_encode($data);
        }
        if($manufacturer) {
            if ( $this->Privileges_Model->check_privilege( 'manufacturer', 'delete' ) ) {
                if ( isset( $manufacturer[ 'id' ] ) ) {
                    $result = $this->Manufacturer_Model->delete_manufacturer( $id ,$manufacturer['name']);
                    $manufacturer = lang('manufacturer');
                    if ( $result ) {
                        $data['message'] = sprintf( lang( 'success_delete' ), $manufacturer . '' );
                        $data['success'] = true;
                        echo json_encode($data);
                    } else {
                        $data['message'] = sprintf( lang( 'cant_delete' ), $manufacturer . '' );
                        $data['success'] = false;
                        echo json_encode($data);
                    }
                } else {
                    show_error( 'Manufacturer not deleted' );
                }
            } else {
                $data['success'] = false;
                $data['message'] = lang('you_dont_have_permission');
                echo json_encode($data);
            }
        } else {
            $this->session->set_flashdata( 'ntf3',lang( 'you_dont_have_permission' ) );
            redirect(base_url('manufacturer'));
        }
    }


    function update_manufacturer($id) {
        if ( $this->Privileges_Model->check_privilege( 'manufacturer', 'edit' ) ) {
            if (isset($id) && isAdmin()) {
                if (isset($_POST) && count($_POST) > 0 ) {
                    $name = $this->input->post('name');
                    
                    $hasError = false;
                    
                    if ($name == '') {
                        $hasError = true;
                        $return['message'] = lang('required_message').' '.lang('name');
                    } else if ($this->Manufacturer_Model->isDuplicate( $name , $id )) {
                        $hasError = true;
                        $data['message'] = lang('manufacturer_exist');
                    } 
                    
                    if ($hasError) {
                        $return['success'] = false;
                        echo json_encode($data);
                    }
                    if (!$hasError) {
                        $params = array(
                            'name' => $this->input->post('name')
                        );
                        $this->db->where('id', $id)->update('manufacturer', $params);
                        $return['success'] = true;
                        $return['message'] = lang('manufacturer').' '.lang('updatemessage');
                        echo json_encode($return);
                    }
                } else {
                    $return['message'] = lang('errormessage');
                    $return['success'] = false;
                    echo json_encode($return);
                }
            } else {
                $return['message'] = lang('errormessage');
                $return['success'] = false;
                echo json_encode($return);
            }
        } else {
            $return['success'] = false;
            $return['message'] = lang('you_dont_have_permission');
            echo json_encode($return);
        }
    }

    function getManufacturers() {
        $manufacturers = $this->Manufacturer_Model->get_manufacturers();
        print_r( json_encode($manufacturers) );
    }

	
}