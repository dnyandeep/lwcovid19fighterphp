<?php
class Salesperson_Model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function getXmlDataOfSalespersonBetweenDate($salesperson_id, $from_date, $to_date)
	{
		$this->db->select("id,start_time,end_time,start_address,end_address,CONCAT_WS(' ', distance, 'Km') as distance", FALSE)
			->from('salesman_locations')
			->where('salesman_id',$salesperson_id)
            ->where('end_time >=',$from_date)
            ->where('end_time <=',$to_date);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
        	foreach($query->result_array() as $row)
        	{
        		$data[] = $row;
            }
            return $data;
        }
        else
        {
        	return FALSE;
        }
	}

    function getXmldata($id) 
    {
        $this->db->select('
            end_time,start_time,route_info,start_address,end_address,staff.staffname,distance')
        ->from('salesman_locations')
        ->join('staff','staff.id=salesman_locations.salesman_id')
        ->where('salesman_locations.id',$id);
        $sql=$this->db->get();
         if ($sql->num_rows() > 0) {
            foreach (($sql->result_array()) as $row) {
                $data = $row;
            }
            return $data;
        }
        return false;
    }
}