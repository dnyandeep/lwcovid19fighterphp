<?php
class Manufacturer_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function add_manufacturer( $params ) {
		$this->db->insert( 'manufacturer', $params );
		$manufacturer_id = $this->db->insert_id();
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $this->session->usr_id . '"> ' . $this->session->staffname . '</a> ' . lang( 'addedamanufacturer' ) . ' <a href="manufacturer/manufacturer/' . $manufacturer_id . '">' . '</a>' ),
			'staff_id' => $this->session->usr_id
		) );		
		return $manufacturer_id;
	}

	function isDuplicate( $name , $id=NULL ) {
		if($id)
		{
			$this->db->get_where( 'manufacturer', array( 'name' => $name ,'id !=' => $id ), 1 );
		}
		else
		{
			$this->db->get_where( 'manufacturer', array( 'name' => $name ), 1 );
		}
		$manufacturer = $this->db->affected_rows();
		if($manufacturer > 0) {
			$result = true;
		} else {
			$result = false;
		}
		return $result;
	}

	function check_data_allready_exist( $params )
	{
		$query = $this->db->get_where('bit', $params);
        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
        	return false;
        }
	}

	function get_all_manufacturers()
	{
		return $this->db->get_where( 'manufacturer', array( '' ) )->result_array();
	}

	function get_manufacturer_by_privileges( $id, $staff_id='' ) { 
		return $this->db->get_where( 'manufacturer', array( 'id' => $id ) )->row_array();		
	}

	function delete_manufacturer( $id ,$name) {
		
			$response = $this->db->delete( 'manufacturer', array( 'id' => $id ) );
			$loggedinuserid = $this->session->usr_id;
			$this->db->insert( 'logs', array(
				'date' => date( 'Y-m-d H:i:s' ),
				'detail' => ( '<a href="staff/staffmember/' . $loggedinuserid . '"> ' . $this->session->staffname . '</a> ' . lang( 'deleted' ) . ' '. $name . lang( 'manufacturer' ) .'' ),
				'staff_id' => $this->session->usr_id
			) );
			return true;
		
	}

	function get_manufacturers() { 
		return $this->db->get_where( 'manufacturer', array( '' ) )->result_array();
	}
}
