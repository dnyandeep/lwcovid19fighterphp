<?php

class Cust_Category_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	


	// for add subcategory
	function add_custcategory( $params ) { 
		$this->db->insert( 'customer_categories', $params );
		$category = $this->db->insert_id();
		
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $this->session->usr_id . '"> ' . $this->session->staffname . '</a> ' . lang( 'addedanewcustcategory' ) . ' <a href="customercategories/category/' . $category . '"> ' . lang( 'category' ) . '</a>' ),
			'staff_id' => $this->session->usr_id
		) );
		return $category;
	}
}