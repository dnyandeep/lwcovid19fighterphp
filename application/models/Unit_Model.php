<?php
class Unit_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_base_units() { 
		return $this->db->get_where( 'units', array( 'base_unit' => NULL ) )->result_array();
	}

	function add_unit( $params ) {
		$this->db->insert( 'units', $params );
		$unit_id = $this->db->insert_id();
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $this->session->usr_id . '"> ' . $this->session->staffname . '</a> ' . lang( 'addedaunit' ) . ' <a href="units/unit/' . $unit_id . '">' . '</a>' ),
			'staff_id' => $this->session->usr_id
		) );		
		return $unit_id;
	}

	function isDuplicate( $name , $id=NULL ) {
		if($id)
		{
			$this->db->get_where( 'units', array( 'name' => $name ,'id !=' => $id ), 1 );
		}
		else
		{
			$this->db->get_where( 'manufacturer', array( 'name' => $name ), 1 );
		}
		$manufacturer = $this->db->affected_rows();
		if($manufacturer > 0) {
			$result = true;
		} else {
			$result = false;
		}
		return $result;
	}

	function check_data_allready_exist( $params ,$id=NULL)
	{
		if($id)
		{
			$this->db->select('*')	
				->from('units')
				->where($params)
				->where('id !=',$id);
			$query = $this->db->get();	
		}
		else
		{
			$query = $this->db->get_where('units', $params);
		}		
        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
        	return false;
        }
	}

	function get_all_units()
	{
		$this->db->select("units.id as id, units.name, units.base_unit, b.name as basename", FALSE)
            ->from("units")
            ->join("units b", 'b.id=units.base_unit', 'left')
            ->group_by('units.id');
            $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
	}

	function get_unit_by_privileges( $id, $staff_id='' ) { 
		return $this->db->get_where( 'units', array( 'id' => $id ) )->row_array();		
	}

	function delete_unit( $id ,$name) {
		
			$response = $this->db->delete( 'units', array( 'id' => $id ) );
			$loggedinuserid = $this->session->usr_id;
			$this->db->insert( 'logs', array(
				'date' => date( 'Y-m-d H:i:s' ),
				'detail' => ( '<a href="staff/staffmember/' . $loggedinuserid . '"> ' . $this->session->staffname . '</a> ' . lang( 'deleted' ) . ' '. $name . lang( 'unit' ) .'' ),
				'staff_id' => $this->session->usr_id
			) );
			return true;
		
	}

	function get_units() { 
		return $this->db->get_where( 'units', array( '' ) )->result_array();
	}
}
