<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Countries_Model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to get all active states

	function get_countries() { 
		return $this->db->get( 'countries' )->result_array();
	}

	
}