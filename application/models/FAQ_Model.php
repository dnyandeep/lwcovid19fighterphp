<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class FAQ_Model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to get all active faqs

	function get_all_faqs() { 
		return $this->db->get_where( 'faq', array( 'status' => 'Active' ) )->result_array();
	}
}