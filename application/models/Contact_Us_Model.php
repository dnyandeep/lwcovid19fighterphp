<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Contact_Us_Model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to add record.
	function add( $params ) 
	{
		if($this->db->insert( 'contact_us', $params ))
		{
			return true;
		}
		else
		{
			return false;
		}	
		
	}
}