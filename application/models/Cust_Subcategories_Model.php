<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Cust_Subcategories_Model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to get all active subcategories.
	function get_customer_subcategories( ) 
	{
		$this->db->order_by( 'id', 'acs' );
		return $this->db->get_where( 'customer_subcategories', array( 'status'=>'Active' ) )->result_array();		
	}

	function get_customer_subcategories_by_categoryid($category_id ) 
	{
		return $this->db->get_where( 'customer_subcategories', array( 'status'=>'Active', 'cust_cat_id' =>  $category_id) )->result_array();		
	}

	function get_cust_subcategory_name_by_id($id)
	{
		return $this->db->get_where( 'customer_subcategories', array( 'id' => $id ))->row()->name;
	}
}