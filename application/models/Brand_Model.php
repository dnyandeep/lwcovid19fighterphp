<?php
class Brand_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_base_units() { 
		return $this->db->get_where( 'units', array( 'base_unit' => NULL ) )->result_array();
	}

	function add_brand( $params ) {
		$this->db->insert( 'brands', $params );
		$brand_id = $this->db->insert_id();
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $this->session->usr_id . '"> ' . $this->session->staffname . '</a> ' . lang( 'addeda' ) . ' ' . lang( 'addeda' ). ' <a href="brands/brand/' . $brand_id . '">' . '</a>' ),
			'staff_id' => $this->session->usr_id
		) );		
		return $brand_id;
	}

	function isDuplicate( $name , $id=NULL ) {
		if($id)
		{
			$this->db->get_where( 'units', array( 'name' => $name ,'id !=' => $id ), 1 );
		}
		else
		{
			$this->db->get_where( 'manufacturer', array( 'name' => $name ), 1 );
		}
		$manufacturer = $this->db->affected_rows();
		if($manufacturer > 0) {
			$result = true;
		} else {
			$result = false;
		}
		return $result;
	}

	function check_data_allready_exist( $params ,$id=NULL)
	{
		if($id)
		{
			$this->db->select('*')	
				->from('brands')
				->where($params)
				->where('id !=',$id);
			$query = $this->db->get();	
		}
		else
		{
			$query = $this->db->get_where('brands', $params);
		}		
        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
        	return false;
        }
	}

	function get_all_brands()
	{
		$this->db->select("brands.id as id, brands.name, brands.manufacturer_id, b.name as manufacturer", FALSE)
            ->from("brands")
            ->join("manufacturer b", 'b.id=brands.manufacturer_id');
            $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
	}

	function get_brand_by_privileges( $id, $staff_id='' ) { 
		return $this->db->get_where( 'brands', array( 'id' => $id ) )->row_array();		
	}

	function delete_brand( $id ,$name) {
		
			$response = $this->db->delete( 'brands', array( 'id' => $id ) );
			$loggedinuserid = $this->session->usr_id;
			$this->db->insert( 'logs', array(
				'date' => date( 'Y-m-d H:i:s' ),
				'detail' => ( '<a href="staff/staffmember/' . $loggedinuserid . '"> ' . $this->session->staffname . '</a> ' . lang( 'deleted' ) . ' '. $name . lang( 'unit' ) .'' ),
				'staff_id' => $this->session->usr_id
			) );
			return true;
		
	}
}
