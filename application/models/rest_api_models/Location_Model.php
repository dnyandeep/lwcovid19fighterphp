<?php
class Location_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function addSalesmanCurrentLocation($data, $salesman_id)
	{
		$this->db->select('*')
            ->from('salesman_current_location')
            ->where(array('salesman_id' => $salesman_id ));

        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            $this->db->where(array('salesman_id'=>$salesman_id));
            $this->db->update('salesman_current_location',$data);
            return true;

        }
        else
        {
            $this->db->insert('salesman_current_location', $data);
            $insert_id = $this->db->insert_id();
            return true;
        }
		return false;
	}

	function checkRoutePresentOrNot($data)
    {
        $query = $this->db->get_where('salesman_locations', $data);
        if ($query->num_rows() > 0) 
        {
            return true;
        }
        else
        {
            return FALSE;
        }
    }

    function checkDistancePresentForSameDate($salesman,$end_date,$distance)
    {
        $query = $this->db->get_where('salesman_locations',array('salesman_id' => $salesman,'end_date' => $end_date,'distance' => $distance));
        if ($query->num_rows() > 0) 
        {
            return true;
        }
        else
        {
            return FALSE;
        }
    }

    function add_salesman_route($data)
    {
    	$this->db->insert('salesman_locations', $data);
        $insert_id = $this->db->insert_id();

        if( $insert_id )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
