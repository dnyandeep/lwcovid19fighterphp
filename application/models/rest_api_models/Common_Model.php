<?php
class Common_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function getUserDetails($email, $password)
	{
		$password = md5($password);
		return $this->db->get_where( 'staff', array( 'email' => $email , 'password' => $password ) )->row();
	}

	function updatePassword( $post ) 
	{
		$this->db->where( 'id', $post[ 'user_id' ] );
		$this->db->update( 'staff', array( 'password' => $post[ 'password' ] ) );
		$success = $this->db->affected_rows();

		if ( !$success ) {
			return false;
		}
		return true;
	}

	function getUserDetailsByEmail($email)
	{
		$q = $this->db->get_where( 'staff', array( 'email' => $email ), 1 );
		if ( $this->db->affected_rows() > 0 ) {
			$row = $q->result_array();
			return $row;
		} else {
			return false;
		}
	}
}
