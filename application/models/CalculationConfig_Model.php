<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class CalculationConfig_Model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to get latest active configuration

	function getLatestCalConfiguration() 
	{ 
		$this->db->select('*');
        $this->db->from('solar_cal_config');
        $this->db->where('solar_cal_config.status','Active');
        $this->db->order_by('updated_date', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data = $row;
            }
            return $data;
        }
        return FALSE;


		//return $this->db->get_where( 'faq', array( 'status' => 'Active' ) )->result_array();
	}
}