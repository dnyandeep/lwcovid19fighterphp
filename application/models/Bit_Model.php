<?php
class Bit_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function add_area( $params ) {
		$this->db->insert( 'bit', $params );
		$area_id = $this->db->insert_id();
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $this->session->usr_id . '"> ' . $this->session->staffname . '</a> ' . lang( 'addedaarea' ) . ' <a href="area/area/' . $area_id . '">' . '</a>' ),
			'staff_id' => $this->session->usr_id
		) );		
		return $area_id;
	}

	function check_data_allready_exist( $params )
	{
		$query = $this->db->get_where('bit', $params);
        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
        	return false;
        }
	}

	function get_all_areas()
	{
		return $this->db->get_where( 'bit', array( '' ) )->result_array();
	}

	function get_areas( $id ) {
		return $this->db->get_where( 'bit', array( 'id' => $id ) )->row_array();
	}

	function get_area_by_privileges( $id, $staff_id='' ) { 
		return $this->db->get_where( 'bit', array( 'id' => $id ) )->row_array();		
	}

	function update_area( $id, $params ) {
		$appconfig = get_appconfig();		
		$this->db->where( 'id', $id );
		$response = $this->db->update( 'bit', $params );
		$loggedinuserid = $this->session->usr_id;
		$staffname = $this->session->staffname;
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="'.base_url().'staff/staffmember/' . $loggedinuserid . '"> ' . $staffname . '</a> ' . lang( 'updated' ) . ' <a href="'.base_url().'area/area/' . $id . '">'. '</a>.' ),
			'staff_id' => $loggedinuserid,
		) );
	}

	function delete_area( $id ,$bit) {
		
			$response = $this->db->delete( 'bit', array( 'id' => $id ) );
			$loggedinuserid = $this->session->usr_id;
			$this->db->insert( 'logs', array(
				'date' => date( 'Y-m-d H:i:s' ),
				'detail' => ( '<a href="staff/staffmember/' . $loggedinuserid . '"> ' . $this->session->staffname . '</a> ' . lang( 'deleted' ) . ' '. $bit . '' ),
				'staff_id' => $this->session->usr_id
			) );
			return true;
		
	}
}
