<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class TableVersions_model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to get all active faqs

	function getAllTableVersions($tablelist) 
	{ 
		$this->db->select('*');
        $this->db->from('table_versions');
        $this->db->where_in('table_name',$tablelist);
        $query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) 
            {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
	}
}