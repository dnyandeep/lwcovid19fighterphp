<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class KWSolarRateCard_Model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to get all active faqs

	function get_all_KWSolarRateCard() { 
		return $this->db->get_where( 'kw_solar_rate_card', array( '' ) )->result_array();
	}
}