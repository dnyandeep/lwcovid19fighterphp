<?php
class EmployeeModel extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	
function getEmployees() { 
		$this->db->order_by( 'id', 'asc' );
		return $this->db->get_where( 'employee', array( '' ) )->result_array();
	}

	function addEmployee( $params ) {
		//echo " ID ". $params['id'];
		$adv_id =0;
		if( $params['id'])
		{
			if( $params['id'] > 0)
			{
				
				$this->db->where('id', $params['id']);
				unset($params['id']);
				$this->db->update( 'employee', $params );
				$adv_id=1;
			}
			
		}
		else
			{
				$this->db->insert( 'employee', $params );
				$adv_id = $this->db->insert_id();

			}
		
		
		return $adv_id;
	}



	function delete_adv_material( $id ,$name) 
	{
		$response = $this->db->delete( 'free_sample_products', array( 'id' => $id ) );
		$loggedinuserid = $this->session->usr_id;
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $loggedinuserid . '"> ' . $this->session->staffname . '</a> ' . lang( 'deleted' ) . ' '. $name . lang( 'adv_material' ) .'' ),
				'staff_id' => $this->session->usr_id
		) );
		return true;		
	}

	function get_all_distributions() 
	{
		$this->db->select("stock_distribution.id as id, stock_distribution.date, b.staffname, a.name,stock_distribution.quantity,c.company", FALSE)
            ->from("stock_distribution")
            ->join("free_sample_products a",'a.id=stock_distribution.material_id')
            ->join("staff b", 'b.id=stock_distribution.salesman_id')
            ->join("customers c",'c.id=stock_distribution.customer_id');
            $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
	}

	function get_all_adv_stocks() 
	{
		$this->db->select("stock.id as id, stock.date, stock.salesman_id, stock.product_id, b.staffname, a.name,stock.quantity", FALSE)
            ->from("stock")
            ->join("free_sample_products a",'a.id=stock.product_id')
            ->join("staff b", 'b.id=stock.salesman_id');
            $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
	}

	function getAllAdvertisingProduct() { 
		$this->db->order_by( 'id', 'asc' );
		return $this->db->get_where( 'free_sample_products', array( '' ) )->result_array();
	}

	function check_stock_allready_exist( $params ,$id=NULL)
	{
		if($id)
		{
			$this->db->select('*')	
				->from('stock')
				->where($params)
				->where('id !=',$id);
			$query = $this->db->get();	
		}	
        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
        	return false;
        }
	}

	function get_stock_by_privileges( $id, $staff_id='' ) { 
		return $this->db->get_where( 'stock', array( 'id' => $id ) )->row_array();		
	}

	function delete_stock( $id ) 
	{
		$response = $this->db->delete( 'stock', array( 'id' => $id ) );
		$loggedinuserid = $this->session->usr_id;
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $loggedinuserid . '"> ' . $this->session->staffname . '</a> ' . lang( 'deleted' ) . ' ' . lang( 'stock' ) .'' ),
				'staff_id' => $this->session->usr_id
		) );
		return true;		
	}

	function deleteStockByDateAndSalesmanid($date , $salesman_id)
	{
		$this->db->delete('stock',array('date'=>$date,'salesman_id'=>$salesman_id));
		return true;
	}

	function add_adv_stock( $params , $length)
	{
		for($i = 0 ; $i < $length ; $i++)
		{
			$this->db->insert('stock',$params[$i]);
		}
		return true;
	}

	function addDiastributionMaterial($items = array(), $salesman_id)
	{
		foreach ($items as $item) 
		{
            $this->db->insert('stock_distribution', $item);
            $this->db->insert( 'logs', array(
				'date' => date( 'Y-m-d H:i:s' ),
				'detail' => ( '<a href="staff/staffmember/' . $salesman_id . '"> </a> ' . lang( 'addedadistributionmaterial' ) ),
				'staff_id' => $salesman_id
			) );
        }
	}

    function getAllMaterials($salesman_id)
    {
    	$this->db->select("stock.id as st_id, stock.salesman_id, stock.product_id, material.name, material.category, SUM(stock.quantity) AS qty", FALSE)
            ->from("stock")
            ->join("free_sample_products material",'material.id=stock.product_id')
            ->where("stock.salesman_id", $salesman_id)
            ->group_by("stock.product_id");

        $result = $this->db->get();   
        if ($result->num_rows() > 0) {
            foreach (($result->result()) as $row) 
            {
                $data[] = $row;
            }
            return $data;
        }else{
            return false;
        }

    }
}
