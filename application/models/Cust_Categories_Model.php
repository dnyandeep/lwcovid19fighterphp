<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class Cust_Categories_Model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to get all active categories.
	function get_customer_categories( ) 
	{
		$this->db->order_by( 'id', 'acs' );
		return $this->db->get_where( 'customer_categories', array( 'status'=>'Active' ) )->result_array();		
	}

	function get_category_name_by_id($id)
	{
		return $this->db->get_where( 'customer_categories', array( 'id' => $id ))->row()->name;
	}
}