<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class District_Model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to get all active districts

	function get_districts() { 
		return $this->db->get_where( 'districts', array( 'status' => 'Active' ) )->result_array();
	}

	function get_district_by_id($id)
	{
		$this->db->select('*');
		return $this->db->get_where( 'districts', array( 'id' => $id ) )->row_array();
	}

	function get_district_by_state_id($state_id)
	{
		$this->db->select('*');
		return $this->db->get_where( 'districts', array( 'state_id' => $state_id ) )->result_array();
	}

	function get_district_name_by_id($id)
	{
		return $this->db->get_where( 'districts', array( 'id' => $id ))->row()->name;
	}
}