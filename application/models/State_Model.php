<?php
defined( 'BASEPATH' )OR exit( 'No direct script access allowed' );
class State_Model extends CI_Model {
	public

	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	// This function is used to get all active states

	function get_states() { 
		return $this->db->get_where( 'states', array( 'status' => 'Active' ) )->result_array();
	}

	function get_state_by_id($id)
	{
		$this->db->select('*');
		return $this->db->get_where( 'states', array( 'id' => $id ) )->row_array();
	}

	function get_state_name_by_id($id)
	{
		return $this->db->get_where( 'states', array( 'id' => $id ))->row()->name;
	}
}