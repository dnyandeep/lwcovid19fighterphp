<?php
class Cities_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function add_city( $params ) {
		$this->db->insert( 'cities', $params );
		$city_id = $this->db->insert_id();
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $this->session->usr_id . '"> ' . $this->session->staffname . '</a> ' . lang( 'addeda' ) . ' ' . lang( 'city' ) . ' <a href="cities/city/' . $city_id . '">' . '</a>' ),
			'staff_id' => $this->session->usr_id
		) );		
		return $city_id;
	}

	function isDuplicate( $city , $id=NULL ) {
		if($id)
		{
			$this->db->get_where( 'cities', array( 'city' => $city ,'id !=' => $id ), 1 );
		}
		else
		{
			$this->db->get_where( 'cities', array( 'city' => $city ), 1 );
		}
		$cities = $this->db->affected_rows();
		if($cities > 0) {
			$result = true;
		} else {
			$result = false;
		}
		return $result;
	}

	function check_data_allready_exist( $params )
	{
		$query = $this->db->get_where('cities', $params);
        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
        	return false;
        }
	}

	function get_all_cities()
	{
		return $this->db->get_where( 'cities', array( '' ) )->result_array();
	}

	function get_city_by_privileges( $id, $staff_id='' ) { 
		return $this->db->get_where( 'cities', array( 'id' => $id ) )->row_array();		
	}

	function delete_city( $id ,$city) {
		
			$response = $this->db->delete( 'cities', array( 'id' => $id ) );
			$loggedinuserid = $this->session->usr_id;
			$this->db->insert( 'logs', array(
				'date' => date( 'Y-m-d H:i:s' ),
				'detail' => ( '<a href="staff/staffmember/' . $loggedinuserid . '"> ' . $this->session->staffname . '</a> ' . lang( 'deleted' ) . ' '. $city . lang( 'city' ) .'' ),
				'staff_id' => $this->session->usr_id
			) );
			return true;
		
	}

	function get_cities() { 
		return $this->db->get_where( 'cities', array( '' ) )->result_array();
	}
}
