<?php
class Marketing_Model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_base_units() { 
		return $this->db->get_where( 'units', array( 'base_unit' => NULL ) )->result_array();
	}

	function get_all_advertising_material() 
	{
		$this->db->select("free_sample_products.id as id, free_sample_products.name, unit_id,category,description,image,b.name as unit_name", FALSE)
            ->from("free_sample_products")
            ->join("units b", 'b.id=free_sample_products.unit_id')
            ->order_by("free_sample_products.id","desc");
            $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
	}

	function check_data_allready_exist( $params ,$id=NULL)
	{
		if($id)
		{
			$this->db->select('*')	
				->from('free_sample_products')
				->where($params)
				->where('id !=',$id);
			$query = $this->db->get();	
		}
		else
		{
			$query = $this->db->get_where('free_sample_products', $params);
		}		
        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
        	return false;
        }
	}

	function add_adv_material( $params ) {
		$this->db->insert( 'free_sample_products', $params );
		$adv_id = $this->db->insert_id();
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $this->session->usr_id . '"> ' . $this->session->staffname . '</a> ' . lang( 'addeda' ) . ' ' . lang( 'adv_material' ). ' <a href="marketing/adv_material/' . $adv_id . '">' . '</a>' ),
			'staff_id' => $this->session->usr_id
		) );		
		return $adv_id;
	}

	function get_material_by_privileges( $id, $staff_id='' ) { 
		return $this->db->get_where( 'free_sample_products', array( 'id' => $id ) )->row_array();		
	}

	function delete_adv_material( $id ,$name) 
	{
		$response = $this->db->delete( 'free_sample_products', array( 'id' => $id ) );
		$loggedinuserid = $this->session->usr_id;
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $loggedinuserid . '"> ' . $this->session->staffname . '</a> ' . lang( 'deleted' ) . ' '. $name . lang( 'adv_material' ) .'' ),
				'staff_id' => $this->session->usr_id
		) );
		return true;		
	}

	function get_all_distributions() 
	{
		$this->db->select("stock_distribution.id as id, stock_distribution.date, b.staffname, a.name,stock_distribution.quantity,c.company", FALSE)
            ->from("stock_distribution")
            ->join("free_sample_products a",'a.id=stock_distribution.material_id')
            ->join("staff b", 'b.id=stock_distribution.salesman_id')
            ->join("customers c",'c.id=stock_distribution.customer_id');
            $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
	}

	function get_all_adv_stocks() 
	{
		$this->db->select("stock.id as id, stock.date, stock.salesman_id, stock.product_id, b.staffname, a.name,stock.quantity", FALSE)
            ->from("stock")
            ->join("free_sample_products a",'a.id=stock.product_id')
            ->join("staff b", 'b.id=stock.salesman_id');
            $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
	}

	function getAllAdvertisingProduct() { 
		$this->db->order_by( 'id', 'asc' );
		return $this->db->get_where( 'free_sample_products', array( '' ) )->result_array();
	}

	function check_stock_allready_exist( $params ,$id=NULL)
	{
		if($id)
		{
			$this->db->select('*')	
				->from('stock')
				->where($params)
				->where('id !=',$id);
			$query = $this->db->get();	
		}	
        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
        	return false;
        }
	}

	function get_stock_by_privileges( $id, $staff_id='' ) { 
		return $this->db->get_where( 'stock', array( 'id' => $id ) )->row_array();		
	}

	function delete_stock( $id ) 
	{
		$response = $this->db->delete( 'stock', array( 'id' => $id ) );
		$loggedinuserid = $this->session->usr_id;
		$this->db->insert( 'logs', array(
			'date' => date( 'Y-m-d H:i:s' ),
			'detail' => ( '<a href="staff/staffmember/' . $loggedinuserid . '"> ' . $this->session->staffname . '</a> ' . lang( 'deleted' ) . ' ' . lang( 'stock' ) .'' ),
				'staff_id' => $this->session->usr_id
		) );
		return true;		
	}

	function deleteStockByDateAndSalesmanid($date , $salesman_id)
	{
		$this->db->delete('stock',array('date'=>$date,'salesman_id'=>$salesman_id));
		return true;
	}

	function add_adv_stock( $params , $length)
	{
		for($i = 0 ; $i < $length ; $i++)
		{
			$this->db->insert('stock',$params[$i]);
		}
		return true;
	}

	function addDiastributionMaterial($items = array(), $salesman_id)
	{
		foreach ($items as $item) 
		{
            $this->db->insert('stock_distribution', $item);
            $this->db->insert( 'logs', array(
				'date' => date( 'Y-m-d H:i:s' ),
				'detail' => ( '<a href="staff/staffmember/' . $salesman_id . '"> </a> ' . lang( 'addedadistributionmaterial' ) ),
				'staff_id' => $salesman_id
			) );
        }
	}

    function getAllMaterials($salesman_id)
    {
    	$this->db->select("stock.id as st_id, stock.salesman_id, stock.product_id, material.name, material.category, SUM(stock.quantity) AS qty", FALSE)
            ->from("stock")
            ->join("free_sample_products material",'material.id=stock.product_id')
            ->where("stock.salesman_id", $salesman_id)
            ->group_by("stock.product_id");

        $result = $this->db->get();   
        if ($result->num_rows() > 0) {
            foreach (($result->result()) as $row) 
            {
                $data[] = $row;
            }
            return $data;
        }else{
            return false;
        }

    }
}
