<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>

<div class="lw-body-content" ng-controller="Patient_Controller">
  <style type="text/css">
    rect.highcharts-background {
      fill: #f3f3f3;
    }

    .md-datepicker-input-container {
    border: none;
    width: 80%;
}

md-dialog{
  width: 600px;
}

  </style>
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <md-button class="md-icon-button" aria-label="File">
          <md-icon><i class="ion-document text-muted"></i></md-icon>
        </md-button>
        <h2 flex md-truncate><?php echo lang('patients'); ?><small>(<span ng-bind="persons.length"></span>)</small>
        </h2>
        <div class="lw-external-search-in-table">
          <input ng-model="search" class="search-table-external" id="search" name="search" type="text" placeholder="<?php echo lang('searchword') ?>">
          <md-button class="md-icon-button" aria-label="Search" ng-cloak>
            <md-icon><i class="ion-search text-muted"></i></md-icon>
          </md-button>
        </div>

        <md-button ng-click="Create()" class="md-icon-button" aria-label="New" ng-cloak>
          <md-tooltip md-direction="bottom"><?php echo lang('create') ?></md-tooltip>
          <md-icon><i class="ion-android-add-circle text-success"></i></md-icon>
        </md-button>

      </div>
    </md-toolbar>
    <div ng-show="personsLoader" layout-align="center center" class="text-center" id="circular_loader" ng-cloak>
      <md-progress-circular md-mode="indeterminate" md-diameter="30"></md-progress-circular>
      <p style="font-size: 15px;margin-bottom: 5%;">
        <span><?php echo lang('please_wait') ?> <br>
          <small><strong><?php echo lang('loading') . ' ' . lang('patients') . '...' ?></strong></small></span>
        </p>
      </div>
      <md-content ng-show="!personsLoader" class="bg-white" ng-cloak>
        <md-table-container ng-show="persons.length > 0">
          <table md-table md-progress="promise">
            <thead md-head md-order="persons_list.order">
              <tr md-row>
                <th md-column md-order-by="fname"><span><?php echo lang('First Name'); ?></span></th>
                <th md-column md-order-by="lname"><span><?php echo lang('Last Name'); ?></span></th>

                <th md-column md-order-by="mobile_no"><span><?php echo lang('Mobile No'); ?></span></th>

                 <th md-column md-order-by="email"><span><?php echo lang('Email'); ?></span></th>

                 <th md-column md-order-by="stateName"><span><?php echo lang('State'); ?></span></th>

                  <th md-column md-order-by="districtName"><span><?php echo lang('District'); ?></span></th>

                  <th md-column md-order-by="resiCountry"><span><?php echo lang('Country'); ?></span></th>

                <!--  <th md-column md-order-by="travelCountry"><span><?php echo lang('Last Travelled Country'); ?></span></th> -->

                <th md-column><span><?php echo lang('action'); ?></span></th>
              </tr>
            </thead>
            <tbody md-body>
              <tr class="select_row" md-row ng-repeat="person in persons | orderBy: person_list.order | limitTo: person_list.limit : (person_list.page -1) * person_list.limit | filter: person.search | filter: FilteredData" class="cursor" >
                <td md-cell>
                  <strong><span ng-bind="person.fname"></span></strong><br>
                </td>
                <td md-cell>
                  <strong><span ng-bind="person.lname"></span></strong><br>
                </td>

                <td md-cell>
                  <strong><span ng-bind="person.mobile_no"></span></strong><br>
                </td>

                <td md-cell>
                  <strong><span ng-bind="person.email"></span></strong><br>
                </td>
               

                   <td md-cell>
                  <strong><span ng-bind="person.stateName"></span></strong><br>
                </td>

                <td md-cell>
                  <strong><span ng-bind="person.districtName"></span></strong><br>
                </td>

                <td md-cell>
                  <strong><span ng-bind="person.resiCountry"></span></strong><br>
                </td>

              <!--  <td md-cell>
                  <strong><span ng-bind="person.travelCountry"></span></strong><br>
                </td> -->

                <td md-cell>


                  <md-icon ng-click="editPerson(person)" md-menu-align-target class="ion-compose"></md-icon>



                  <md-button ng-click="Delete(person.id)" class="md-icon-button md-primary" aria-label="Actions" ng-cloak>
                    <md-icon class="ion-trash-b"></md-icon>
                  </md-button>                    

                </td>
              </tr>
            </tbody>
          </table>
        </md-table-container>
        <md-table-pagination ng-show="persons.length > 0" md-limit="person_list.limit" md-limit-options="limitOptions" md-page="person_list.page" md-total="{{persons.length}}"></md-table-pagination>
        <md-content ng-show="!persons.length && !personsLoader" class="md-padding no-item-data">
          <?php echo lang('notdata') ?></md-content>
        </md-content>
      </div>

      <md-sidenav class="md-sidenav-right md-whiteframe-4dp" md-component-id="Create" style="width:550px;" ng-cloak>
        <md-toolbar class="toolbar-white">
          <div class="md-toolbar-tools">
            <md-button ng-click="close()" class="md-icon-button" aria-label="Close"> <i class="ion-android-arrow-forward"></i> </md-button>
            <h2 flex md-truncate><?php echo lang('create') ?></h2>
          </div>
        </md-toolbar>
       <form name="frm" novalidate="novalidate">
        <md-content>
          <md-content layout-padding>
            <md-input-container class="md-block">
              <label><?php echo lang('First Name'); ?></label>
              <input name="fname" ng-model="person.fname" required/>

              <span style = "color:red" ng-show = "(frm.lname.$dirty && frm.lname.$invalid) || (frm.lname.$invalid && submitted)">
                        <span ng-show = "frm.fname.$error.required">First Name is required.</span>
                     </span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Middle Name'); ?></label>
              <input name="mname" ng-model="person.mname" >
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Last Name'); ?></label>
              <input name="lname" ng-model="person.lname" required>
               <span style = "color:red" ng-show = "(frm.lname.$dirty && frm.lname.$invalid) || (frm.lname.$invalid && submitted)">
                        <span ng-show = "frm.lname.$error.required">Last Name is required.</span>
                     </span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Username'); ?></label>
              <input name="username" ng-model="person.username" required>
               <span style = "color:red" ng-show = "(frm.username.$dirty && frm.username.$invalid) || (frm.username.$invalid && submitted)">
                        <span ng-show = "frm.username.$error.required">User Name is required.</span>
                     </span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Password'); ?></label>
              <input name="password" ng-model="person.password" type="password" required>
               <span style = "color:red" ng-show = "(frm.password.$dirty && frm.password.$invalid) || (frm.password.$invalid && submitted)">
                        <span ng-show = "frm.password.$error.required">Password is required.</span>
                     </span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Mobile No'); ?></label>
              <input name="mobile_no" ng-model="person.mobile_no" type="number" ng-pattern="ph_numbr" required>

               <span style = "color:red" ng-show = "(frm.mobile_no.$dirty && frm.mobile_no.$invalid) || (frm.mobile_no.$invalid && submitted)">
                        <span ng-show = "frm.mobile_no.$error.required">Mobile No is required.</span>
                     </span>

                     <span ng-show="frm.mobile_no.$error.pattern" style="color:red">Please enter valid mobile no.</span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Alternate No'); ?></label>
              <input name="alternate_no" ng-model="person.alternate_no" type="number" ng-pattern="ph_numbr">

               <span ng-show="frm.alternate_no.$error.pattern" style="color:red">Please enter valid mobile no.</span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Whatsapp No'); ?></label>
              <input name="whatsapp_no" ng-model="person.whatsapp_no" type="number" ng-pattern="ph_numbr">
               <span ng-show="frm.whatsapp_no.$error.pattern" style="color:red">Please enter valid mobile no.</span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Email'); ?></label>
              <input name="email" ng-model="person.email" ng-pattern="reEmail" required>

               <span ng-show="(frm.email.$dirty && frm.email.$error.required) || (submitted && frm.email.$error.required)" style="color:red">Please enter email address.</span>
              <span ng-show="frm.email.$error.pattern" style="color:red">Please enter correct email address.</span>
            </md-input-container>

            <md-input-container  class="md-block">

             <md-select-header><?php echo lang('Gender'); ?></md-select-header>
             <md-select  ng-model="person.gender" name="gender"  required>
              <md-option ng-value="{{item}}" ng-repeat="item in genders">{{ item.name }}</md-option>
            </md-select>

            <span ng-show="(frm.gender.$dirty && frm.gender.$error.required) || (submitted && frm.gender.$error.required)" style="color:red">Please select gender.</span>
          </md-input-container>

          <md-input-container  class="md-block">
            <label><?php echo lang('Date Of Birth'); ?></label>
           <md-datepicker name="dob" ng-model="person.dob"  md-max-date="maxDate" required></md-datepicker> 

            <!--  <input type="text" name="dob" ng-model="person.dob"  my-datepicker required/>-->

          

           <span ng-show="(frm.dob.$dirty && frm.dob.$error.required) || (submitted && frm.dob.$error.required)" style="color:red">Please enter DOB.</span>

             <span ng-show="(frm.dob.$dirty && !frm.dob.$error.required && frm.dob.$invalid) || (submitted && !frm.dob.$error.required && frm.dob.$invalid)" style="color:red">Please enter DOB in proper format.</span>
          </md-input-container>


          

          <md-input-container class="md-block">
            <label><?php echo lang('Aadhar No'); ?></label>
            <input name="aadhar_no" ng-model="person.aadhar_no" type="number" ng-pattern="aadharno" required>

              <span ng-show="(frm.aadhar_no.$dirty && frm.aadhar_no.$error.required) || (submitted && frm.aadhar_no.$error.required)" style="color:red">Please enter aadhar no.</span>
              <span ng-show="frm.aadhar_no.$error.pattern" style="color:red">Please enter correct aadhar no.</span>

          </md-input-container>


          <md-input-container class="md-block">
            <label><?php echo lang('Nationality'); ?></label>
            <input name="nationality" ng-model="person.nationality" required>
             <span ng-show="(frm.nationality.$dirty && frm.nationality.$error.required) || (submitted && frm.nationality.$error.required)" style="color:red">Please enter nationality.</span>

          </md-input-container>

          <md-input-container  class="md-block">
          <md-select-header>
      <span><?php echo lang('Citizen Type'); ?></span>
    </md-select-header>
           <md-select  ng-model="person.citizen_type" name="citizen_type"  >
            <md-option ng-value="{{item}}" ng-repeat="item in citizen_types">{{ item.name }}</md-option>
          </md-select>
           <span ng-show="(frm.citizen_type.$dirty && frm.citizen_type.$error.required) || (submitted && frm.citizen_type.$error.required)" style="color:red">Please select citizen type.</span>

        </md-input-container>

        <md-input-container  class="md-block">
          <md-select-header>
      <span><?php echo lang('State'); ?></span>
    </md-select-header>
         <md-select  ng-model="person.state" name="state"  ng-change="stateChange(person.state)" required>
          <md-option ng-value="{{item}}" ng-repeat="item in states">{{ item.name }}</md-option>
        </md-select>
         <span ng-show="(frm.state.$dirty && frm.state.$error.required) || (frm.state.$error.required && submitted)" style="color:red">Please select state.</span>

      </md-input-container>

      <md-input-container  class="md-block">
     <md-select-header>
      <span><?php echo lang('District'); ?></span>
    </md-select-header>
       <md-select  ng-model="person.district_id" name="district_id" >
        <md-option ng-value="{{item}}" ng-repeat="item in districts">{{ item.name }}</md-option>
      </md-select>
       <span ng-show="(frm.district_id.$dirty && frm.district_id.$error.required) || (frm.district_id.$error.required && submitted)" style="color:red">Please select district.</span>
    </md-input-container>


    <md-input-container class="md-block">
      <label><?php echo lang('Tahsil'); ?></label>
      <input name="person.tahsil_name" ng-model="person.tahsil_name" name="tahsil_name">
    </md-input-container>

    <md-input-container  class="md-block">
          <md-select-header>
      <span><?php echo lang('Country'); ?></span>
    </md-select-header>
     <md-select  ng-model="person.res_country_id" name="res_country_id" required>
      <md-option ng-value="{{item}}" ng-repeat="item in countries">{{ item.countries_name }}</md-option>
    </md-select>
    <span ng-show="(frm.res_country_id.$dirty && frm.res_country_id.$error.required) || (frm.res_country_id.$error.required && submitted)" style="color:red">Please select country.</span>
  </md-input-container>

  <md-input-container class="md-block">
    <label><?php echo lang('Travel History'); ?></label>
    <textarea name="travel_history" ng-model="travel_history" md-maxlength="150" ></textarea> 
  </md-input-container>

  <md-input-container  class="md-block">
    <md-select-header>
      <span><?php echo lang('Last Travel Country'); ?></span>
    </md-select-header>
   <md-select  ng-model="person.last_travel_country_id" name="last_travel_country_id" >
    <md-option ng-value="{{item}}" ng-repeat="item in countries">{{ item.countries_name }}</md-option>
  </md-select>
</md-input-container>


<md-input-container  class="md-block">
  <label"><?php echo lang('Date Of Arrival India'); ?></label>
  <md-datepicker md-max-date="maxDate"  ng-model="person.date_of_arrival_india" name="date_of_arrival_india" readonly></md-datepicker>



             <span ng-show="(frm.date_of_arrival_india.$dirty && !frm.date_of_arrival_india.$error.required && frm.date_of_arrival_india.$invalid) || (submitted && !frm.date_of_arrival_india.$error.required && frm.date_of_arrival_india.$invalid)" style="color:red">Please enter date in proper format.</span>
</md-input-container>


<md-input-container class="md-block">
  <label><?php echo lang('Passport No'); ?></label>
  <input name="passport_no" ng-model="person.passport_no">
</md-input-container>


<md-input-container  class="md-block">
   <md-select-header>
      <span><?php echo lang('Covid19 confirmed'); ?></span>
    </md-select-header>
   <md-select  ng-model="person.is_covid19_confirmed" name="is_covid19_confirmed" required>
    <md-option ng-value="{{item}}" ng-repeat="item in is_covid19_confirmeds">{{ item.name }}</md-option>
  </md-select>
   <span ng-show="(frm.is_covid19_confirmed.$dirty && frm.is_covid19_confirmed.$error.required) || (( frm.is_covid19_confirmed.$error.required) && submitted)" style="color:red">Please select covid19 confirmed / not.</span>

</md-input-container>

<md-input-container  class="md-block">
  
   <md-select-header>
      <span><?php echo lang('Is Suspect / Patient'); ?></span>
    </md-select-header>
   <md-select  ng-model="person.is_suspect_patient" name="is_suspect_patient" required>
      <md-option ng-value="{{item}}" ng-repeat="item in is_suspect_patients">{{ item.name }}</md-option>
  </md-select>

  <span ng-show="(frm.is_suspect_patient.$dirty && frm.is_suspect_patient.$error.required) || ( ( frm.is_suspect_patient.$error.required) &&  submitted)" style="color:red">Please select is suspect patient.</span>
</md-input-container>


 <md-input-container class="md-block">
            <label><?php echo lang('Pincode'); ?></label>
            <input name="pincode" ng-model="person.pincode" type="number" ng-pattern="pincode" required>

              <span ng-show="(frm.pincode.$dirty && frm.pincode.$error.required) || (submitted && frm.pincode.$error.required)" style="color:red">Please enter pincode.</span>
              <span ng-show="frm.pincode.$error.pattern" style="color:red">Please enter correct pincode.</span>

          </md-input-container>

 <md-input-container class="md-block">
    <label><?php echo lang('Address 1'); ?></label>
    <textarea name="address1" ng-model="person.address1" md-maxlength="150" ></textarea> 
  </md-input-container>

  <md-input-container class="md-block">
    <label><?php echo lang('Address 2'); ?></label>
    <textarea name="address2" ng-model="person.address2" md-maxlength="150" ></textarea> 
  </md-input-container>


</md-content>







<md-content class="layout-padding">
</md-content>
<md-content layout-padding>
  <section layout="row" layout-sm="column" layout-align="center center" layout-wrap>
    <md-button   ng-click="setsubmitted() && frm.$valid  && AddPerson()" class="md-raised md-primary btn-report block-button">
      <span ng-hide="saving == true"><?php echo lang('create'); ?></span>
      <md-progress-circular class="white" ng-show="saving == true" md-mode="indeterminate" md-diameter="20">
      </md-progress-circular>
    </md-button>
    <br/><br/><br/><br/>
  </section>
</md-content>

</md-content>
</form>

</md-sidenav>
</div>

<script type="text/ng-template" id="update_person.html">
  <md-dialog aria-label="Patient">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <h2><strong class="text-success"><?php echo lang('update').' '.lang('patient');?></strong></h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="close()">
          <md-icon class="ion-close-round" aria-label="Close dialog" style="color:black"></md-icon>
        </md-button>
      </div>
    </md-toolbar>
    <md-dialog-content style="max-width:900px;max-height:810px; ">
     
      <md-content class="bg-white">
       
            
               <form name="updateFrm" novalidate="novalidate">
        <md-content>
          <md-content layout-padding>
            <md-input-container class="md-block">
              <label><?php echo lang('First Name'); ?></label>
              <input name="fname" type="text" ng-model="person.fname" required/>

              <span style = "color:red" ng-show = "(updateFrm.fname.$dirty && updateFrm.fname.$invalid) || (updateFrm.fname.$invalid && submitted)">
                        <span ng-show = "updateFrm.fname.$error.required">First Name is required.</span>
                     </span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Middle Name'); ?></label>
              <input name="mname" ng-model="person.mname" >
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Last Name'); ?></label>
              <input name="lname" ng-model="person.lname" required>
               <span style = "color:red" ng-show = "(updateFrm.mname.$dirty && updateFrm.mname.$invalid) || (updateFrm.mname.$invalid && submitted)">
                        <span ng-show = "updateFrm.mname.$error.required">Last Name is required.</span>
                     </span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Username'); ?></label>
              <input name="username" ng-model="person.username" required>
               <span style = "color:red" ng-show = "(updateFrm.username.$dirty && updateFrm.username.$invalid) || (updateFrm.username.$invalid && submitted)">
                        <span ng-show = "updateFrm.username.$error.required">User Name is required.</span>
                     </span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Password'); ?></label>
              <input name="password" ng-model="person.password" type="password" required>
               <span style = "color:red" ng-show = "(updateFrm.password.$dirty && updateFrm.password.$invalid) || (updateFrm.password.$invalid && submitted)">
                        <span ng-show = "updateFrm.password.$error.required">Password is required.</span>
                     </span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Mobile No'); ?></label>
              <input name="mobile_no" string-to-number ng-model="person.mobile_no" type="text" ng-pattern="ph_numbr" required>

               <span style = "color:red" ng-show = "(updateFrm.mobile_no.$dirty && updateFrm.mobile_no.$invalid) || (updateFrm.mobile_no.$invalid && submitted)">
                        <span ng-show = "updateFrm.mobile_no.$error.required">Mobile No is required.</span>
                     </span>

                     <span ng-show="updateFrm.mobile_no.$error.pattern" style="color:red">Please enter valid mobile no.</span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Alternate No'); ?></label>
              <input name="alternate_no" string-to-number ng-model="person.alternate_no" type="text" ng-pattern="ph_numbr">

               <span ng-show="updateFrm.alternate_no.$error.pattern" style="color:red">Please enter valid mobile no.</span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Whatsapp No'); ?></label>
              <input name="whatsapp_no" string-to-number ng-model="person.whatsapp_no" type="text" ng-pattern="ph_numbr">
               <span ng-show="updateFrm.whatsapp_no.$error.pattern" style="color:red">Please enter valid mobile no.</span>
            </md-input-container>

            <md-input-container class="md-block">
              <label><?php echo lang('Email'); ?></label>
              <input name="email" ng-model="person.email" ng-pattern="reEmail" required>

               <span ng-show="(updateFrm.email.$dirty && updateFrm.email.$error.required) || (submitted && updateFrm.email.$error.required)" style="color:red">Please enter email address.</span>
              <span ng-show="updateFrm.email.$error.pattern" style="color:red">Please enter correct email address.</span>
            </md-input-container>

            <md-input-container  class="md-block">

             <md-select-header><?php echo lang('Gender'); ?></md-select-header>
             <md-select    ng-model="person.gender" name="gender"  required>
              <md-option  ng-value="item.id" ng-repeat="item in genders">{{ item.name }}</md-option>
            </md-select>

            <span ng-show="(updateFrm.gender.$dirty && updateFrm.gender.$error.required) || (submitted && updateFrm.gender.$error.required)" style="color:red">Please select gender.</span>
          </md-input-container>

          <md-input-container  class="md-block">
            <label><?php echo lang('Date Of Birth'); ?></label>
           <md-datepicker name="dob" ng-model="person.dob"  md-max-date="maxDate" required></md-datepicker> 

            <!--  <input type="text" name="dob" ng-model="person.dob"  my-datepicker required/>-->

          

           <span ng-show="(updateFrm.dob.$dirty && updateFrm.dob.$error.required) || (submitted && updateFrm.dob.$error.required)" style="color:red">Please enter DOB.</span>

             <span ng-show="(updateFrm.dob.$dirty && !updateFrm.dob.$error.required && updateFrm.dob.$invalid) || (submitted && !updateFrm.dob.$error.required && updateFrm.dob.$invalid)" style="color:red">Please enter DOB in proper format.</span>
          </md-input-container>


          

          <md-input-container class="md-block">
            <label><?php echo lang('Aadhar No'); ?></label>
            <input name="aadhar_no" ng-model="person.aadhar_no" type="text" ng-pattern="aadharno" required>

              <span ng-show="(updateFrm.aadhar_no.$dirty && updateFrm.aadhar_no.$error.required) || (submitted && updateFrm.aadhar_no.$error.required)" style="color:red">Please enter aadhar no.</span>
              <span ng-show="updateFrm.aadhar_no.$error.pattern" style="color:red">Please enter correct aadhar no.</span>

          </md-input-container>


          <md-input-container class="md-block">
            <label><?php echo lang('Nationality'); ?></label>
            <input name="nationality" ng-model="person.nationality" required>
             <span ng-show="(updateFrm.nationality.$dirty && updateFrm.nationality.$error.required) || (submitted && updateFrm.nationality.$error.required)" style="color:red">Please enter nationality.</span>

          </md-input-container>

          <md-input-container  class="md-block">
          <md-select-header>
      <span><?php echo lang('Citizen Type'); ?></span>
    </md-select-header>
           <md-select  ng-model="person.citizen_type" name="citizen_type"  >
            <md-option ng-value="item.id" ng-repeat="item in citizen_types">{{ item.name }}</md-option>
          </md-select>
           <span ng-show="(updateFrm.citizen_type.$dirty && updateFrm.citizen_type.$error.required) || (submitted && updateFrm.citizen_type.$error.required)" style="color:red">Please select citizen type.</span>

        </md-input-container>

        <md-input-container  class="md-block">
          <md-select-header>
      <span><?php echo lang('State'); ?></span>
    </md-select-header>
         <md-select  ng-model="person.state" name="state"  ng-change="stateChange(person.state)" required>
          <md-option ng-value="item.id" ng-repeat="item in states">{{ item.name }}</md-option>
        </md-select>
         <span ng-show="(updateFrm.state.$dirty && updateFrm.state.$error.required) || (updateFrm.state.$error.required && submitted)" style="color:red">Please select state.</span>

      </md-input-container>

      <md-input-container  class="md-block">
     <md-select-header>
      <span><?php echo lang('District'); ?></span>
    </md-select-header>
       <md-select  ng-model="person.district_id" name="district_id" >
        <md-option ng-value="item.id" ng-repeat="item in districts">{{ item.name }}</md-option>
      </md-select>
       <span ng-show="(updateFrm.district_id.$dirty && updateFrm.district_id.$error.required) || (updateFrm.district_id.$error.required && submitted)" style="color:red">Please select district.</span>
    </md-input-container>


    <md-input-container class="md-block">
      <label><?php echo lang('Tahsil'); ?></label>
      <input name="person.tahsil_name" ng-model="person.tahsil_name" name="tahsil_name">
    </md-input-container>

    <md-input-container  class="md-block">
          <md-select-header>
      <span><?php echo lang('Country'); ?></span>
    </md-select-header>
     <md-select  ng-model="person.res_country_id" name="res_country_id" required>
      <md-option ng-value="item.countries_id" ng-repeat="item in countries">{{ item.countries_name }}</md-option>
    </md-select>
    <span ng-show="(updateFrm.res_country_id.$dirty && updateFrm.res_country_id.$error.required) || (updateFrm.res_country_id.$error.required && submitted)" style="color:red">Please select country.</span>
  </md-input-container>

  <md-input-container class="md-block">
    <label><?php echo lang('Travel History'); ?></label>
    <textarea name="travel_history" ng-model="travel_history" md-maxlength="150" ></textarea> 
  </md-input-container>

  <md-input-container  class="md-block">
    <md-select-header>
      <span><?php echo lang('Last Travel Country'); ?></span>
    </md-select-header>
   <md-select  ng-model="person.last_travel_country_id" name="last_travel_country_id" >
    <md-option ng-value="item.countries_id" ng-repeat="item in countries">{{ item.countries_name }}</md-option>
  </md-select>
</md-input-container>


<md-input-container  class="md-block">
  <label"><?php echo lang('Date Of Arrival India'); ?></label>
  <md-datepicker md-max-date="maxDate"  ng-model="person.date_of_arrival_india" name="date_of_arrival_india" readonly></md-datepicker>



             <span ng-show="(updateFrm.date_of_arrival_india.$dirty && !updateFrm.date_of_arrival_india.$error.required && updateFrm.date_of_arrival_india.$invalid) || (submitted && !updateFrm.date_of_arrival_india.$error.required && updateFrm.date_of_arrival_india.$invalid)" style="color:red">Please enter date in proper format.</span>
</md-input-container>


<md-input-container class="md-block">
  <label><?php echo lang('Passport No'); ?></label>
  <input name="passport_no" ng-model="person.passport_no">
</md-input-container>


<md-input-container  class="md-block">
   <md-select-header>
      <span><?php echo lang('Covid19 confirmed'); ?></span>
    </md-select-header>
   <md-select  ng-model="person.is_covid19_confirmed" name="is_covid19_confirmed" required>
    <md-option ng-value="item.id" ng-repeat="item in is_covid19_confirmeds">{{ item.name }}</md-option>
  </md-select>
   <span ng-show="(updateFrm.is_covid19_confirmed.$dirty && updateFrm.is_covid19_confirmed.$error.required) || (( updateFrm.is_covid19_confirmed.$error.required) && submitted)" style="color:red">Please select covid19 confirmed / not.</span>

</md-input-container>

<md-input-container  class="md-block">
  
   <md-select-header>
      <span><?php echo lang('Is Suspect / Patient'); ?></span>
    </md-select-header>
   <md-select  ng-model="person.is_suspect_patient" name="is_suspect_patient" required>
      <md-option ng-value="item.id" ng-repeat="item in is_suspect_patients">{{ item.name }}</md-option>
  </md-select>

  <span ng-show="(updateFrm.is_suspect_patient.$dirty && updateFrm.is_suspect_patient.$error.required) || ( ( updateFrm.is_suspect_patient.$error.required) &&  submitted)" style="color:red">Please select is suspect patient.</span>
</md-input-container>


 <md-input-container class="md-block">
            <label><?php echo lang('Pincode'); ?></label>
            <input name="pincode" ng-model="person.pincode" type="text" ng-pattern="pincode" required>

              <span ng-show="(updateFrm.pincode.$dirty && updateFrm.pincode.$error.required) || (submitted && updateFrm.pincode.$error.required)" style="color:red">Please enter pincode.</span>
              <span ng-show="updateFrm.pincode.$error.pattern" style="color:red">Please enter correct pincode.</span>

          </md-input-container>

 <md-input-container class="md-block">
    <label><?php echo lang('Address 1'); ?></label>
    <textarea name="address1" ng-model="person.address1" md-maxlength="150" ></textarea> 
  </md-input-container>

  <md-input-container class="md-block">
    <label><?php echo lang('Address 2'); ?></label>
    <textarea name="address2" ng-model="person.address2" md-maxlength="150" ></textarea> 
  </md-input-container>
          

          <md-divider>
          </md-divider>
          <br><br>
          <md-button ng-click="setsubmitted() && updateFrm.$valid  && UpdatePerson(person.id)" class="template-button" ng-disabled="update == true">
            <span ng-hide="update == true"><?php echo lang('update');?></span>
            <md-progress-circular class="white" ng-show="update == true" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
          </md-button>
          <md-button ng-click="close()" class="">
            <span><?php echo lang('cancel');?></span>
          </md-button>
        </md-list>
      </md-content>
    </form>
    </md-dialog-content>
  </md-dialog>
</script>
<script type="text/javascript">
  var lang = {};
  lang.new = '<?php echo lang('new') ?>';
  lang.name = '<?php echo lang('name') ?>';
  lang.add = '<?php echo lang('add') ?>';
  lang.cancel = '<?php echo lang('cancel') ?>';
  lang.save = '<?php echo lang('save') ?>';
  lang.edit = '<?php echo lang('edit') ?>';

  lang.doIt = "<?php echo lang('doIt')?>";
  lang.cancel = "<?php echo lang('cancel')?>";
  lang.attention = "<?php echo lang('attention')?>";
  lang.delete_area = "<?php echo lang('manufacturerattentiondetail')?>";
</script>

<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script src="<?php echo base_url('assets/lib/highcharts/highcharts.js')?>"></script>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/manufacturer.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/moment.min.js') ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/patient.js') ?>"></script>