<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<style type="text/css">
.progress-bar {
  background-color: rgb(34, 194, 129) !important;
}
</style>
<div class="lw-body-content" ng-controller="Stock_Controller">
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <div class="col-md-12" style="padding: 0px">
      <div class="panel-default">
        <div class="lw-invoice-summary"></div>
      </div>
      <md-toolbar class="toolbar-white">
        <div class="md-toolbar-tools">
          <h2 flex md-truncate class="text-bold"><?php echo lang('adv_stock_info'); ?> <small>(<span ng-bind="stocks.length"></span>)</small><br>
            </h2>
          <div class="lw-external-search-in-table">
            <input ng-model="stock_search" class="search-table-external" id="search" name="search" type="text" placeholder="<?php echo lang('searchword')?>">
            <md-button class="md-icon-button" aria-label="Search" ng-cloak>
              <md-icon><i class="ion-search text-muted"></i></md-icon>
            </md-button>
          </div>
          <?php if (check_privilege('marketing/adv_stock', 'create') == true) { ?>
            <md-button  ng-href="<?php echo base_url('marketing/add_adv_stock')?>" class="md-icon-button" aria-label="New" ng-cloak>
              <md-tooltip md-direction="bottom"><?php echo lang('addadvstock') ?></md-tooltip>
              <md-icon><i class="ion-android-add-circle text-success"></i></md-icon>
            </md-button>
          <?php } ?>
        </div>
      </md-toolbar>
      <md-content style="padding-top: 0px;">
        <div ng-show="stocksLoader" layout-align="center center" class="text-center" id="circular_loader">
          <md-progress-circular md-mode="indeterminate" md-diameter="30"></md-progress-circular>
          <p style="font-size: 15px;margin-bottom: 5%;">
            <span><?php echo lang('please_wait') ?> <br>
            <small><strong><?php echo lang('loading'). ' '. lang('stock_information').'...' ?></strong></small></span>
          </p>
        </div>
      <md-content ng-show="!stocksLoader" class="bg-white" ng-cloak> 
				<md-table-container ng-show="stocks.length > 0">
					<table md-table  md-progress="promise">
						<thead md-head md-order="stock_list.order">
							<tr md-row>
								<th md-column md-order-by="staffname"><span><?php echo lang('salesperson'); ?></span></th>
								<th md-column md-order-by="adv_product"><span><?php echo lang('product'); ?></span></th>
                <th md-column md-order-by="date"><span><?php echo lang('date'); ?></span></th>
								<th md-column md-order-by="quantity"><span><?php echo lang('quantity'); ?></span></th>
                <th md-column><span><?php echo lang('action'); ?></span></th>
							</tr>
						</thead>
						<tbody md-body>
							<tr class="select_row" md-row ng-repeat="stock in stocks | orderBy: stock_list.order | limitTo: stock_list.limit : (stock_list.page -1) * stock_list.limit | filter: stock_search | filter: FilteredData" class="cursor">
								<td md-cell>
									<strong><span ng-bind="stock.staffname"></span></strong>
								</td>
								<td md-cell>
									<strong><span ng-bind="stock.adv_product"></span></strong>
								</td>
                <td md-cell>
                  <strong><span ng-bind="stock.date"></span></strong>
                </td>
								<td md-cell>
                  <strong><span ng-bind="stock.quantity"></span></strong>
								</td>
								<td md-cell>
                <?php if (check_privilege('marketing/adv_stock', 'edit')) { ?> 
                  <md-icon ng-click="editAdvStock(stock.id)" md-menu-align-target class="ion-compose"></md-icon>
                <?php } ?>
                <?php if (check_privilege('marketing/adv_stock', 'delete')) { ?> 
                  <md-button ng-click="DeleteStock(stock.id)" class="md-icon-button md-primary" aria-label="Actions" ng-cloak>
                    <md-icon class="ion-trash-b"></md-icon>
                  </md-button>                    
                <?php } ?>
              </td>
							</tr>
						</tbody>
					</table>
				</md-table-container>
				<md-table-pagination ng-show="stocks.length > 0" md-limit="stock_list.limit" md-limit-options="limitOptions" md-page="stock_list.page" md-total="{{stocks.length}}" ></md-table-pagination>
				<md-content ng-show="!stocks.length" class="md-padding no-item-data"><?php echo lang('notdata') ?></md-content>	
			</md-content>
    </div>
  </div>
</div>

<script type="text/ng-template" id="update_adv_stock.html">
  <md-dialog aria-label="Payment">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <h2><strong class="text-success"><?php echo lang('update').' '.lang('stock');?></strong></h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="close()">
          <md-icon class="ion-close-round" aria-label="Close dialog" style="color:black"></md-icon>
        </md-button>
      </div>
    </md-toolbar>
    <md-dialog-content style="max-width:800px;max-height:810px; ">
      <md-content class="bg-white">
        <md-list flex>
          <md-list-item>
            <md-input-container class="md-block full-width">
                <label><?php echo lang('date'); ?></label>
                <input mdc-datetime-picker="" date="true" time="false" type="text" id="datetime" placeholder="<?php echo lang('chooseadate') ?>" show-todays-date="" minutes="true" show-icon="true" ng-model="stock.date" class=" dtp-no-msclear dtp-input md-input" required>
              </md-input-container>
          </md-list-item> 

          <md-list-item>
            <md-input-container class="md-block full-width">
              <label><?php echo lang('salesperson'); ?></label>
              <md-select placeholder="<?php echo lang('salesperson'); ?>" ng-model="stock.salesman_id" required>
                <md-option ng-value="name.id" ng-repeat="name in salesperson">{{name.staffname}}</md-option>
              </md-select>
            </md-input-container>
          </md-list-item>

          <md-list-item>
            <md-input-container class="md-block full-width">
              <label><?php echo lang('products'); ?></label>
              <md-select placeholder="<?php echo lang('product'); ?>" ng-model="stock.product_id" required>
                <md-option ng-value="product.id" ng-repeat="product in advproducts">{{product.name}}</md-option>
              </md-select>
            </md-input-container>
          </md-list-item>  

          <md-list-item>
            <md-input-container class="md-block full-width">
              <label><?php echo lang('quantity'); ?></label>
              <input ng-model="stock.quantity" required>
            </md-input-container>
          </md-list-item> 

          <md-divider>
          </md-divider>
          <br><br>
          <md-button ng-click="UpdateStock(stock.id)" class="template-button" ng-disabled="update == true">
            <span ng-hide="update == true"><?php echo lang('update');?></span>
            <md-progress-circular class="white" ng-show="update == true" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
          </md-button>
          <md-button ng-click="close()" class="">
            <span><?php echo lang('cancel');?></span>
          </md-button>
        </md-list>
      </md-content>
    </md-dialog-content>
  </md-dialog>
</script>

<script type="text/javascript">
var lang = {};
lang.new = '<?php echo lang('new') ?>';
lang.name = '<?php echo lang('name') ?>';
lang.add = '<?php echo lang('add') ?>';
lang.cancel = '<?php echo lang('cancel') ?>';
lang.save = '<?php echo lang('save') ?>';
lang.edit = '<?php echo lang('edit') ?>';

lang.doIt = "<?php echo lang('doIt')?>";
lang.cancel = "<?php echo lang('cancel')?>";
lang.attention = "<?php echo lang('attention')?>";
lang.delete_stock = "<?php echo lang('stockattentiondetail')?>";
</script>

<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/marketing.js') ?>"></script>
<script type="text/javascript">

</script>