<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<div class="lw-body-content" ng-controller="AddStock_Controller">
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <h2 flex md-truncate ><?php echo lang('addadvstock') ?></h2>
        <md-button type="submit" ng-click="AddStock()" class="md-icon-button" aria-label="Save" ng-cloak>
          <md-progress-circular ng-show="savingExpense == true" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
          <md-tooltip ng-hide="savingExpense == true" md-direction="bottom"><?php echo lang('create') ?></md-tooltip>
          <md-icon ng-hide="savingExpense == true"><i class="ion-checkmark-circled text-success"></i></md-icon>
        </md-button>
      </div>
    </md-toolbar>
    <div ng-show="expensesLoader" layout-align="center center" class="text-center" id="circular_loader">
      <md-progress-circular md-mode="indeterminate" md-diameter="40"></md-progress-circular>
      <p style="font-size: 15px;margin-bottom: 5%;">
        <span><?php echo lang('loading'). ' '. lang('please_wait'). '....' ?> <br>
        </span>
      </p>
    </div>
    <md-content ng-show="!expensesLoader" class="bg-white" layout-padding ng-cloak>
      <div layout-gt-xs="row">
        <div class="col-md-3">
          <md-input-container>
            <label><?php echo lang('date') ?></label>
            <md-datepicker required name="date" ng-model="addstock.date" md-open-on-focus></md-datepicker>
            <md-tooltip md-direction="top"><?php echo lang('date') ?></md-tooltip>
          </md-input-container>
        </div>

        <div class="col-md-3">
          <md-input-container class="md-block">
            <label><?php echo lang('salesperson'); ?></label>
            <md-select required placeholder="<?php echo lang('choisesalesperson'); ?>" ng-model="addstock.salesperson_id" name="salesperson" style="min-width: 200px;">
              <md-option ng-value="person.id" ng-repeat="person in salesperson">{{person.staffname}}</md-option>
            </md-select>
          </md-input-container>
        </div>

        <div class="col-md-3">
          <md-input-container class="md-block">
            <label><?php echo lang('products'); ?></label>
            <md-select placeholder="<?php echo lang('choiseproduct'); ?>" ng-model="addstock.product_id" name="product" style="min-width: 200px;" ng-change="add(addstock.product_id)">
              <md-option ng-value="product.id" ng-repeat="product in advproducts">{{product.name}}</md-option>
            </md-select>
          </md-input-container>
        </div>
        <br>
      </div>
      
      <div ng-show="expense_recurring" layout-gt-xs="row">
        <md-input-container class="md-block" flex-gt-xs>
          <label><?php echo lang('recurring_period') ?></label>
          <input type="number" ng-value="1" value="1" ng-init="recurring_period = 1" min="1" ng-model="recurring_period" name="recurring_period">
        </md-input-container>
        <md-input-container class="md-block" flex-gt-xs>
          <label><?php echo lang('recurring_type') ?></label>
          <md-select ng-model="recurring_type" name="recurring_type">
            <md-option value="0"><?php echo lang('days') ?></md-option>
            <md-option value="1"><?php echo lang('weeks') ?></md-option>
            <md-option value="2" selected><?php echo lang('months') ?></md-option>
            <md-option value="3"><?php echo lang('years') ?></md-option>
          </md-select>
        </md-input-container>
        <br>
        <md-input-container>
          <label><?php echo lang('ends_on') ?></label>
          <md-datepicker md-min-date="date" name="EndRecurring" ng-model="EndRecurring" style="min-width: 100%;" md-open-on-focus></md-datepicker>
          <div >
            <div ng-message="required" class="my-message"><?php echo lang('leave_blank_for_lifetime') ?></div>
          </div>
        </md-input-container>
      </div>
    </md-content>
    <md-content ng-show="!expensesLoader" class="bg-white" layout-padding ng-cloak>
      <div layout-gt-sm="row">
        <div class="col-md-4">
          <label><?php echo lang('product'); ?></label>
        </div>
        <div class="col-md-1">
          <label><?php echo lang('quantity'); ?></label>
        </div>
        <div class="col-md-1">
          <label><?php echo lang('action'); ?></label>
        </div>
      </div>
      <md-list-item ng-repeat="item in addstock.items" ng-show="loaditems">

        <div layout-gt-sm="row" style="width:50%">
          <div class="col-md-8">
            <md-input-container class="md-block" flex-gt-sm>
              <input type="hidden" ng-model="item.product_id" >
              <span ng-bind="item.name" style="line-height: 26px;width: 100%;float: left;border-style: solid;border-width: 0 0 1px;"></span>
            </md-input-container>
          </div>
          <div class="col-md-2">
            <md-input-container class="md-block" flex-gt-sm>
              <input class="min_input_width" ng-model="item.quantity" >
            </md-input-container>
          </div>
          <div class="col-md-2">
            <md-icon aria-label="Remove Line" ng-click="remove($index)" class=" ion-trash-b text-muted" style="margin-top: 20px;"></md-icon>
          </div>
        </div>        
      </md-list-item>
    </md-content>
  </div>
</div>
<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/marketing.js') ?>"></script>