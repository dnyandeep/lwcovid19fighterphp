<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<div class="lw-body-content" ng-controller="Distribution_Info_Controller">
  <style type="text/css">
  rect.highcharts-background {
    fill: #f3f3f3;
  }
  /*added for text editor */
  .ta-editor { 
  min-height: 300px;
  height: auto;
  overflow: auto;
  font-family: inherit;
  font-size: 100%;
}
  </style>
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <md-button class="md-icon-button" aria-label="File">
          <md-icon><i class="ion-document text-muted"></i></md-icon>
        </md-button>
        <h2 flex md-truncate><?php echo lang('distribution_information'); ?><small>(<span ng-bind="distributions.length"></span>)</small>
        </h2>
        <div class="lw-external-search-in-table">
          <input ng-model="distribution_search" class="search-table-external" id="search" name="search" type="text" placeholder="<?php echo lang('searchword') ?>">
          <md-button class="md-icon-button" aria-label="Search" ng-cloak>
            <md-icon><i class="ion-search text-muted"></i></md-icon>
          </md-button>
        </div>
      </div>
    </md-toolbar>
    <div ng-show="distributionsLoader" layout-align="center center" class="text-center" id="circular_loader" ng-cloak>
      <md-progress-circular md-mode="indeterminate" md-diameter="30"></md-progress-circular>
      <p style="font-size: 15px;margin-bottom: 5%;">
        <span><?php echo lang('please_wait') ?> <br>
          <small><strong><?php echo lang('loading') . ' ' . lang('distribution_information') . '...' ?></strong></small></span>
      </p>
    </div>
    <md-content ng-show="!distributionsLoader" class="bg-white" ng-cloak>
      <md-table-container ng-show="distributions.length > 0">
        <table md-table md-progress="promise">
          <thead md-head md-order="distribution_list.order">
            <tr md-row>
              <th md-column md-order-by="date"><span><?php echo lang('date'); ?></span></th>
              <th md-column md-order-by="staffname"><span><?php echo lang('salesperson'); ?></span></th>
              <th md-column md-order-by="adv_product"><span><?php echo lang('product'); ?></span></th>
              <th md-column md-order-by="quantity"><span><?php echo lang('quantity'); ?></span></th>
              <th md-column md-order-by="customer"><span><?php echo lang('customer'); ?></span></th>
            </tr>
          </thead>
          <tbody md-body>
            <tr class="select_row" md-row ng-repeat="distribution in distributions | orderBy: distribution_list.order | limitTo: distribution_list.limit : (distribution_list.page -1) * distribution_list.limit | filter: distribution_search | filter: FilteredData" class="cursor" >
              <td md-cell>
                <strong><span ng-bind="distribution.date"></span></strong><br>
              </td>
              <td md-cell>
                <strong><span ng-bind="distribution.staffname"></span></strong><br>
              </td>
              <td md-cell>
                <strong><span ng-bind="distribution.adv_product"></span></strong><br>
              </td>
              <td md-cell>
                <strong><span ng-bind="distribution.quantity"></span></strong><br>
              </td>
              <td md-cell>
                <strong><span ng-bind="distribution.customer"></span></strong><br>
              </td>
            </tr>
          </tbody>
        </table>
      </md-table-container>
      <md-table-pagination ng-show="distributions.length > 0" md-limit="distribution_list.limit" md-limit-options="limitOptions" md-page="distribution_list.page" md-total="{{distribution.length}}"></md-table-pagination>
      <md-content ng-show="!distributions.length && !distributionsLoader" class="md-padding no-item-data">
        <?php echo lang('notdata') ?></md-content>
    </md-content>
  </div>
</div>

<script type="text/javascript">
var lang = {};
lang.new = '<?php echo lang('new') ?>';
lang.name = '<?php echo lang('name') ?>';
lang.add = '<?php echo lang('add') ?>';
lang.cancel = '<?php echo lang('cancel') ?>';
lang.save = '<?php echo lang('save') ?>';
lang.edit = '<?php echo lang('edit') ?>';

lang.doIt = "<?php echo lang('doIt')?>";
lang.cancel = "<?php echo lang('cancel')?>";
lang.attention = "<?php echo lang('attention')?>";
lang.delete_adv_material = "<?php echo lang('advmaterialattentiondetail')?>";
</script>

<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script src="<?php echo base_url('assets/lib/highcharts/highcharts.js')?>"></script>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/marketing.js') ?>"></script>

  <!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.1.2/textAngular.min.js'></script> --> <!-- added for text editor -->
