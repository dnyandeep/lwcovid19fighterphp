<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<!-- <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css'> --> <!-- added for text editor -->
<!-- <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.min.css'> --> <!-- added for text editor -->
<div class="lw-body-content" ng-controller="Adv_Material_Controller">
  <style type="text/css">
  rect.highcharts-background {
    fill: #f3f3f3;
  }
  /*added for text editor */
  .ta-editor { 
  min-height: 300px;
  height: auto;
  overflow: auto;
  font-family: inherit;
  font-size: 100%;
}
  </style>
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <md-button class="md-icon-button" aria-label="File">
          <md-icon><i class="ion-document text-muted"></i></md-icon>
        </md-button>
        <h2 flex md-truncate><?php echo lang('advertising_material'); ?><small>(<span ng-bind="materials.length"></span>)</small>
        </h2>
        <div class="lw-external-search-in-table">
          <input ng-model="material_search" class="search-table-external" id="search" name="search" type="text" placeholder="<?php echo lang('searchword') ?>">
          <md-button class="md-icon-button" aria-label="Search" ng-cloak>
            <md-icon><i class="ion-search text-muted"></i></md-icon>
          </md-button>
        </div>
        <?php if (check_privilege('marketing/adv_material', 'create')) { ?> 
          <md-button ng-click="Create()" class="md-icon-button" aria-label="New" ng-cloak>
            <md-tooltip md-direction="bottom"><?php echo lang('create') ?></md-tooltip>
            <md-icon><i class="ion-android-add-circle text-success"></i></md-icon>
          </md-button>
        <?php } ?>
      </div>
    </md-toolbar>
    <div ng-show="materialsLoader" layout-align="center center" class="text-center" id="circular_loader" ng-cloak>
      <md-progress-circular md-mode="indeterminate" md-diameter="30"></md-progress-circular>
      <p style="font-size: 15px;margin-bottom: 5%;">
        <span><?php echo lang('please_wait') ?> <br>
          <small><strong><?php echo lang('loading') . ' ' . lang('advertising_material') . '...' ?></strong></small></span>
      </p>
    </div>
    <md-content ng-show="!materialsLoader" class="bg-white" ng-cloak>
      <md-table-container ng-show="materials.length > 0">
        <table md-table md-progress="promise">
          <thead md-head md-order="material_list.order">
            <tr md-row>
              <th md-column md-order-by="name"><span><?php echo lang('name'); ?></span></th>
              <th md-column md-order-by="unit_id"><span><?php echo lang('unit'); ?></span></th>
              <th md-column md-order-by="category"><span><?php echo lang('category'); ?></span></th>
              <th md-column><span><?php echo lang('action'); ?></span></th>
            </tr>
          </thead>
          <tbody md-body>
            <tr class="select_row" md-row ng-repeat="material in materials | orderBy: material_list.order | limitTo: material_list.limit : (material_list.page -1) * material_list.limit | filter: material_search | filter: FilteredData" class="cursor" >
              <td md-cell>
                <strong><span ng-bind="material.name"></span></strong><br>
              </td>
              <td md-cell>
                <strong><span ng-bind="material.unit_name"></span></strong><br>
              </td>
              <td md-cell>
                <strong><span ng-bind="material.category"></span></strong><br>
              </td>
              <td md-cell>
                <?php if (check_privilege('marketing/adv_material', 'edit')) { ?> 
                  <md-icon ng-click="editAdvMaterial(material)" md-menu-align-target class="ion-compose"></md-icon>
                <?php } ?>
                <?php if (check_privilege('marketing/adv_material', 'delete')) { ?> 
                  <md-button ng-click="Delete(material.id)" class="md-icon-button md-primary" aria-label="Actions" ng-cloak>
                    <md-icon class="ion-trash-b"></md-icon>
                  </md-button>                    
                <?php } ?>
              </td>
            </tr>
          </tbody>
        </table>
      </md-table-container>
      <md-table-pagination ng-show="materials.length > 0" md-limit="material_list.limit" md-limit-options="limitOptions" md-page="material_list.page" md-total="{{material.length}}"></md-table-pagination>
      <md-content ng-show="!materials.length && !materialsLoader" class="md-padding no-item-data">
        <?php echo lang('notdata') ?></md-content>
    </md-content>
  </div>
  
  <md-sidenav class="md-sidenav-right md-whiteframe-4dp" md-component-id="Create" style="width: 450px;" ng-cloak>
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <md-button ng-click="close()" class="md-icon-button" aria-label="Close"> <i class="ion-android-arrow-forward"></i> </md-button>
        <h2 flex md-truncate><?php echo lang('create') ?></h2>
      </div>
    </md-toolbar>
    <md-content>
      <md-content layout-padding>
        <md-input-container class="md-block">
          <label><?php echo lang('adv_product_name'); ?></label>
          <input name="name" ng-model="materials.name" required>
        </md-input-container>
        <md-input-container class="md-block" style="width:100%;">
          <label><?php echo lang('units'); ?></label>
          <md-select placeholder="<?php echo lang('units'); ?>" ng-model="materials.unit_id" name="unit_id" required>
            <md-option ng-value="unit.id" ng-repeat="unit in units">{{unit.name}}</md-option>
          </md-select>
        </md-input-container>

        <md-input-container class="md-block" flex-gt-xs>
          <label><?php echo lang('category'); ?></label>
          <md-select placeholder="<?php echo lang('category'); ?>" ng-model="materials.category" name="category" style="min-width: 200px;" required>
            <?php 
            $option = array('Marketing' => 'Marketing','Free Sample' => 'Free Sample');
            foreach ($option as $opt) { ?>
              <md-option ng-value='"<?php echo $opt ?>"'><?php echo $opt ?></md-option>
            <?php } ?>
          </md-select>
        </md-input-container>

        <md-input-container class="md-block" style="width:100%;">
          <div>
          <strong><?php echo lang('product') . ' ' . lang('image'); ?></strong>
        </div>
          
        <div class="file-upload">
          <div class="file-select">
            <div class="file-select-button" id="fileName"><span class="mdi mdi-accounts-list-alt"></span> <?php echo lang('image')?></div>
            <div class="file-select-name" id="noFile"><?php echo lang('notchoise')?></div>
            <input type="file" name="attachment" id="chooseFile" file-model="materials.attachment">
          </div>
        </div>
        </md-input-container>

        <md-input-container class="md-block">
          <label><?php echo lang('product').' '.lang('details') ?></label>
          <br>
          <textarea class="tinymce" ng-model="materials.description"></textarea>
        </md-input-container>

      </md-content>
      <custom-fields-vertical></custom-fields-vertical>
      <md-content class="layout-padding">
    </md-content>
      <md-content layout-padding>
        <section layout="row" layout-sm="column" layout-align="center center" layout-wrap>
          <md-button ng-click="AddAdvMaterial()" class="md-raised md-primary btn-report block-button" ng-disabled="saving == true">
            <span ng-hide="saving == true"><?php echo lang('create'); ?></span>
            <md-progress-circular class="white" ng-show="saving == true" md-mode="indeterminate" md-diameter="20">
            </md-progress-circular>
          </md-button>
          <br/><br/><br/><br/>
        </section>
      </md-content>
    </md-content>
  </md-sidenav>
</div>

<script type="text/ng-template" id="update_adv_material.html">
  <md-dialog aria-label="Payment" style="min-width: 50%;">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <h2><strong class="text-success"><?php echo lang('update').' '.lang('advertising_material');?></strong></h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="close()">
          <md-icon class="ion-close-round" aria-label="Close dialog" style="color:black"></md-icon>
        </md-button>
      </div>
    </md-toolbar>
    <md-dialog-content style="max-width:800px;max-height:810px; ">
      <md-content class="bg-white">
        <md-list flex>
          <div class="row">
            <div class="col-md-6">
              <md-list-item>
                <md-input-container class="md-block full-width">
                  <label><?php echo lang('adv_product_name'); ?></label>
                  <input ng-model="material.name" required>
                </md-input-container>
              </md-list-item> 
            </div>
            
            <div class="col-md-6">
              <md-list-item>
                <md-input-container  class="md-block full-width">
                  <label><?php echo lang('units'); ?></label>
                  <md-select ng-model="material.unit_id" required>
                    <md-option ng-value="unit.id" ng-repeat="unit in units">{{unit.name}}</md-option>
                  </md-select>
                </md-input-container>
              </md-list-item>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <md-list-item>
                <md-input-container class="md-block full-width">
                  <label><?php echo lang('category'); ?></label>
                    <md-select placeholder="<?php echo lang('category'); ?>" ng-model="material.category" name="category" style="min-width: 200px;" required>
                    <?php 
                      $option = array('Marketing' => 'Marketing','Free Sample' => 'Free Sample');
                      foreach ($option as $opt) { ?>
                      <md-option ng-value='"<?php echo $opt ?>"'><?php echo $opt ?></md-option>
                    <?php } ?>
                    </md-select>
                </md-input-container> 
              </md-list-item> 
            </div>

            <div class="col-md-6">
              <md-list-item>
                <md-input-container class="md-block full-width">
                  <div>
                    <strong><?php echo lang('product') . ' ' . lang('image'); ?></strong>
                  </div>
              
                  <div class="file-upload">
                    <input type="file" required name="attachment" file-model="material.attachment">
                  </div>
                  <div>
                    <a class="cursor" ng-href="<?php echo base_url('marketing/download_file/{{material.image}}');?>" href="<?php echo base_url('marketing/download_file/{{material.image}}');?>">
                    <h4 class="link" ng-bind="material.image"></h4>
                    </a>
                  </div>
                </md-input-container>
              </md-list-item> 
            </div>
          </div> 
          <div class="row">
            <div class="col-md-12">
            <md-list-item>
              <md-input-container class="md-block">
                <label><?php echo lang('product').' '.lang('details') ?></label>
                <br>
                <textarea class="tinymce" ng-model="material.description" style="width:80%"></textarea>
              </md-input-container >
            </md-list-item>
          </div>
          </div>
          

          <md-divider>
          </md-divider>
          <br><br>
          <md-button ng-click="UpdateAdvMaterial(material.id)" class="template-button" ng-disabled="update == true">
            <span ng-hide="update == true"><?php echo lang('update');?></span>
            <md-progress-circular class="white" ng-show="update == true" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
          </md-button>
          <md-button ng-click="close()" class="">
            <span><?php echo lang('cancel');?></span>
          </md-button>
        </md-list>
      </md-content>
    </md-dialog-content>
  </md-dialog>
</script>
<script type="text/javascript">
var lang = {};
lang.new = '<?php echo lang('new') ?>';
lang.name = '<?php echo lang('name') ?>';
lang.add = '<?php echo lang('add') ?>';
lang.cancel = '<?php echo lang('cancel') ?>';
lang.save = '<?php echo lang('save') ?>';
lang.edit = '<?php echo lang('edit') ?>';

lang.doIt = "<?php echo lang('doIt')?>";
lang.cancel = "<?php echo lang('cancel')?>";
lang.attention = "<?php echo lang('attention')?>";
lang.delete_adv_material = "<?php echo lang('advmaterialattentiondetail')?>";
</script>

<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script src="<?php echo base_url('assets/lib/highcharts/highcharts.js')?>"></script>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script src="<?php echo base_url('assets/lib/tinymce/tinymce.min.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/marketing.js') ?>"></script>

 <script>

  tinymce.init({ 
    selector:'textarea',
    theme: 'modern',
    //height: 200,
    editor_selector : "mceEditor",
    theme: 'modern',
    valid_elements : '*',
    valid_styles: '*', 
    plugins: 'print preview searchreplace autoresize autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking importcss anchor code insertdatetime advlist lists textcolor wordcount imagetools  contextmenu colorpicker textpattern',
    //body_class: 'my_class',
    valid_children : "+body[style]",
    valid_elements: "@[id|class|title|style],"
+ "a[name|href|target|title|alt],"
+ "#p,-ol,,div,h1,h2,h3,h4,h5,h6,strong,-ul,-li,br,img[src|unselectable],-sub,-sup,-b,-i,-u,"
+ "-span[data-mce-type],hr",

valid_child_elements : "body[p,ol,ul,div,h1,h2,h3,h4,h5,h6,strong,b]"
+ ",p[a|span|b|i|u|sup|sub|img|hr|#text]"
+ ",span[a|b|i|u|sup|sub|img|#text]"
+ ",a[span|b|i|u|sup|sub|img|#text]"
+ ",b[span|a|i|u|sup|sub|img|#text]"
+ ",i[span|a|b|u|sup|sub|img|#text]"
+ ",sup[span|a|i|b|u|sub|img|#text]"
+ ",sub[span|a|i|b|u|sup|img|#text]"
+ ",li[span|a|b|i|u|sup|sub|img|ol|ul|#text]"
+ ",ol[li]"
+ ",ul[li]",
    content_css: [
    //fonts.googleapis.com/css?family=Lato:300,300i,400,400i’,
    //www.tinymce.com/css/codepen.min.css’
    ],
    toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'
  });
</script>
