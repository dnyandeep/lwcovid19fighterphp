<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<div class="lw-body-content" ng-controller="Staffs_Controller">
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12" layout="row" layout-wrap>
    <md-toolbar ng-show="!customersLoader" class="toolbar-white" style="margin: 0px 8px 8px 8px;">
    <div class="md-toolbar-tools">
      <md-button class="md-icon-button" aria-label="File">
        <md-icon><i class="ico-lw-staff text-muted"></i></md-icon>
      </md-button>
      <h2 flex md-truncate><?php echo lang('staff'); ?> <small>(<span ng-bind="staff.length"></span>)</small></h2>
      <div class="lw-external-search-in-table">
        <input ng-model="search" class="search-table-external" id="search" name="search" type="text" placeholder="<?php echo lang('search').' '.lang('staff') ?>">
        <md-button class="md-icon-button" aria-label="Search" ng-cloak>
          <md-icon><i class="ion-search text-muted"></i></md-icon>
        </md-button>
      </div>
      <?php if (check_privilege('staff', 'create')) { ?>
        <md-button ng-click="Create()" class="md-icon-button" aria-label="New" ng-cloak>
          <md-tooltip md-direction="bottom"><?php echo lang('create').' '.lang('category') ?></md-tooltip>
          <md-icon><i class="ion-android-add-circle text-success"></i></md-icon>
        </md-button>
      <?php }?>
    </div>
  </md-toolbar>
  <br>
    
    <md-content flex-gt-xs="100" flex-xs="100" class="bg-white" ng-cloak style="margin: -20px 8px 8px 8px;">
      <md-table-container ng-show="staff.length > 0">
        <table md-table  md-progress="promise">
          <thead md-head md-order="staff_list.order">
            <tr md-row>
              <th md-column><span>#</span></th>
              <th md-column><span><?php echo lang('staff'); ?></span></th>
              <th md-column><span><?php echo lang('type'); ?></span></th>
              <th md-column md-order-by="created"><span><?php echo lang('email'); ?></span></th>
              <th md-column md-order-by="duedate"><span><?php echo lang('department'); ?></span></th>
              <th md-column md-order-by="status"><span><?php echo lang('phone'); ?></span></th>
              <!-- <th md-column md-order-by="total"><span><?php echo lang('amount'); ?></span></th> -->
            </tr>
          </thead>
          <tbody md-body>
            <tr class="select_row" md-row ng-repeat="member in staff | orderBy: staff_list.order | limitTo: staff_list.limit : (staff_list.page -1) * staff_list.limit | filter: search | filter: FilteredData">
              <td md-cell>
                <div style="margin-top: 5px;" data-toggle="tooltip" data-placement="left" data-container="body" title="" data-original-title="Created by: {{member.name}}" class="assigned-staff-for-this-lead user-avatar"><img ng-click="ViewStaff(member.id)" ng-src="<?php echo base_url('uploads/images/{{member.avatar}}')?>" alt="{{member.name}}"></div>
              </td>
              <td md-cell>
                <div>
                  <strong>
                    <a class="link cursor" ng-click="ViewStaff(member.id)"> <span ng-bind="member.name"></span></a>
                  </strong><br>
                  <small class="blur" ng-bind="member.staff_number"></small>
                </div>
              </td>
              <td md-cell>
                <span class="sup-label" style="background: <?php echo '{{member.color}}' ?>;font-size: 14px !important;" ng-bind="member.type"></span>
              </td>
              <td md-cell>
                <span><a ng-href="email:{{member.email}}"><span ng-bind="member.email"></span></a></span>
              </td>
              <td md-cell>
                <span><span ng-bind="member.department"></span></span>
              </td>
              <td md-cell>
                <a href="tel:{{member.phone}}">{{member.phone}}</a>
              </td>
            </tr>
          </tbody>
        </table>
      </md-table-container>
      <md-table-pagination ng-show="staff.length > 0" md-limit="staff_list.limit" md-limit-options="limitOptions" md-page="staff_list.page" md-total="{{staff.length}}" ></md-table-pagination>
    </md-content>
    
  </div>
  
  <md-sidenav class="md-sidenav-right md-whiteframe-4dp" md-component-id="Create" ng-cloak style="width: 450px;">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <md-button ng-click="close()" class="md-icon-button" aria-label="Close"> <i class="ion-android-arrow-forward"></i> </md-button>
        <h2 flex md-truncate><?php echo lang('create') ?></h2>
      </div>
    </md-toolbar>
    <md-content>
      <md-content layout-padding>
        <md-input-container class="md-block">
          <label><?php echo lang('name') ?></label>
          <input required type="text" ng-model="categories.name" class="form-control" id="title">
        </md-input-container>
        <md-input-container class="md-block" flex-gt-xs>
          <label><?php echo lang('categories'); ?></label>
          <md-select placeholder="<?php echo lang('categories'); ?>" ng-model="categories.status" name="status" style="min-width: 200px;">
            <?php 
            $option = array('Avtive' => 'Active','In Active' => 'In Active');
            foreach ($option as $gateway) { ?>
              <md-option ng-value='"<?php echo $gateway ?>"'><?php echo $gateway ?></md-option>
            <?php } ?>
          </md-select>
        </md-input-container>
      </md-content>
      <custom-fields-vertical></custom-fields-vertical>
      <md-content>
        <section layout="row" layout-sm="column" layout-align="center center" layout-wrap>
          <?php if (check_privilege('categories', 'create')) { ?>
            <md-button ng-click="AddStaff()" class="md-raised md-primary btn-report block-button" ng-disabled="saving == true">
              <span ng-hide="saving == true"><?php echo lang('add');?></span>
              <md-progress-circular class="white" ng-show="saving == true" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
            </md-button>
          <?php }?>
          <br/><br/><br/><br/>
        </section>
      </md-content>
    </md-content>
  </md-sidenav>
</div>
<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/staffs.js'); ?>"></script>