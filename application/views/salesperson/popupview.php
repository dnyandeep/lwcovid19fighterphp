<?php include_once( APPPATH . 'views/inc/lw_solar_calculator.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<div id="pageContent">
  <div class="lw-body-content" ng-controller="Popupview_Controller" style="margin-left: 0px;">
    <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
      <md-content class="md-pt-0 bg-white">
        <div class="col-md-12">
          <section layout="row">
            <div id="us3" style="width: 100%; height: 630px;"></div> 
          </section> 
        </div>     
      </md-content>
    </div>
  </div>
</div>
<script type="text/javascript">
  var xmldata = '<?php echo $xml_data;?>';
</script>

<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCfaLjR2MOJOoSz6Wiel3aejSYBxqJBs7o'></script>
<script src="<?php echo base_url('assets/lib/highcharts/highcharts.js')?>"></script>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/salesperson.js'); ?>"></script>
