<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<div id="pageContent">
  <div class="lw-body-content" ng-controller="Location_Controller">
    <style type="text/css">
      /*rect.highcharts-background {
        fill: #f3f3f3;
      }*/
    </style>
    <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
      <md-toolbar class="toolbar-white">
        <div class="md-toolbar-tools">
          <h2 flex md-truncate class="text-bold"><?php echo lang('salesperson').' '.lang('currentlocation') ; ?> 
          </h2>        
        </div>
      </md-toolbar>
        
        <div ng-show="isLoading" layout-align="center center" class="text-center" id="circular_loader">
        <md-progress-circular md-mode="indeterminate" md-diameter="40"></md-progress-circular>
        <p style="font-size: 15px;margin-bottom: 5%;">
          <span><?php echo lang('please_wait') ?> <br>
            <small><strong><?php echo lang('loading') . '...' ?></strong></small></span>
          </p>
        </div>
        <md-content ng-show="!isLoading" class="md-pt-0 bg-white">
           <div class="col-md-3" style="margin-top: 10px;">
            <section layout="row">
              <md-input-container class="md-block">
                <label><?php echo lang('salesperson'); ?></label>
                  <md-select placeholder="<?php echo lang('salesperson'); ?>" ng-model="salesperson.id" ng-change="getCurrentLocation(salesperson.id)" style="min-width: 200px;">
                    <md-option ng-value="name.id" ng-repeat="name in salesperson">{{name.staffname}}</md-option>
                  </md-select>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-9">
            <section layout="row">
              <label id="ulabel" style="display:none;margin-top: 40px;">Updated on :</label><label id="time" style="margin-top: 40px;"></label>
              <label id="slabel" style="display:none;margin-top: 40px;margin-left:10px;">Status :</label><label id="status" style="margin-top: 40px;"></label>
            </section>
          </div>

          <div class="col-md-12">
            <section layout="row">
              <div id="map" style="width: 100%; height: 400px;"></div> 
            </section> 
          </div>     
        </md-content>
      </div>
    </div>
  </div>
<script type="text/javascript">
  

</script>
<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCfaLjR2MOJOoSz6Wiel3aejSYBxqJBs7o'></script>
<script src="<?php echo base_url('assets/lib/highcharts/highcharts.js')?>"></script>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/salesperson.js'); ?>"></script>
