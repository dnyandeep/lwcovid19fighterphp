<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<div id="pageContent">
  <div class="lw-body-content" ng-controller="TrackSalesperson_Controller">
    <style type="text/css">
      /*rect.highcharts-background {
        fill: #f3f3f3;
      }*/
    </style>
    <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
      <md-toolbar class="toolbar-white">
        <div class="md-toolbar-tools">
          <h2 flex md-truncate class="text-bold"><?php echo lang('track_salesperson') ; ?> 
          </h2>        
        </div>
      </md-toolbar>
        
        <div ng-show="isLoading" layout-align="center center" class="text-center" id="circular_loader">
        <md-progress-circular md-mode="indeterminate" md-diameter="40"></md-progress-circular>
        <p style="font-size: 15px;margin-bottom: 5%;">
          <span><?php echo lang('please_wait') ?> <br>
            <small><strong><?php echo lang('loading') . '...' ?></strong></small></span>
          </p>
        </div>
        <md-content ng-show="!isLoading" class="md-pt-0 bg-white">
           <div class="col-md-3" style="margin-top: 10px;">
            <section layout="row">
              <md-input-container class="md-block">
                <label><?php echo lang('salesperson'); ?></label>
                  <md-select placeholder="<?php echo lang('salesperson'); ?>" ng-model="salesperson.id" style="min-width: 300px;">
                    <md-option ng-value="name.id" ng-repeat="name in salesperson">{{name.staffname}}</md-option>
                  </md-select>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-3" style="margin-top: 10px;">
            <section layout="row">
              <md-input-container>
                <label><?php echo lang('from_date') ?></label>
                  <md-datepicker name="from_date" ng-model="from_date" md-open-on-focus></md-datepicker>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-3" style="margin-top: 10px;">
            <section layout="row">
              <md-input-container>
                <label><?php echo lang('to_date') ?></label>
                  <md-datepicker name="to_date" ng-model="to_date" md-open-on-focus></md-datepicker>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-2" style="margin-top: 20px;">
            <section layout="row">
              <md-button ng-click="getTrackData()" class="md-raised md-primary pull-right"><?php echo lang('search'); ?></md-button>
            </section>
          </div>
        </md-content>

        <md-content class="md-pt-0 bg-white">
          <md-table-container ng-show="tracks.length > 0">
            <table md-table  md-progress="promise" ng-cloak>
              <thead md-head md-order="track_list.order">
                <tr md-row>
                  <th md-column md-order-by="start_time"><span><?php echo lang('start_time'); ?></span></th>
                  <th md-column md-order-by="end_time"><span><?php echo lang('end_time'); ?></span></th>
                  <th md-column md-order-by="start_address"><span><?php echo lang('start_address'); ?></span></th>
                  <th md-column md-order-by="end_address"><span><?php echo lang('end_address'); ?></span></th>
                  <th md-column md-order-by="distance"><span><?php echo lang('distance'); ?></span></th>
                </tr>
              </thead>
              <tbody md-body>
                <tr class="select_row" md-row ng-repeat="track in tracks | orderBy: track_list.order | limitTo: track_list.limit : (track_list.page -1) * track_list.limit | filter: search | filter: FilteredData" class="cursor" ng-click="displayMap(track.id)">
                  <td md-cell>
                    <strong ng-bind="track.start_time"></strong>
                  </td>
                  <td md-cell>
                    <strong ng-bind="track.end_time"></strong>
                  </td>
                  <td md-cell>
                    <strong ng-bind="track.start_address"></strong>
                  </td>
                  <td md-cell>
                    <strong ng-bind="track.end_address"></strong>
                  </td>
                  <td md-cell>
                    <strong ng-bind="track.distance"></strong>
                  </td>
                </tr>
              </tbody>
            </table>
          </md-table-container>
          <md-table-pagination ng-show="tracks.length > 0" md-limit="track_list.limit" md-limit-options="limitOptions" md-page="track_list.page" md-total="{{tracks.length}}" ></md-table-pagination>
          <md-content ng-show="!tracks.length" class="md-padding no-item-data" ng-cloak><?php echo lang('notdata') ?></md-content>
        </md-content>
      </div>
    </div>
  </div>
<script type="text/javascript">
  

</script>
<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCfaLjR2MOJOoSz6Wiel3aejSYBxqJBs7o'></script>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/salesperson.js'); ?>"></script>
