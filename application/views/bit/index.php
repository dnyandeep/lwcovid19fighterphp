<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<div class="lw-body-content" ng-controller="Area_Controller">
  <style type="text/css">
  rect.highcharts-background {
    fill: #f3f3f3;
  }
  </style>
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <md-button class="md-icon-button" aria-label="File">
          <md-icon><i class="ion-document text-muted"></i></md-icon>
        </md-button>
        <h2 flex md-truncate><?php echo lang('bit/area'); ?><small>(<span ng-bind="areas.length"></span>)</small>
        </h2>
        <div class="lw-external-search-in-table">
          <input ng-model="area_search" class="search-table-external" id="search" name="search" type="text" placeholder="<?php echo lang('searchword') ?>">
          <md-button class="md-icon-button" aria-label="Search" ng-cloak>
            <md-icon><i class="ion-search text-muted"></i></md-icon>
          </md-button>
        </div>
        <?php if (check_privilege('customers', 'create')) { ?> 
          <md-button ng-click="Create()" class="md-icon-button" aria-label="New" ng-cloak>
            <md-tooltip md-direction="bottom"><?php echo lang('create') ?></md-tooltip>
            <md-icon><i class="ion-android-add-circle text-success"></i></md-icon>
          </md-button>
        <?php } ?>
      </div>
    </md-toolbar>
    <div ng-show="areasLoader" layout-align="center center" class="text-center" id="circular_loader" ng-cloak>
      <md-progress-circular md-mode="indeterminate" md-diameter="30"></md-progress-circular>
      <p style="font-size: 15px;margin-bottom: 5%;">
        <span><?php echo lang('please_wait') ?> <br>
          <small><strong><?php echo lang('loading') . ' ' . lang('bit/area') . '...' ?></strong></small></span>
      </p>
    </div>
    <md-content ng-show="!areasLoader" class="bg-white" ng-cloak>
      <md-table-container ng-show="areas.length > 0">
        <table md-table md-progress="promise">
          <thead md-head md-order="area_list.order">
            <tr md-row>
              <th md-column md-order-by="city"><span><?php echo lang('city'); ?></span></th>
              <th md-column md-order-by="bit"><span><?php echo lang('bit/area'); ?></span></th>
              <th md-column md-order-by="distributor_email"><span><?php echo lang('distributer_email'); ?></span></th>
              <th md-column md-order-by="day"><span><?php echo lang('day'); ?></span></th>
              <th md-column><span><?php echo lang('action'); ?></span></th>
            </tr>
          </thead>
          <tbody md-body>
            <tr class="select_row" md-row ng-repeat="area in areas | orderBy: area_list.order | limitTo: area_list.limit : (area_list.page -1) * area_list.limit | filter: area_search | filter: FilteredData" class="cursor" >
              <td md-cell>
                <strong><span ng-bind="area.city"></span></strong><br>
              </td>
              <td md-cell>
                <strong><span ng-bind="area.bit"></span></strong>
              </td>
              <td md-cell>
                <strong><span ng-bind="area.distributor_email"></span></strong>
              </td>
              <td md-cell>
                <strong><span ng-bind="area.day"></span></strong>
              </td>
              <td md-cell>
                <?php if (check_privilege('area', 'edit')) { ?> 
                  <md-icon ng-click="editArea(area)" md-menu-align-target class="ion-compose"></md-icon>
                <?php } ?>
                <?php if (check_privilege('area', 'delete')) { ?> 
                  <md-button ng-click="Delete(area.id)" class="md-icon-button md-primary" aria-label="Actions" ng-cloak>
                    <md-icon class="ion-trash-b"></md-icon>
                  </md-button>                    
                <?php } ?>
              </td>
            </tr>
          </tbody>
        </table>
      </md-table-container>
      <md-table-pagination ng-show="areas.length > 0" md-limit="area_list.limit" md-limit-options="limitOptions" md-page="area_list.page" md-total="{{areas.length}}"></md-table-pagination>
      <md-content ng-show="!areas.length && !areasLoader" class="md-padding no-item-data">
        <?php echo lang('notdata') ?></md-content>
    </md-content>
  </div>
  
  <md-sidenav class="md-sidenav-right md-whiteframe-4dp" md-component-id="Create" style="width: 450px;" ng-cloak>
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <md-button ng-click="close()" class="md-icon-button" aria-label="Close"> <i class="ion-android-arrow-forward"></i> </md-button>
        <h2 flex md-truncate><?php echo lang('create') ?></h2>
      </div>
    </md-toolbar>
    <md-content>
      <md-content layout-padding>
        <md-input-container class="md-block">
          <label><?php echo lang('city'); ?></label>
          <input name="city" ng-model="area.city" required>
        </md-input-container>
        <md-input-container class="md-block">
          <label><?php echo lang('bit/area') ?></label>
          <input name="bit" ng-model="area.bit" required>
        </md-input-container>
        <md-input-container class="md-block">
          <label><?php echo lang('distributer_email'); ?></label>
          <input name="distributor_email" ng-model="area.distributor_email" required minlength="10" maxlength="100" ng-pattern="/^.+@.+\..+$/">
        </md-input-container>  
        <md-input-container class="md-block">
          <label><?php echo lang('day'); ?></label>
          <input name="day" ng-model="area.day" ng-pattern ="/^[a-zA-Z\s]*$/">
        </md-input-container>
        </md-content>
      <custom-fields-vertical></custom-fields-vertical>
      <md-content class="layout-padding">
    </md-content>
      <md-content layout-padding>
        <section layout="row" layout-sm="column" layout-align="center center" layout-wrap>
          <md-button ng-click="AddArea()" class="md-raised md-primary btn-report block-button" ng-disabled="saving == true">
            <span ng-hide="saving == true"><?php echo lang('create'); ?></span>
            <md-progress-circular class="white" ng-show="saving == true" md-mode="indeterminate" md-diameter="20">
            </md-progress-circular>
          </md-button>
          <br/><br/><br/><br/>
        </section>
      </md-content>
    </md-content>
  </md-sidenav>
</div>

<script type="text/ng-template" id="update_area.html">
  <md-dialog aria-label="Payment">
    <md-toolbar class="toolbar-white">
      <div class="md-toolbar-tools">
        <h2><strong class="text-success"><?php echo lang('update').' '.lang('bit/area');?></strong></h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="close()">
          <md-icon class="ion-close-round" aria-label="Close dialog" style="color:black"></md-icon>
        </md-button>
      </div>
    </md-toolbar>
    <md-dialog-content style="max-width:800px;max-height:810px; ">
      <md-content class="bg-white">
        <md-list flex>
          <md-list-item>
            <md-input-container class="md-block full-width">
              <label><?php echo lang('city'); ?></label>
              <input ng-model="area.city" required>
            </md-input-container>
          </md-list-item>
          <md-list-item>
            <md-input-container class="md-block full-width">
              <label><?php echo lang('bit/area') ?></label>
              <input ng-model="area.bit" required>
            </md-input-container>
          </md-list-item>
          <md-list-item>
            <md-input-container class="md-block full-width">
              <label><?php echo lang('distributer_email'); ?></label>
              <input ng-model="area.distributor_email" required>
            </md-input-container>
          </md-list-item>
          <md-list-item>
            <md-input-container class="md-block full-width">
              <label><?php echo lang('day'); ?></label>
              <input ng-model="area.day">
            </md-input-container>
          </md-list-item>         
    
          <md-divider>
          </md-divider>
          <br><br>
          <md-button ng-click="UpdateArea(area.id)" class="template-button" ng-disabled="update == true">
            <span ng-hide="update == true"><?php echo lang('save');?></span>
            <md-progress-circular class="white" ng-show="update == true" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
          </md-button>
          <md-button ng-click="close()" class="">
            <span><?php echo lang('cancel');?></span>
          </md-button>
        </md-list>
      </md-content>
    </md-dialog-content>
  </md-dialog>
</script>
<script type="text/javascript">
var lang = {};
lang.new = '<?php echo lang('new') ?>';
lang.name = '<?php echo lang('name') ?>';
lang.add = '<?php echo lang('add') ?>';
lang.cancel = '<?php echo lang('cancel') ?>';
lang.save = '<?php echo lang('save') ?>';
lang.edit = '<?php echo lang('edit') ?>';

lang.doIt = "<?php echo lang('doIt')?>";
lang.cancel = "<?php echo lang('cancel')?>";
lang.attention = "<?php echo lang('attention')?>";
lang.delete_area = "<?php echo lang('areaattentiondetail')?>";
lang.delete_contact = "<?php echo lang('contactattentiondetail')?>";
</script>

<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script src="<?php echo base_url('assets/lib/highcharts/highcharts.js')?>"></script>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/area.js') ?>"></script>