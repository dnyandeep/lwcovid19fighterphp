<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<style type="text/css">
.progress-bar {
  background-color: rgb(34, 194, 129) !important;
}
.md-datepicker-button{
  display: none;
}
</style>
<div class="lw-body-content" ng-controller="Noorders_Controller">
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <div class="col-md-12" style="padding: 0px">
      <div class="panel-default">
        <div class="lw-invoice-summary"></div>
      </div>
      <md-toolbar class="toolbar-white">
        <div class="md-toolbar-tools">
          <h2 flex md-truncate class="text-bold"><?php echo lang('no_orders_list'); ?> <small>(<span ng-bind="noorders.length"></span>)</small><br>
          </h2>
          <div class="lw-external-search-in-table">
            <input ng-model="expense_search" class="search-table-external" id="search" name="search" type="text" placeholder="<?php echo lang('searchword')?>">
            <md-button class="md-icon-button" aria-label="Search" ng-cloak>
              <md-icon><i class="ion-search text-muted"></i></md-icon>
            </md-button>
          </div>
           <md-button ng-click="refreshTimeLogs()" class="md-icon-button" aria-label="Filter" ng-cloak>
          
          <md-tooltip md-direction="top"><?php echo lang('refresh').' '.lang('no_orders_list') ?></md-tooltip>
          <md-icon><i class="ion-ios-refresh text-muted"></i></md-icon>
        </md-button>
        </div>
      </md-toolbar>
      <md-content class="md-padding bg-white">
          <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs>
          <md-select placeholder="<?php echo lang('choisecustomer'); ?>" ng-model="customer" name="customer" style="min-width: 200px;" data-md-container-class="selectdemoSelectHeader">
            <md-select-header class="demo-select-header">
              <label style="display: none;"><?php echo lang('search').' '.lang('customer')?></label>
              <input ng-submit="search_customers(search_input)" ng-model="search_input" type="text" placeholder="<?php echo lang('search').' '.lang('customers')?>" class="demo-header-searchbox md-text" ng-keyup="search_customers(search_input)">
            </md-select-header>
            <md-optgroup label="customers">
              <md-option ng-value="customer.id" ng-repeat="customer in all_customers">
                <strong ng-bind="customer.name"></strong><br>
                <span class="blur">(<small ng-bind="customer.email"></small>)</span>
              </md-option>
            </md-optgroup>
          </md-select>
        </md-input-container>

        <md-input-container class="md-block" flex-gt-xs>
          <label><?php echo lang('salesperson'); ?></label>
            <md-select placeholder="<?php echo lang('salesperson'); ?>" ng-model="salesman_id">
              <md-option ng-value="name.id" ng-repeat="name in salesperson">{{name.staffname}}</md-option>
            </md-select>
        </md-input-container>

        <md-input-container class="md-block" flex-gt-xs>
          <label><?php echo lang('beat/area'); ?></label>
            <md-select placeholder="<?php echo lang('beat/area'); ?>" ng-model="beat">
              <md-option ng-value="beat.id" ng-repeat="beat in beats">{{beat.bit + " (" + beat.city + " )"}}</md-option>
            </md-select>
        </md-input-container>

        <md-input-container flex-gt-xs>
          <label><?php echo lang('from') ?></label>
          <md-datepicker name="from" ng-model="from" md-open-on-focus></md-datepicker>
        </md-input-container>

        <md-input-container flex-gt-xs>
          <label><?php echo lang('to') ?></label>
          <md-datepicker md-min-date="to" name="to" ng-model="to" md-open-on-focus></md-datepicker>
        </md-input-container>
            
            
            
        <md-button ng-click="viewReport()" class="md-raised md-primary btn-report" aria-label='New'>
          <span><?php echo lang('submit'); ?></span>
          <md-tooltip md-direction="bottom"><?php echo lang('submit') ?></md-tooltip>
        </md-button>
        </div>
        </md-content>
      <md-content style="padding-top: 0px;">
        <div ng-show="noordersLoader" layout-align="center center" class="text-center" id="circular_loader">
          <md-progress-circular md-mode="indeterminate" md-diameter="30"></md-progress-circular>
          <p style="font-size: 15px;margin-bottom: 5%;">
            <span><?php echo lang('please_wait') ?> <br>
            <small><strong><?php echo lang('loading'). ' '. lang('expenses').'...' ?></strong></small></span>
          </p>
        </div>
      <md-content ng-show="!noordersLoader" class="bg-white" ng-cloak> 
				<md-table-container ng-show="noorders.length > 0">
					<table md-table  md-progress="promise">
						<thead md-head md-order="noorder_list.order">
							<tr md-row>
								<th md-column md-order-by="date"><span><?php echo lang('date'); ?></span></th>
								<th md-column md-order-by="reference_no"><span><?php echo lang('invoice_no'); ?></span></th>
								<th md-column md-order-by="name"><span><?php echo lang('customer'); ?></span></th>
								<th md-column md-order-by="salesman"><span><?php echo lang('salesperson'); ?></span></th>
                <th md-column md-order-by="status"><span><?php echo lang('sale_status'); ?></span></th>
                <th md-column md-order-by="beat"><span><?php echo lang('beat'); ?></span></th>
                <th md-column md-order-by="noorder_reason"><span><?php echo lang('reason'); ?></span></th>
							</tr>
						</thead>
						<tbody md-body>
							<tr class="select_row" md-row ng-repeat="noorder in noorders | orderBy: noorder_list.order | limitTo: noorder_list.limit : (enoorder_list.page -1) * noorder_list.limit | filter: noorder_search | filter: FilteredData" class="cursor" ng-click="displayLocation(noorder.id)">
								<td md-cell>
									<strong><span ng-bind="noorder.date"></span></strong>
								</td>
                <td md-cell>
                  <strong><span ng-bind="noorder.reference_no"></span></strong>
                </td>
                <td md-cell>
                  <strong><span ng-bind="noorder.name"></span></strong>
                </td>
								<td md-cell>
									<strong><span ng-bind="noorder.salesman"></span></strong>
								</td>
                <td md-cell>
                  <strong><span ng-bind="getStatus(noorder.status)"></span></strong>
                </td>
                <td md-cell>
                  <strong><span ng-bind="noorder.beat"></span></strong>
                </td>
                <td md-cell>
                  <strong><span ng-bind="noorder.noorder_reason"></span></strong>
                </td>
							</tr>
						</tbody>
					</table>
				</md-table-container>
				<md-table-pagination ng-show="noorders.length > 0" md-limit="noorder_list.limit" md-limit-options="limitOptions" md-page="noorder_list.page" md-total="{{noorders.length}}" ></md-table-pagination>
				<md-content ng-show="!noorders.length" class="md-padding no-item-data"><?php echo lang('notdata') ?></md-content>	
			</md-content>
    </div>
  </div>
  <!-- <lw-sidebar ng-show="expensesLoader"></lw-sidebar> -->
  <md-sidenav class="md-sidenav-right md-whiteframe-4dp" md-component-id="ContentFilter"  ng-cloak style="width: 450px;">
    <md-toolbar class="md-theme-light" style="background:#262626">
      <div class="md-toolbar-tools">
        <md-button ng-click="close()" class="md-icon-button" aria-label="Close"> <i class="ion-android-arrow-forward"></i> </md-button>
        <md-truncate><?php echo lang('filter') ?></md-truncate>
      </div>
    </md-toolbar>
    <md-content layout-padding="" ng-cloak>
      <div ng-repeat="(prop, ignoredValue) in expenses[0]" ng-init="filter[prop]={}" ng-if="prop != 'id' && prop != 'title' && prop != 'prefix' && prop != 'longid' && prop != 'amount' && prop != 'staff' && prop != 'color' && prop != 'displayinvoice' && prop != 'date' && prop != 'category' && prop != 'billstatus' && prop != 'billable'">
        <div class="filter col-md-12">
          <h4 class="text-muted text-uppercase"><strong>{{prop}}</strong></h4>
          <hr>
          <div class="labelContainer" ng-repeat="opt in getOptionsFor(prop)">
            <md-checkbox id="{{[opt]}}" ng-model="filter[prop][opt]" aria-label="{{opt}}"><span class="text-uppercase">{{opt}}</span></md-checkbox>
          </div>
        </div>
      </div>
    </md-content>
  </md-sidenav>
</div>

<script type="text/ng-template" id="displayLocation.html">
  <md-dialog aria-label="Payment">
    <md-dialog-content style="max-width:800px;max-height:810px; ">
      <md-content class="bg-white">
        <md-list flex>
          <div id="map" style="width: 100%; height: 400px;"></div>

          <md-divider>
          </md-divider>
          <br><br>
        </md-list>
      </md-content>
    </md-dialog-content>
  </md-dialog>
  <div>
    
  </div>
</script>
<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCfaLjR2MOJOoSz6Wiel3aejSYBxqJBs7o'></script>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/noorder.js') ?>"></script>
<script type="text/javascript">


</script>