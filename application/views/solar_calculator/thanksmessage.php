
<?php include_once(APPPATH . 'views/inc/lw_solar_calculator.php'); ?>
<?php $appconfig = get_appconfig(); ?>

  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-10" style="padding : 0px 10px 0px 200px">  
  <div class="lw-body-content" ng-controller="Solar_Calculator_Controller">
    
    <div layout="row">
      <md-content class="information-section-hide" flex style="text-align: center;">  
        <div class="panel-heading"><img src="<?php echo base_url('uploads/lw_settings/'.$rebrand['nav_logo'].''); ?>" alt="logo" class="logo-img nav-logo"><?php echo $settings['company'] ?></span>
        </div>  
      </md-content>
    </div>         
    <section layout="row" flex ng-cloak>
      <md-content class="bg-white information-section-hide" flex >        
        <md-content class="md-padding bg-white">
          <?php if($calculationtype=='unit'){?>
            <h4><?php echo lang('thankyou');?></h4>
          <?php }else if($calculationtype=='fileupload'){?>
            <h4 style="text-align: center;"><?php echo lang('thankyou_message');?> <b><?php echo lang('app_company_name');?></b></h4>
            <br>
            <h4 style="color: #97B512;text-align: center;"><?php echo lang('message_after_electricity_bill_upload');?></h4>
            <h4 style="color: #97B512;text-align: center;"><?php echo lang('greatdaywish');?></h4>
          <?php }else if($calculationtype=='sessionexpire'){?>
            <h4 style="text-align: center;"><?php echo lang('sessionexpired');?></h4>
          <?php }?>
        </md-content>      
      </md-content>
    </section> 
  </div>
</div>
<script>
    
    var lang ={};
    lang.doIt = "<?php echo lang('doIt')?>";
    lang.cancel = "<?php echo lang('cancel')?>";
    lang.attention = "<?php echo lang('attention')?>";
    lang.delete_customer = "<?php echo lang('customerattentiondetail')?>";
    lang.delete_contact = "<?php echo lang('contactattentiondetail')?>";


</script>
<?php include_once( APPPATH . 'views/inc/footer.php' );?>
<script src="<?php echo base_url('assets/lib/chartjs/dist/Chart.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/solar_calculator.js') ?>"></script>
