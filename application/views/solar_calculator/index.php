
<?php include_once(APPPATH . 'views/inc/lw_solar_calculator.php'); ?>
<?php $appconfig = get_appconfig(); ?>

  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-10" style="padding : 0px 10px 0px 200px">  
  <div class="lw-body-content" ng-controller="Solar_Calculator_Controller">
    <?php echo form_open_multipart('solar_calculator/getOtp'); ?>
    <div layout="row">
      <md-content class="information-section-hide" flex style="text-align: center;">  
        <div class="panel-heading"><img src="<?php echo base_url('uploads/lw_settings/'.$rebrand['nav_logo'].''); ?>" alt="logo" class="logo-img nav-logo"><?php echo $settings['company'] ?></span>
        </div>  
      </md-content>
    </div>         
    <section layout="row" flex ng-cloak ng-show="<?= empty($this->session->userdata( 'generatedotp' ));?>">
      <md-content class="bg-white information-section-hide" flex >        
        <md-content class="md-padding bg-white">
          <div class="col-md-12">
            <h4 style="text-align: center;">Mobile Number Verification</h4>
          </div>
          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/phone.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('phone') ?></label>
                <input type="text" name="phone" required value="<?= (isset($_POST['phone']) ? $_POST['phone'] : ''); ?>">
              </md-input-container>
            </section>
          </div>  

          <div class="col-md-3">
            <section layout="row" layout-sm="column" layout-align="center center" layout-wrap><br>
              <md-button type="submit" class="template-button block-button">
                <span ng-hide="saving == true"><?php echo lang('get_otp');?></span>
                <md-progress-circular class="white" ng-show="saving == true" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
              </md-button>
              <br><br>
            </section>
          </div>  
        </md-content>      
      </md-content>
    </section> 
    <?php echo form_close(); ?>

    <?php echo form_open_multipart('solar_calculator/validateotp'); ?>
    <section layout="row" flex ng-cloak ng-show="<?= !empty($this->session->userdata( 'generatedotp' ));?>">
      <md-content class="bg-white information-section-hide" flex >        
        <md-content class="md-padding bg-white">
          <div class="col-md-12">
            <h4 style="text-align: center;">Please enter the OTP sent to your mobile number.</h4>
          </div>
          <div class="col-md-3">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/phone.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('OTP') ?></label>
                <input type="hidden" name="contact_no" value="<?= $this->session->userdata( 'phone' );?>">
                <input type="text" name="otp" required>
              </md-input-container>
            </section>
          </div>  

          <div class="col-md-3">
            <section layout="row" layout-sm="column" layout-align="center center" layout-wrap><br>
              <md-button type="submit" class="template-button block-button">
                <span ng-hide="saving == true"><?php echo lang('submit');?></span>
                <md-progress-circular class="white" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
              </md-button>
              <br><br>
            </section>
          </div>  
          <?php echo form_close(); ?>

          <?php echo form_open_multipart('solar_calculator/getOtp'); ?>
          <div class="col-md-3">
            <input type="hidden" name="phone" value="<?= $this->session->userdata( 'phone' );?>">
            <input type="hidden" name="resendotpcnt" value="<?= $this->session->userdata( 'resendotpcnt' );?>">
            <section layout="row" layout-sm="column" layout-align="center center" layout-wrap ng-show="<?php echo $this->session->userdata( 'resendotpcnt' )<= 3;?>"><br>
              <md-button type="submit" class="template-button block-button" disabled="disabled" id="resendotp">
                <span ng-hide="saving == true"><?php echo lang('resendotp');?></span>
                <md-progress-circular class="white" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
              </md-button>
              <br><br>
            </section>
          </div>
          <?php echo form_close(); ?>
        </md-content>      
      </md-content>
    </section>
  </div>
  
</div>
<script>
    
    var lang ={};
    lang.doIt = "<?php echo lang('doIt')?>";
    lang.cancel = "<?php echo lang('cancel')?>";
    lang.attention = "<?php echo lang('attention')?>";
    lang.delete_customer = "<?php echo lang('customerattentiondetail')?>";
    lang.delete_contact = "<?php echo lang('contactattentiondetail')?>";


</script>
<?php include_once( APPPATH . 'views/inc/footer.php' );?>
<script src="<?php echo base_url('assets/lib/chartjs/dist/Chart.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/solar_calculator.js') ?>"></script>


