<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> <!-- Encoding utf8 chartset for the pdf -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('assets/lib/bootstrap/dist/css/bootstrap.min.css'); ?>'> <!-- Bootstrap CSS file link -->
	<style>
		.list-group-item.active,
		.list-group-item.active:focus,
		.list-group-item.active:hover {
			z-index: 2;
			color: #fff;
			background-color: #555;
			border-color: #555;
		}
		.page-header.row {
			margin: 30px 0 10px 0 !important;
		}
		.page-header .logo {
			padding-right: 0 !important;
			padding-left: 0 !important;
		}
		.panel {
			box-shadow: 0 1px 1px rgb(255, 255, 255) !important;
		}/*Custom CSS write/paste here*/

		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
		{
			padding: 5px;

		}

		#watermark {
                position: fixed;

                /** 
                    Set a position in the page for your image
                    This should center it vertically
                **/
                bottom:   10cm;
                left:     5.5cm;

                /** Change image dimensions**/
                width:    8cm;
                height:   8cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;
            }
	</style>
</head>
<?php 

// Invoice logo
$logo =  file_exists(FCPATH.'uploads/lw_settings/'.$settings['app_logo']);
if(file_exists(FCPATH.'uploads/lw_settings/'.$settings['app_logo'])) {
	$logo = FCPATH.'uploads/lw_settings/'.$settings['app_logo'];
} else {
	$logo = FCPATH.'uploads/lw_settings/'.$settings['logo']; // Use app logo, if invoice logo is not found
}
?>

<body>
	<div class="container">
		
			<div class="col-md-12 nav panel" style="padding-bottom: 20px">

				<div style="padding: 0;padding: 0;border: 1px solid #90909045;border-radius: 4px;margin-bottom: 20px;">
						<table class="table" style="page-break-inside: avoid;">
							
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center;">SOLAR PV GRID TIE SYSTEM</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center;">APPROX UNIT GENERATION AND PAYBACK PERIOD</td>
								</tr>
								<tr>
									<th>NAME</th>
									<td><?= $lead_detail['name'].' '.$lead_detail['last_name']; ?></td>
								</tr>
								<tr>
									<th>STATE</th>
									<td><?= $state; ?></td>
								</tr>
								<tr>
									<th>DISTRICT</th>
									<td><?= $district; ?></td>
								</tr>	
								<tr>
									<th>CATEGORY</th>
									<td><?= $category; ?></td>
								</tr>
								<tr>
									<th>CONSUMER NUMBER</th>
									<td><?= $lead_detail['consumer_number']; ?></td>
								</tr>
								<tr>
									<th>SYSTEM PROPOSED(KW)</th>
									<td><?= number_format((float)$requiredKW, 2, '.', ''); ?></td>
								</tr>
								<tr>
									<th>CONSUMED UNITS</th>
									<td><?= $lead_detail['units'].' ' .$lead_detail['unit_type']; ?></td>
								</tr>
								<tr>
									<th>SYSTEM GENERATED UNITS/YEAR</th>
									<td><?= number_format((float)$systemGeneratedUnits, 2, '.', ''); ?></td>
								</tr>
								<tr>
									<th>APPROX. UNIT RATE</th>
									<td><?= $lead_detail['approx_unit_rate']; ?></td>
								</tr>
								<tr>
									<th>TOTAL REVENUE GENERATED/YEAR</th>
									<td><?= number_format((float)$totalRevenueGenerated, 2, '.', ''); ?></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center;">**APPROX ELECTRICITY UNIT RATE CONSIDERED BASED ON LAST MONTH BILL</td>
								</tr>
							</tbody>
						</table>
					</div>

					
					<div style="padding: 0;padding: 0;border: 1px solid #90909045;border-radius: 4px;margin-bottom: 20px;">
						
							<strong>
								<?php echo 'SYSTEM COST AND APPROX. PAYBACK YEARS'; ?>
							</strong>
						
						<table class="table" style="page-break-inside: avoid;">
							
							<tbody>
								<tr>
									<th></th>
									<th>MONO</th>
									<th>POLY A+</th>
									<th>POLY A</th>
								</tr>
								<tr>
									<th>SYSTEM COST</th>
									<td><?= number_format((float)$monoSystemCost, 2, '.', '');?></td>
									<td><?= number_format((float)$polyAplusCost, 2, '.', '');?></td>
									<td><?= number_format((float)$polyACost, 2, '.', '');?></td>
								</tr>
								<tr>
									<th>APPROX. PAYBACK YEARS</th>
									<td><?= number_format((float)$monoYears, 2, '.', '');?></td>
									<td><?= number_format((float)$polyAPlusYears, 2, '.', '');?></td>
									<td><?= number_format((float)$polyAYear, 2, '.', '');?></td>
								</tr>				
							</tbody>
						</table>
					</div>


					<div style="padding: 0;padding: 0;border: 1px solid #90909045;border-radius: 4px;margin-bottom: 20px;">
						
							<strong>
								<?php echo 'RATE CARD'; ?>
							</strong>
						
						<table class="table" style="page-break-inside: avoid;">
							<thead>
								<tr>
									<th>KW</th>
									<th>MONO RATE/KW</th>
									<th>POLY A+ RATE/KW</th>
									<th>POLY A RATE/KW</th>
								</tr>
							</thead>
							
							<tbody>
									
								<?php $present =0;?> 
								<?php for($i=0;$i<count($ratecard);$i++)
                   				{	
                   					if($i==0)
                   					{
                   						if(($requiredKW < $ratecard[$i]['lower_kw']) || ($requiredKW >= $ratecard[$i]['lower_kw'] && $requiredKW <= $ratecard[$i]['higher_kw']))
                   						{ 
                   							$present = 1;
                   						}
                   					}
                   					else if($i==(count($ratecard)-1))
                   					{
                   						if(($requiredKW >= $ratecard[$i]['lower_kw'] && $requiredKW <= $ratecard[$i]['higher_kw']) || ($requiredKW < $ratecard[$i]['higher_kw']))
                   						{
                   							$present = 1;
                   						}
                   					}
                   					else 
                   					{
                   						if(($requiredKW >= $ratecard[$i]['lower_kw'] && $requiredKW <= $ratecard[$i]['higher_kw']))
                   						{
                   							$present = 1;
                   						}
                   					}?>
                   					<?php if($present==1)
                   					{?>
                   						<tr style="background-color: #bfff00;">
												<td><?= $ratecard[$i]['higher_kw']=='0.00' ? $ratecard[$i]['lower_kw'] .'KW TO Above' : $ratecard[$i]['lower_kw'] .'KW TO '.$ratecard[$i]['higher_kw']. 'KW';?></td>
												<td><?= $ratecard[$i]['mono_rate_per_kw'];?></td>
												<td><?= $ratecard[$i]['poly_a_plus_rate_per_kw'];?></td>
												<td><?= $ratecard[$i]['poly_a_rate_per_kw'];?></td>
											</tr>

                   					<?php }else{ ?>
                   						<tr>
												<td><?= $ratecard[$i]['higher_kw']=='0.00' ? $ratecard[$i]['lower_kw'] .'KW TO Above' : $ratecard[$i]['lower_kw'] .'KW TO '.$ratecard[$i]['higher_kw']. 'KW';?></td>
												<td><?= $ratecard[$i]['mono_rate_per_kw'];?></td>
												<td><?= $ratecard[$i]['poly_a_plus_rate_per_kw'];?></td>
												<td><?= $ratecard[$i]['poly_a_rate_per_kw'];?></td>
											</tr>

                   					<?php }
                   					
                   					$present=0;
                   				}	?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</body>
</html>
