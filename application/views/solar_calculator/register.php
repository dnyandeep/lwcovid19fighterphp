
<?php include_once(APPPATH . 'views/inc/lw_solar_calculator.php'); ?>
<?php $appconfig = get_appconfig(); ?>
<style type="text/css">
  .req { 
            font-size: 90%; 
            font-style: italic; 
            color: red; 
        } 
</style>
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-10" style="padding : 0px 10px 0px 200px">  
  <div class="lw-body-content" ng-controller="Solar_Calculator_Controller">
    <?php echo form_open_multipart('solar_calculator/create',array("name"=>"myForm")); ?>
    <div layout="row">
      <md-content class="information-section-hide" flex style="text-align: center;">  
        <div class="panel-heading"><img src="<?php echo base_url('uploads/lw_settings/'.$rebrand['nav_logo'].''); ?>" alt="logo" class="logo-img nav-logo"><?php echo $settings['company'] ?></span>
        </div>  
      </md-content>
    </div>         
    <section layout="row" flex ng-cloak>
      <md-content class="bg-white information-section-hide" flex >        
        <md-content class="md-padding bg-white">
          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/phone.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <strong><?php echo lang('phone') ?></strong>
                <input type="hidden" name="phone" value="<?= $phone;?>">
                <span><?= $phone;?></span>
              </md-input-container>
            </section>
          </div>  

          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('first_name') ?></label>
                <input type="text" ng-model="name" name="name" ng-required ="true" value="<?= (isset($_POST['name']) ? $_POST['name'] :  '') ;?>">
                <span ng-show="myForm.name.$invalid" class="req"> 
                  This is the required field. 
                </span>
              </md-input-container>
              
            </section>
          </div>
          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('middle_name') ?></label>
                <input type="text" name="middle_name" value="<?= (isset($_POST['middle_name']) ? $_POST['middle_name'] :  '') ;?>">
              </md-input-container>
            </section>
          </div>
          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('last_name') ?></label>
                <input type="text" ng-model="leads.last_name" name="last_name" required value="<?= (isset($_POST['last_name']) ? $_POST['last_name'] :  '') ;?>">
                <span ng-show="myForm.last_name.$invalid" class="req"> 
                  This is the required field. 
                </span>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/email.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('email') ?></label>
                <input type="email" name="email" value="<?= (isset($_POST['email']) ? $_POST['email'] :  '') ;?>">
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/address.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('address') ?></label>
                <input type="text" ng-model="leads.address" name="address" required value="<?= (isset($_POST['address']) ? $_POST['address'] : ''); ?>">
                <span ng-show="myForm.address.$invalid" class="req"> 
                  This is the required field. 
                </span>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/pincode.png');?>" style="width: 24px; height: 24px;">
              </span>                  
                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('state'); ?></label>
                <md-select placeholder="<?php echo lang('state'); ?>" ng-model="leads.state_id" required name="state_id" ng-change="getDistricts(leads.state_id)" style="min-width: 200px;">
                  <md-option ng-value="state.id" ng-repeat="state in states">{{state.name}}</md-option>
                </md-select>
                <span ng-show="myForm.state_id.$invalid" class="req"> 
                  This is the required field. 
                </span>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/pincode.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('district'); ?></label>
                <md-select placeholder="<?php echo lang('district'); ?>" ng-model="leads.district_id" name="district_id" style="min-width: 200px;" required>
                  <md-option ng-value="district.id" ng-repeat="district in districts">{{district.name}}</md-option>
                </md-select>
                <span ng-show="myForm.district_id.$invalid" class="req"> 
                  This is the required field. 
                </span>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/city.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('city') ?></label>
                <input type="text" ng-model="leads.city" name="city" required value="<?= (isset($_POST['city']) ? $_POST['city'] : ''); ?>">
                <span ng-show="myForm.city.$invalid" class="req"> 
                  This is the required field. 
                </span>
              </md-input-container>
            </section>
          </div>        

          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/pincode.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('pincode') ?></label>
                <input type="text" ng-model="pincode" name="pincode" required value="<?= (isset($_POST['pincode']) ? $_POST['pincode'] : ''); ?>">
                <span ng-show="myForm.pincode.$invalid" class="req"> 
                  This is the required field. 
                </span>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6">            
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span>                  
                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('customer_category'); ?></label>
                <md-select placeholder="<?php echo lang('customer_category'); ?>" ng-model="leads.cust_category_id" name="cust_category_id" ng-change="getCustSubcategories(leads.cust_category_id)" 
                        style="min-width: 200px;">
                <md-option ng-value="category.id" ng-repeat="category in cust_categories">{{category.name}}</md-option>
                </md-select>
              </md-input-container>
            </section>
          </div> 

          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span>                  
                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('customer_subcategory'); ?></label>
                <md-select placeholder="<?php echo lang('customer_subcategory'); ?>" ng-model="leads.cust_subcategory_id"  name="cust_subcategory_id" 
                        style="min-width: 200px;">
                  <md-option ng-value="subcategory.id" ng-repeat="subcategory in cust_subcategories">{{subcategory.name}}</md-option>
                </md-select>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6">               
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/pincode.png');?>" style="width: 24px; height: 24px;">
              </span>                  
                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('product'); ?></label>
                <md-select placeholder="<?php echo lang('product'); ?>" name="product_id"
                      ng-model="leads.product"  style="min-width: 200px;">
                  <md-option ng-value="product.id" ng-repeat="product in products">{{product.productname}}</md-option>
                </md-select>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span> 
              <md-input-container class="md-block" style="width:100%;">
                <input type="hidden" name="calculationtype" value="{{calculationtype}}">
                <md-radio-group ng-model="leads.calculation_type" name="calculation_type" id="required" required>
                  <md-radio-button style="display: inline !important;" value="unit" ng-selected="true" ng-click="ShowFields('unit')">
                        <?php echo lang('Enter Units'); ?>
                  </md-radio-button>
                  <md-radio-button style="display: inline !important;" ng-model="calculationtype" value="fileupload" ng-click="ShowFields('fileupload')">
                        <?php echo lang('Upload Electricity Bill'); ?>
                  </md-radio-button>
                  <span>*</span>
                </md-radio-group> 
                <br> 
                <span ng-show="myForm.calculation_type.$invalid" class="req"> 
                  This is the required field. 
                </span>
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6" ng-show = "type == 'unit'">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('consumer_no') ?></label>
                <input type="text" name="consumer_number" value="<?= (isset($_POST['consumer_number']) ? $_POST['consumer_number'] : ''); ?>">
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6" ng-show = "type == 'unit'">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span>                  
              <md-input-container class="md-block" style="width:100%;">
                <label><?php echo lang('supplier') ?></label>
                <input type="text" name="supplier" value="<?= (isset($_POST['supplier']) ? $_POST['supplier'] : ''); ?>">
              </md-input-container>
            </section>
          </div>

          <div class="col-md-6"  ng-show = "type == 'unit'">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span>
              <md-input-container class="md-block" style="width:100%;">
                  <input type="hidden" name="unittype" value="{{unittype}}">
                  <label><?php echo lang('units') ?></label>
                  <input type="text" ng-model="leads.units" id="units" name="units" ng-required="type == 'unit'">
                  <span ng-show="myForm.units.$invalid" class="req"> 
                    This is the required field. 
                  </span>
              </md-input-container>
            </section>
          </div>
          <div class="col-md-6" ng-show = "type == 'unit'">
            <section layout="row">
                  <span style="margin : 20px 5px;">
                    <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
                  </span>                  
                  <md-input-container class="md-block" style="width:100%;">
                    <label><?php echo lang('approx_unit_rate') ?></label>
                    <input type="text" ng-model="leads.approx_unit_rate" name="approx_unit_rate" value="<?= (isset($_POST['approx_unit_rate']) ? $_POST['approx_unit_rate'] : ''); ?>" ng-required="type == 'unit'">
                    <span ng-show="myForm.approx_unit_rate.$invalid" class="req"> 
                      This is the required field. 
                    </span>
                  </md-input-container>
                </section> 
          </div>
          <div class="col-md-6" ng-show = "type == 'unit'">
            <section layout="row">
              <span style="margin : 20px 5px;">
                <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
              </span>
                <md-input-container class="md-block" style="width:100%;">
                  <md-radio-group name="unit_type" ng-model="leads.unit_type" ng-required="type == 'unit'">
                    <md-radio-button style="display: inline !important;" value="Monthly" ng-selected="true" ng-click="ShowMonth('Monthly')">
                      Monthly
                    </md-radio-button>
                    <md-radio-button style="display: inline !important;" value="Yearly"  ng-click="ShowMonth('Yearly')">
                      Yearly                            
                    </md-radio-button>
                    <span ng-show="myForm.unit_type.$invalid" class="req"> 
                    This is the required field. 
                  </span>
                  </md-radio-group>
                  
                </md-input-container>
                <md-input-container class="md-block" ng-show ="unittype=='Monthly'" style="width: 100%;">
                  <label><?php echo lang('months') ?></label>
                  <input type="number" ng-model="leads.months" name="months"  value="<?= (isset($_POST['months']) ? $_POST['months'] : ''); ?>" ng-required="unittype=='Monthly'">
                  
                </md-input-container>
                <span ng-show="myForm.months.$invalid" class="req"> 
                    This is the required field. 
                  </span>
                
            </section>
          </div>        

          

          <div class="col-md-6" ng-show = "type == 'fileupload'">
            <section layout="row">
                <span style="margin : 20px 5px;">
                  <img class="png-icon" src="<?php echo base_url('uploads/icons/user.png');?>" style="width: 24px; height: 24px;">
                </span>                  
                <md-input-container class="md-block" style="width:100%;">
                  <div class="file-upload">
                    <div class="file-select">
                      <div class="file-select-button" id="fileName"><span class="mdi mdi-accounts-list-alt"></span>
                        <?php echo lang('attachment') ?>
                      </div>
                      <div class="file-select-name" id="noFile">
                        <?php echo lang('notchoise') ?>
                      </div>
                        
                      <input type="file" ng-model="electricity_bill" name="electricity_bill" id="chooseFile" accept=".png,.jpeg,.pdf" file-model="electricity_bill" ng-model="leads.electricity_bill" ng-required="type == 'fileupload'">            
                    </div>
                  </div>
                  <span ng-show="myForm.electricity_bill.$error.required" class="req"> 
                    This is the required field. 
                  </span>
                </md-input-container>
              </section>
          </div>

          <div class="col-md-6">
            <section layout="row" layout-sm="column" layout-align="center center" layout-wrap><br>
              <md-button type="submit" class="template-button block-button" ng-disabled="myForm.$invalid">
                <span><?php echo lang('save');?></span>
                <md-progress-circular class="white" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
              </md-button>
              <br><br>
            </section>
          </div>  
        </md-content>      
      </md-content>
    </section>   
  </div>
  <?php echo form_close(); ?>
</div>
<script>
    
    var lang ={};
    lang.doIt = "<?php echo lang('doIt')?>";
    lang.cancel = "<?php echo lang('cancel')?>";
    lang.attention = "<?php echo lang('attention')?>";
    lang.delete_customer = "<?php echo lang('customerattentiondetail')?>";
    lang.delete_contact = "<?php echo lang('contactattentiondetail')?>";


</script>
<?php include_once( APPPATH . 'views/inc/footer.php' );?>
<script src="<?php echo base_url('assets/lib/chartjs/dist/Chart.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/solar_calculator.js') ?>"></script>
