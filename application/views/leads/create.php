<?php include_once( APPPATH . 'views/inc/lw_data_table_header.php' ); ?>
<?php $appconfig = get_appconfig(); ?>
<style type="text/css">
  .md-datepicker-button{
    display: none;
  }
</style>
<div class="lw-body-content" ng-controller="LeadCreate_Controller">
  <div class="main-content container-fluid col-xs-12 col-md-12 col-lg-12">
    <form name="createLead" novalidate>
    <md-toolbar class="toolbar-white" ng-cloak>
      <div class="md-toolbar-tools">
        <md-button class="md-icon-button" aria-label="Invoice" ng-disabled="true">
          <md-icon><i class="ico-lw-invoices text-muted"></i></md-icon>
        </md-button>
        <h2 flex md-truncate><?php echo lang('newlead') ?></h2>

        <md-switch ng-model="current_product" aria-label="Recurring"> <strong class="text-muted"><?php echo lang('existing_product') ?></strong> </md-switch>

        <md-button ng-href="<?php echo base_url('leads')?>" class="md-icon-button" aria-label="Save">
          <md-tooltip md-direction="bottom"><?php echo lang('cancel') ?></md-tooltip>
          <md-icon><i class="ion-close-circled text-danger"></i></md-icon>
        </md-button>
        <md-button type="submit" ng-click="saveAll()" class="md-icon-button" aria-label="Save">
          <md-progress-circular ng-show="savingInvoice == true" md-mode="indeterminate" md-diameter="20"></md-progress-circular>
          <md-tooltip ng-hide="savingInvoice == true" md-direction="bottom"><?php echo lang('save') ?></md-tooltip>
          <md-icon ng-hide="savingInvoice == true"><i class="ion-checkmark-circled text-success"></i></md-icon>
        </md-button>
      </div>
    </md-toolbar>
    <div ng-show="invoiceLoader" layout-align="center" class="text-center" id="circular_loader">
        <md-progress-circular md-mode="indeterminate" md-diameter="30"></md-progress-circular>
        <p style="font-size: 15px;margin-bottom: 5%;">
         <span>
            <?php echo lang('please_wait') ?> <br>
           <small><strong><?php echo lang('loading').'...' ?></strong></small>
         </span>
       </p>
    </div>
    <md-content ng-show="!invoiceLoader" class="bg-white" layout-padding ng-cloak>
      <div layout-gt-xs="row">
        <md-input-container class="md-block" flex-gt-xs class="col-md-3">
          <label><?php echo lang('company'); ?></label>
          <input name="company" ng-model="company" required>
          <span style="color: red;" ng-show="createLead.company.$error.required">This field is required.</span>
        </md-input-container>

        <md-input-container class="md-block" flex-gt-xs class="col-md-3">
          <label><?php echo lang('first_name'); ?></label>
          <input required name="name" ng-model="name">
          <span style="color: red;" ng-show="createLead.name.$error.required">This field is required.</span>
        </md-input-container>

        <md-input-container class="md-block" flex-gt-xs class="col-md-3">
          <label><?php echo lang('middle_name'); ?></label>
          <input name="middle_name" ng-model="middle_name">
        </md-input-container>
        
        <md-input-container class="md-block" flex-gt-xs class="col-md-3">
          <label><?php echo lang('last_name'); ?></label>
          <input name="last_name" ng-model="last_name" required>
          <span style="color: red;" ng-show="createLead.last_name.$error.required">This field is required.</span>
        </md-input-container>
      </div>

      <div layout-gt-xs="row">     
        <md-input-container class="md-block" flex-gt-xs class="col-md-3">
          <label><?php echo lang('phone'); ?></label>
          <input name="phone" ng-model="phone" required ng-pattern="/^[7-9][0-9]{9}$/">
          <span style="color: red;" ng-show="createLead.phone.$error.pattern">Please enter valid number!</span>
          <span style="color: red;" ng-show="createLead.phone.$error.required">This field is required.</span>
        </md-input-container>

        <md-input-container class="md-block" flex-gt-xs class="col-md-3">
          <label><?php echo lang('email'); ?></label>
          <input type="email" ng-model="email" minlength="10" maxlength="100" ng-pattern="/^.+@.+\..+$/" name="email">
        </md-input-container>

        <md-input-container class="md-block" flex-gt-xs class="col-md-3">
          <label><?php echo lang('state'); ?></label>
          <md-select placeholder="<?php echo lang('state'); ?>" ng-model="state_id" name="state_id" style="min-width: 200px;" ng-change="getDistricts(state_id)" required>
            <md-option ng-value="state.id" ng-repeat="state in states">{{state.name}}</md-option>
          </md-select>
          <span style="color: red;" ng-show="createLead.state_id.$error.required">This field is required.</span>
        </md-input-container>

        <md-input-container class="md-block" flex-gt-xs class="col-md-3">
          <label><?php echo lang('district'); ?></label>
          <md-select placeholder="<?php echo lang('district'); ?>" ng-model="district_id" name="district_id" style="min-width: 200px;" required>
            <md-option ng-value="district.id" ng-repeat="district in districts">{{district.name}}</md-option>
          </md-select>
          <span style="color: red;" ng-show="createLead.district_id.$error.required">This field is required.</span>
        </md-input-container>
      </div>

      <div layout-gt-xs="row">
        <div class="col-md-6">
          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs class="col-md-3">
              <label><?php echo lang('city'); ?></label>
              <input ng-model="city" required name="city">
              <span style="color: red;" ng-show="createLead.city.$error.required">This field is required.</span>
            </md-input-container>
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs class="col-md-3">
              <label><?php echo lang('pincode'); ?></label>
              <input ng-model="pincode" name="pincode">
            </md-input-container>
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs class="col-md-3">
              <label><?php echo lang('address'); ?></label>
              <input name="address" ng-model="address" required>
              <span style="color: red;" ng-show="createLead.address.$error.required">This field is required.</span>
            </md-input-container>
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs class="col-md-3">
              <label><?php echo lang('business_start_date') ?></label>
              <md-datepicker name="business_start_date" ng-model="business_start_date" md-open-on-focus></md-datepicker>
            </md-input-container>      
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs>
              <label><?php echo lang('product'); ?></label>
              <md-select placeholder="<?php echo lang('product'); ?>" ng-model="product_id" name="product_id" style="min-width: 200px;" required>
                <md-option ng-value="product.product_id" ng-repeat="product in products">{{product.name}}</md-option>
              </md-select>
              <span style="color: red;" ng-show="createLead.product_id.$error.required">This field is required.</span>
            </md-input-container> 
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs>
              <label><?php echo lang('rating'); ?></label>
              <input name="rating" ng-model="rating" required pattern="^(?:[1-9]|0[1-9]|10)$">
              <span style="color: red;" ng-show="createLead.rating.$error.required">This field is required.</span>
              <span style="color: red;" ng-show="createLead.rating.$error.pattern">Please enter value between 1 to 10.</span>
            </md-input-container>
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs>
              <label><?php echo lang('demo_incharge'); ?></label>
              <input name="demo_incharge" ng-model="demo_incharge" required>
              <span style="color: red;" ng-show="createLead.demo_incharge.$error.required">This field is required.</span>
            </md-input-container>
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs>
              <label><?php echo lang('next_meeting_date') ?></label>
              <md-datepicker name="next_meeting_date" ng-model="next_meeting_date" md-open-on-focus></md-datepicker>
            </md-input-container>
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs>
              <label><?php echo lang('emp_capacity'); ?></label>
              <input name="capacity" ng-model="capacity" pattern="^[0-9]+$">
              <span style="color: red;" ng-show="createLead.capacity.$error.pattern">Please enter only digits</span>
            </md-input-container>
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs>
              <label><?php echo lang('no_of_kitchens'); ?></label>
              <input name="no_of_kitchens" ng-model="no_of_kitchens" pattern="^[0-9]+$">
              <span style="color: red;" ng-show="createLead.no_of_kitchens.$error.pattern">Please enter only digits</span>
            </md-input-container>
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs>
              <label><?php echo lang('no_of_tables'); ?></label>
              <input name="no_of_tables" ng-model="no_of_tables" pattern="^[0-9]+$">
              <span style="color: red;" ng-show="createLead.no_of_tables.$error.pattern">Please enter only digits</span>
            </md-input-container>
          </div>

          <div class="col-md-6">
            <md-input-container class="md-block" flex-gt-xs>
              <label><?php echo lang('no_of_menus'); ?></label>
              <input name="no_of_menus" ng-model="no_of_menus" pattern="^[0-9]+$">
              <span style="color: red;" ng-show="createLead.no_of_menus.$error.pattern">Please enter only digits</span>
            </md-input-container>
          </div>

          <div ng-show="current_product">
            <div class="col-md-6">
              <md-input-container class="md-block" flex-gt-xs>
                <label><?php echo lang('existing_product_name'); ?></label>
                <input name="current_product_name" ng-model="current_product_name">
              </md-input-container>
            </div>

            <div class="col-md-6">
              <md-input-container class="md-block" flex-gt-xs>
                <label><?php echo lang('installed_date') ?></label>
                <md-datepicker name="installed_date" ng-model="installed_date" md-open-on-focus></md-datepicker>
              </md-input-container>
            </div>

            <div class="col-md-6">
              <md-input-container class="md-block" flex-gt-xs>
                <label><?php echo lang('existing_product_company'); ?></label>
                <input name="product_company" ng-model="product_company">
              </md-input-container>
            </div>

            <div class="col-md-6">
              <md-input-container class="md-block" flex-gt-xs>
                <label><?php echo lang('existing_product_price'); ?></label>
                <input name="product_price" ng-model="product_price">
              </md-input-container>
            </div>

          </div>
        </div>

        <div class="col-md-6">
          <div class="col-md-8">
            <md-input-container class="md-block" flex-gt-xs class="col-md-6">
              <label><?php echo lang('location'); ?></label>
              <input name="address" id="us3-address">
            </md-input-container>

            <md-input-container class="md-block" flex-gt-xs class="col-md-6">
              <div id="us3" style="width: 460px; height: 400px;"></div>
            </md-input-container>

            <md-input-container class="md-block" flex-gt-xs class="col-md-3">
              <label><?php echo lang('latitude'); ?></label>
              <input name="latitude" required id="us3-lat" ng-model="latitude">
              <span style="color: red;" ng-show="createLead.latitude.$error.required">This field is required.</span>
            </md-input-container>

            <md-input-container class="md-block" flex-gt-xs class="col-md-3">
              <label><?php echo lang('longitude'); ?></label>
              <input name="longitude" required id="us3-lon" ng-model="longitude">
              <span style="color: red;" ng-show="createLead.longitude.$error.required">This field is required.</span>
            </md-input-container>
          </div>
        </div>


      </div>     
      
    </md-content>
  </form>
  </div>
</div>

<?php include_once( APPPATH . 'views/inc/other_footer.php' ); ?>
<script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCfaLjR2MOJOoSz6Wiel3aejSYBxqJBs7o'></script>
<script src="<?php echo base_url('assets/js/locationpicker.jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/lw_data_table.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/leads.js'); ?>"></script>