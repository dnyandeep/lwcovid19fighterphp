<?php 
$rebrand = load_config(); 
$app_logo = base_url('uploads/lw_settings/'.$rebrand['nav_logo']);
$app_logo_alternate = base_url('assets/img/placeholder.png');

?>
<!DOCTYPE html>
<html ng-app="Lw" lang="<?php echo lang('lang_code');?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $rebrand['meta_description'] ?>">
    <meta name="keywords" content="<?php echo $rebrand['meta_keywords'] ?>">
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/images/' . $rebrand['favicon_icon'] . ''); ?>">
    <title></title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/lw.css'); ?>" type="text/css" />
    <script src="<?php echo base_url('assets/lib/angular/angular.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/lib/angular/angular-animate.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/lib/angular/angular-aria.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/lib/angular/i18n/angular-locale_' . lang('lang_code_dash') . '.js'); ?>">
    </script>

    <script>
    var BASE_URL = "<?php echo base_url(); ?>",
        update_error = "<?php echo lang('update_error'); ?>",
        SHOW_ONLY_ADMIN = "<?php echo 'true';?>",
        email_error = "<?php echo lang('email_error'); ?>",
        ACTIVESTAFF = "<?php echo $this->session->userdata('usr_id'); ?>",
        CURRENCY = "<?php echo '$'; ?>",
        LOCATE_SELECTED = "<?php echo lang('lang_code');?>",
        UPIMGURL = "<?php echo base_url('uploads/images/'); ?>",
        NTFTITLE = "<?php echo lang('notification')?>",
        INVMARKCACELLED = "<?php echo lang('invoicecancelled')?>",
        TICKSTATUSCHANGE = "<?php echo lang('ticketstatuschanced')?>",
        LEADMARKEDAS = "<?php echo lang('leadmarkedas')?>",
        LEADUNMARKEDAS = "<?php echo lang('leadunmarkedas')?>",
        TODAYDATE = "<?php echo date('Y.m.d ')?>",
        LOGGEDINSTAFFID = "<?php echo $this->session->userdata('usr_id'); ?>",
        LOGGEDINSTAFFNAME = "<?php echo $this->session->userdata('staffname'); ?>",
        LOGGEDINSTAFFAVATAR = "<?php echo $this->session->userdata('staffavatar'); ?>",
        VOICENOTIFICATIONLANG = "<?php echo lang('lang_code_dash');?>",
        initialLocaleCode = "<?php echo lang('initial_locale_code');?>";
    var new_item = "<?php echo lang('new'); ?>";
    var item_unit = "<?php echo lang('unit'); ?>";
    </script>
</head>
<?php $settings = $this->Settings_Model->get_settings_lw();
?>

<body ng-controller="Lw_Controller">
    <?php if ($rebrand['disable_preloader'] == '0') { 
        $preloader =  base_url('assets/img/'.$rebrand['preloader']); ?>
        <div id="lwloader" style="background-image: url(<?php echo $preloader ?>);"></div>
    <?php } ?>
    
<md-content class="lw-body-wrapper lw-body-fixed-sidebar" lw-ready style="padding-top: 0px;">
