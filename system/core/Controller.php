<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;
	public $token="LWCOVID19";
	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');
	}

	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}

	public function sendsmsGET($mobileNumber,$otpMessage)
    {
        // Account details
        return true;
        $username = 'afmsolar'; //  logicowise
        $password = '5cdea5';//  naN4UO
        // Message details
        $senderid = 'AFMSLR'; // LOGICO
        $type = '1';
        $product = '1';
        $number = $mobileNumber;
        $message = $otpMessage;


        // API credentials
        $credentials = 'username='. $username . '&password='. $password;
        // prepare data for GET request
        $data = '&sender='. $senderid . '&mobile='. $number .'&type='. $type. '&product='. $product .'&message='. urlencode($message);
        // make url to post using cURL
        $url = 'http://makemysms.in/api/sendsms.php?'. $credentials . $data;

        // Configure cURL options
        $options = array (CURLOPT_RETURNTRANSFER => true , // return web page
            CURLOPT_HEADER => false , // don't return headers
            CURLOPT_FOLLOWLOCATION => false , // follow redirects
            CURLOPT_ENCODING => "" , // handle compressed
            CURLOPT_USERAGENT => "test" , // who am i
            CURLOPT_AUTOREFERER => true , // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120 , // timeout on connect
            CURLOPT_TIMEOUT => 120 , // timeout on response
            CURLOPT_MAXREDIRS => 10 ); // stop after 10 redirects

        // Send the GET request with cURL
        $ch = curl_init ( $url );
        curl_setopt_array ( $ch, $options );
        $content = curl_exec ( $ch );
        $err = curl_errno ( $ch );
        $errmsg = curl_error ( $ch );
        $header = curl_getinfo ( $ch );
        $httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );
        curl_close ( $ch );

        // Receive response
        $header [ 'errno' ] = $err;
        $header [ 'errmsg' ] = $errmsg;
        $header [ 'content' ] = $content;

        $json = json_decode($content, true);

        if($json['code'] == 201){
            return true;
        }else{
            return false;
        }        
    }

    function generateOTP()
    {
        $otp = rand(100000, 999999);
        return $otp;
    }

}
